﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmFeeCollectedReconcile : Form
    {
        public FrmFeeCollectedReconcile()
        {
            InitializeComponent();
        }
        CommonClass cmc = new CommonClass();
        decimal FeeAmount = 0;
        decimal Concession = 0;
        decimal FinalAmount = 0;
        decimal PaidAmount = 0;
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        BindingSource source = new BindingSource();
        private void FrmFeeCollectedReconcile_Load(object sender, EventArgs e)
        {
            getClass();
        }
        protected void getClass()
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetAllClass("SPGetClass", conn);
            cmbClass.DataSource = null;
            cmbClass.DisplayMember = "Class_Desc";
            cmbClass.ValueMember = "Cid";
            cmbClass.DataSource = dt;
        }

        private void cmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass",conn);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }

        protected void LoadDataGrid(DataTable dt)
        {
            DataGridStudent.DataSource = null;
            DataGridStudent.AutoGenerateColumns = false;
            DataGridStudent.ColumnCount = 2;
            DataGridStudent.Columns[0].Name = "StudentName";
            DataGridStudent.Columns[0].HeaderText = "Student Name";
            DataGridStudent.Columns[0].DataPropertyName = "StudentName";
            DataGridStudent.Columns[0].Width = 260;
            DataGridStudent.Columns[1].Name = "Uid";
            DataGridStudent.Columns[1].HeaderText = "Uid";
            DataGridStudent.Columns[1].DataPropertyName = "Uid";
            DataGridStudent.Columns[1].Visible = false;
            source.DataSource = dt;
            DataGridStudent.DataSource = source;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtStudentsearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                grStudent.Visible = false;
            }
        }

        private void txtStudentsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grStudent.Visible = false;
            }
        }

        private void DataGridStudent_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = DataGridStudent.SelectedCells[0].RowIndex;
            txtstudent.Text = DataGridStudent.Rows[index].Cells[0].Value.ToString();
            txtstudent.Tag = DataGridStudent.Rows[index].Cells[1].Value;
            DataTable dtTerm = cmc.GetAllClass("SP_GetAllTerm",conn);
            cmbTerm.DataSource = null;
            cmbTerm.DisplayMember = "Refname";
            cmbTerm.ValueMember = "Ruid";
            cmbTerm.DataSource = dtTerm;
            grStudent.Visible = false;
        }

        private void cmbTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FeeAmount = 0; Concession = 0;
                FinalAmount = 0; PaidAmount = 0;
                int Classid = (int)cmbClass.SelectedValue;
                SqlParameter[] parameter = {
                    new SqlParameter("@ClassId",Classid),
                    new SqlParameter("@StudId",txtstudent.Tag),
                    new SqlParameter("@termuid",Convert.ToInt32(cmbTerm.SelectedValue))
                };
                DataTable dt = cmc.GetDatabyParameter("SP_GetTermFeeByStudId", parameter,conn);
                LoadTermFee(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadTermFee(DataTable dt)
        {
            DgvTermFee.DataSource = null;
            DgvTermFee.AutoGenerateColumns = false;
            DgvTermFee.ColumnCount = 8;
            DgvTermFee.Columns[0].Name = "Puid";
            DgvTermFee.Columns[0].HeaderText = "Puid";
            DgvTermFee.Columns[0].DataPropertyName = "Puid";
            DgvTermFee.Columns[0].Visible = false;

            DgvTermFee.Columns[1].Name = "Studid";
            DgvTermFee.Columns[1].HeaderText = "Studid";
            DgvTermFee.Columns[1].DataPropertyName = "Studid";
            DgvTermFee.Columns[1].Visible = false;

            DgvTermFee.Columns[2].Name = "feeid";
            DgvTermFee.Columns[2].HeaderText = "feeid";
            DgvTermFee.Columns[2].DataPropertyName = "feeid";
            DgvTermFee.Columns[2].Visible = false;

            DgvTermFee.Columns[3].Name = "Fees";
            DgvTermFee.Columns[3].HeaderText = "Fees";
            DgvTermFee.Columns[3].DataPropertyName = "Descp";
            DgvTermFee.Columns[3].Width = 230;

            DgvTermFee.Columns[4].Name = "Fee Amount";
            DgvTermFee.Columns[4].HeaderText = "Fee Amount";
            DgvTermFee.Columns[4].DataPropertyName = "feeamt";
            DgvTermFee.Columns[4].Width = 110;
            DgvTermFee.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[5].Name = "Concession";
            DgvTermFee.Columns[5].HeaderText = "Concession";
            DgvTermFee.Columns[5].DataPropertyName = "concession";
            DgvTermFee.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[6].Name = "Finalamt";
            DgvTermFee.Columns[6].HeaderText = "Finalamt";
            DgvTermFee.Columns[6].DataPropertyName = "finalamt";
            DgvTermFee.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[7].Name = "Paid Amount";
            DgvTermFee.Columns[7].HeaderText = "Paid Amount";
            DgvTermFee.Columns[7].DataPropertyName = "paidamt";
            DgvTermFee.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DgvTermFee.DataSource = dt;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeAmount += Convert.ToDecimal(dt.Rows[i]["feeamt"].ToString());
                Concession += Convert.ToDecimal(dt.Rows[i]["concession"].ToString());
                FinalAmount += Convert.ToDecimal(dt.Rows[i]["finalamt"].ToString());
                PaidAmount += Convert.ToDecimal(dt.Rows[i]["paidamt"].ToString());
            }
            lblFeeAmount.Text = FeeAmount.ToString();
            lblConcession.Text = Concession.ToString();
            lblFinalAmount.Text = FinalAmount.ToString();
            lblPaidAmount.Text = PaidAmount.ToString();

        }

        private void DgvTermFee_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < DgvTermFee.Rows.Count; i++)
            {
                FeeAmount += Convert.ToDecimal(DgvTermFee.Rows[i].Cells[4].Value.ToString());
                Concession += Convert.ToDecimal(DgvTermFee.Rows[i].Cells[5].Value.ToString());
                FinalAmount += Convert.ToDecimal(DgvTermFee.Rows[i].Cells[6].Value.ToString());
                PaidAmount += Convert.ToDecimal(DgvTermFee.Rows[i].Cells[7].Value.ToString());
            }
            lblFeeAmount.Text = FeeAmount.ToString();
            lblConcession.Text = Concession.ToString();
            lblFinalAmount.Text = FinalAmount.ToString();
            lblPaidAmount.Text = PaidAmount.ToString();
        }

        private void txtstudent_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass", conn);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }

        private void DgvTermFee_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            for (int i = 0; i < DgvTermFee.Rows.Count; i++)
            {

            }
        }

        private void txtStudentsearch_TextChanged(object sender, EventArgs e)
        {
            source.Filter = string.Format("StudentName LIKE '%{0}%'", txtStudentsearch.Text);
        }
    }
}
