﻿namespace SkoolRPWD
{
    partial class FrmRecipteconsile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecipteconsile));
            this.grFront = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPaidAmount = new System.Windows.Forms.Label();
            this.lblFinalAmount = new System.Windows.Forms.Label();
            this.lblConcession = new System.Windows.Forms.Label();
            this.lblFeeAmount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grStudent = new System.Windows.Forms.GroupBox();
            this.txtStudentsearch = new System.Windows.Forms.TextBox();
            this.DataGridStudent = new System.Windows.Forms.DataGridView();
            this.cmbClass = new System.Windows.Forms.ComboBox();
            this.DgvTermFee = new System.Windows.Forms.DataGridView();
            this.cmbTerm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtstudent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbReceipt = new System.Windows.Forms.ComboBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.grFront.SuspendLayout();
            this.grStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermFee)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.label5);
            this.grFront.Controls.Add(this.lblPaidAmount);
            this.grFront.Controls.Add(this.lblFinalAmount);
            this.grFront.Controls.Add(this.lblConcession);
            this.grFront.Controls.Add(this.lblFeeAmount);
            this.grFront.Controls.Add(this.label4);
            this.grFront.Controls.Add(this.grStudent);
            this.grFront.Controls.Add(this.cmbClass);
            this.grFront.Controls.Add(this.DgvTermFee);
            this.grFront.Controls.Add(this.cmbTerm);
            this.grFront.Controls.Add(this.label3);
            this.grFront.Controls.Add(this.label2);
            this.grFront.Controls.Add(this.txtstudent);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.cmbReceipt);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(12, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(662, 441);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(239, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 161;
            this.label5.Text = "Recipt";
            // 
            // lblPaidAmount
            // 
            this.lblPaidAmount.AutoSize = true;
            this.lblPaidAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaidAmount.Location = new System.Drawing.Point(607, 400);
            this.lblPaidAmount.Name = "lblPaidAmount";
            this.lblPaidAmount.Size = new System.Drawing.Size(13, 18);
            this.lblPaidAmount.TabIndex = 160;
            this.lblPaidAmount.Text = "-";
            // 
            // lblFinalAmount
            // 
            this.lblFinalAmount.AutoSize = true;
            this.lblFinalAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalAmount.Location = new System.Drawing.Point(502, 400);
            this.lblFinalAmount.Name = "lblFinalAmount";
            this.lblFinalAmount.Size = new System.Drawing.Size(13, 18);
            this.lblFinalAmount.TabIndex = 159;
            this.lblFinalAmount.Text = "-";
            // 
            // lblConcession
            // 
            this.lblConcession.AutoSize = true;
            this.lblConcession.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConcession.Location = new System.Drawing.Point(405, 400);
            this.lblConcession.Name = "lblConcession";
            this.lblConcession.Size = new System.Drawing.Size(13, 18);
            this.lblConcession.TabIndex = 158;
            this.lblConcession.Text = "-";
            // 
            // lblFeeAmount
            // 
            this.lblFeeAmount.AutoSize = true;
            this.lblFeeAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeeAmount.Location = new System.Drawing.Point(298, 400);
            this.lblFeeAmount.Name = "lblFeeAmount";
            this.lblFeeAmount.Size = new System.Drawing.Size(13, 18);
            this.lblFeeAmount.TabIndex = 157;
            this.lblFeeAmount.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(208, 400);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 156;
            this.label4.Text = "Total";
            // 
            // grStudent
            // 
            this.grStudent.Controls.Add(this.txtStudentsearch);
            this.grStudent.Controls.Add(this.DataGridStudent);
            this.grStudent.Location = new System.Drawing.Point(358, 22);
            this.grStudent.Name = "grStudent";
            this.grStudent.Size = new System.Drawing.Size(290, 253);
            this.grStudent.TabIndex = 155;
            this.grStudent.TabStop = false;
            this.grStudent.Enter += new System.EventHandler(this.grStudent_Enter);
            // 
            // txtStudentsearch
            // 
            this.txtStudentsearch.BackColor = System.Drawing.Color.White;
            this.txtStudentsearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStudentsearch.Location = new System.Drawing.Point(6, 13);
            this.txtStudentsearch.MaxLength = 250;
            this.txtStudentsearch.Name = "txtStudentsearch";
            this.txtStudentsearch.Size = new System.Drawing.Size(278, 26);
            this.txtStudentsearch.TabIndex = 143;
            this.txtStudentsearch.TextChanged += new System.EventHandler(this.txtStudentsearch_TextChanged);
            // 
            // DataGridStudent
            // 
            this.DataGridStudent.AllowUserToAddRows = false;
            this.DataGridStudent.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStudent.Location = new System.Drawing.Point(6, 42);
            this.DataGridStudent.Name = "DataGridStudent";
            this.DataGridStudent.ReadOnly = true;
            this.DataGridStudent.RowHeadersVisible = false;
            this.DataGridStudent.Size = new System.Drawing.Size(278, 204);
            this.DataGridStudent.TabIndex = 0;
            this.DataGridStudent.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStudent_CellMouseDoubleClick);
            // 
            // cmbClass
            // 
            this.cmbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClass.FormattingEnabled = true;
            this.cmbClass.Location = new System.Drawing.Point(99, 22);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Size = new System.Drawing.Size(169, 26);
            this.cmbClass.TabIndex = 154;
            this.cmbClass.SelectedIndexChanged += new System.EventHandler(this.cmbClass_SelectedIndexChanged);
            // 
            // DgvTermFee
            // 
            this.DgvTermFee.AllowUserToAddRows = false;
            this.DgvTermFee.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvTermFee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTermFee.Location = new System.Drawing.Point(6, 105);
            this.DgvTermFee.Name = "DgvTermFee";
            this.DgvTermFee.RowHeadersVisible = false;
            this.DgvTermFee.Size = new System.Drawing.Size(647, 292);
            this.DgvTermFee.TabIndex = 153;
            // 
            // cmbTerm
            // 
            this.cmbTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTerm.FormattingEnabled = true;
            this.cmbTerm.Location = new System.Drawing.Point(99, 76);
            this.cmbTerm.Name = "cmbTerm";
            this.cmbTerm.Size = new System.Drawing.Size(134, 26);
            this.cmbTerm.TabIndex = 149;
            this.cmbTerm.SelectedIndexChanged += new System.EventHandler(this.cmbTerm_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(51, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 152;
            this.label3.Text = "Term";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 151;
            this.label2.Text = "Student";
            // 
            // txtstudent
            // 
            this.txtstudent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtstudent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstudent.Location = new System.Drawing.Point(99, 49);
            this.txtstudent.MaxLength = 250;
            this.txtstudent.Name = "txtstudent";
            this.txtstudent.Size = new System.Drawing.Size(238, 26);
            this.txtstudent.TabIndex = 148;
            this.txtstudent.Click += new System.EventHandler(this.txtstudent_Click);
            this.txtstudent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtstudent_MouseClick);
            this.txtstudent.TextChanged += new System.EventHandler(this.txtstudent_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 18);
            this.label1.TabIndex = 150;
            this.label1.Text = "Class ";
            // 
            // cmbReceipt
            // 
            this.cmbReceipt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReceipt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbReceipt.FormattingEnabled = true;
            this.cmbReceipt.Location = new System.Drawing.Point(292, 76);
            this.cmbReceipt.Name = "cmbReceipt";
            this.cmbReceipt.Size = new System.Drawing.Size(95, 26);
            this.cmbReceipt.TabIndex = 162;
            this.cmbReceipt.SelectedIndexChanged += new System.EventHandler(this.cmbReceipt_SelectedIndexChanged);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(599, 451);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 30);
            this.btnsave.TabIndex = 124;
            this.btnsave.Text = "Update";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // FrmRecipteconsile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(694, 483);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.grFront);
            this.MaximizeBox = false;
            this.Name = "FrmRecipteconsile";
            this.Text = "Fees Change";
            this.Load += new System.EventHandler(this.FrmRecipteconsile_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            this.grStudent.ResumeLayout(false);
            this.grStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermFee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.ComboBox cmbReceipt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPaidAmount;
        private System.Windows.Forms.Label lblFinalAmount;
        private System.Windows.Forms.Label lblConcession;
        private System.Windows.Forms.Label lblFeeAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grStudent;
        internal System.Windows.Forms.TextBox txtStudentsearch;
        private System.Windows.Forms.DataGridView DataGridStudent;
        private System.Windows.Forms.ComboBox cmbClass;
        private System.Windows.Forms.DataGridView DgvTermFee;
        private System.Windows.Forms.ComboBox cmbTerm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtstudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnsave;
    }
}