﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace SkoolRPWD
{
    public partial class FrmStudrpt : Form
    {
        public FrmStudrpt()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand qur1 = new SqlCommand();

        private void FrmStudrpt_Load(object sender, EventArgs e)
        {
            qur1.Connection = con;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void loadput()
        {
            con.Close();
            con.Open();

            if (Module.type == 12)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a left join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
            }



            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 12)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 12)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }


                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }

            dt.DataSource = tap;


            contc.Show();
            con.Close();


        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            Module.Dtype = 40;
            if (cboorder.Text == "Boys")
            {
                Module.Dtype = 40;
                //qur1.CommandText = "delete from  studrptpnt";
                //qur1.ExecuteNonQuery();
                //qur1.CommandText = "insert into studrptpnt select  sname,Class_Desc,roll_no,Admisno,uid as suid from StudentM a inner join class_mast b on a.Classid =b.Cid where  classid=" + txtcid .Text +" order by Sex desc,roll_no asc,sname asc";
                //qur1.ExecuteNonQuery();

                //Module.strsql = "select * from studrptpnt";

                Module.cid = Convert.ToInt32(txtcid.Text);
                CRViewer crv = new CRViewer();
                crv.Show();
                con.Close();
            }
            else if (cboorder.Text == "Girls")
            {
                Module.Dtype = 41;

                //qur1.CommandText = "delete from  studrptpnt";
                //qur1.ExecuteNonQuery();
                //qur1.CommandText = "insert into studrptpnt select  sname,Class_Desc,roll_no,Admisno,uid as suid from StudentM a inner join class_mast b on a.Classid =b.Cid where  classid=" + txtcid.Text + " order by Sex ,roll_no asc,sname asc";
                //qur1.ExecuteNonQuery();

                //Module.strsql = "select * from studrptpnt";

                Module.cid = Convert.ToInt32(txtcid.Text);
                CRViewer crv = new CRViewer();
                crv.Show();
                con.Close();
            }
            else if (cboorder.Text == "ALL")
            {
                Module.Dtype = 42;

                //qur1.CommandText = "delete from  studrptpnt";
                //qur1.ExecuteNonQuery();
                //qur1.CommandText = "insert into studrptpnt select  sname,Class_Desc,roll_no,Admisno,uid as suid from StudentM a inner join class_mast b on a.Classid =b.Cid where classid=" + txtcid.Text + " order by roll_no asc,sname asc,Sex ";
                //qur1.ExecuteNonQuery();

                //Module.strsql = "select * from studrptpnt";

                Module.cid = Convert.ToInt32(txtcid.Text);
                CRViewer crv = new CRViewer();
                crv.Show();
                con.Close();
            }

        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 12;
            loadput();
        }

        private void txtclass_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
