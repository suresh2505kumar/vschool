﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace SkoolRPWD
{
    public partial class FrmStudLedger : Form
    {
        public FrmStudLedger()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand qur = new SqlCommand();
        public double sum111 = 0;
        public double s1;
        public double s;
        public double sum222 = 0;
        public double s2;
        public double sum333 = 0;
        public double s3;
        public int i = 0;
        public int j = 0;

        private void FrmStudLedger_Load(object sender, EventArgs e)
        {
            Titlep();
            Titlep1();
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";
            con.Close();
            con.Open();
            if (Module.type == 13)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 14)
            {
                FunC.Partylistviewcont3("uid", "name", "classid", Module.strsql, this, txtsid, txtstud, txtcid, GBList);
                Module.strsql = "select uid, sname + '/' + Admisno as name,classid from StudentM where classid=" + txtcid.Text + " ";
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 13)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
            }
            else if (Module.type == 14)
            {
                dt.Columns[1].Width = 265;
                dt.Columns[2].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 13)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 14)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }
                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void txtstud_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Titlep()
        {
            Dgvlist.ColumnCount = 9;
            Dgvlist.Columns[0].Name = "Term";
            Dgvlist.Columns[0].HeaderText = "Term";
            Dgvlist.Columns[0].DataPropertyName = "Term";
            Dgvlist.Columns[0].Width = 85;

            Dgvlist.Columns[1].Name = "Amount";
            Dgvlist.Columns[1].HeaderText = "Amount";
            Dgvlist.Columns[1].DataPropertyName = "Amount";
            Dgvlist.Columns[1].Width = 100;

            Dgvlist.Columns[2].Name = " Concession";
            Dgvlist.Columns[2].Width = 100;

            Dgvlist.Columns[3].Name = "Final Amt";
            Dgvlist.Columns[3].Width = 100;

            Dgvlist.Columns[4].Name = "Due Amt";
            Dgvlist.Columns[4].Width = 100;

            Dgvlist.Columns[5].Name = "Paid Amount";
            Dgvlist.Columns[5].Width = 100;

            Dgvlist.Columns[6].Name = "Date";
            Dgvlist.Columns[6].Width = 110;

            Dgvlist.Columns[7].Name = "Recpt No";
            Dgvlist.Columns[7].Width = 50;

            Dgvlist.Columns[8].DataPropertyName = "termuid";
            Dgvlist.Columns[8].Visible = false;

        }

        private void Titlep1()
        {
            Dgv.ColumnCount = 9;
            Dgv.Columns[0].Name = "Term";
            Dgv.Columns[0].HeaderText = "Term";
            Dgv.Columns[0].Width = 85;

            Dgv.Columns[1].Name = "Amount";
            Dgv.Columns[1].HeaderText = "Amount";
            Dgv.Columns[1].DataPropertyName = "Amount";
            Dgv.Columns[1].Width = 100;

            Dgv.Columns[2].Name = " Concession";
            Dgv.Columns[2].Width = 100;

            Dgv.Columns[3].Name = "Final Amt";
            Dgv.Columns[3].Width = 100;

            Dgv.Columns[4].Name = "Due Amt";
            Dgv.Columns[4].Width = 100;
            Dgv.Columns["Due Amt"].DefaultCellStyle.ForeColor = Color.DeepPink;

            Dgv.Columns[5].Name = "Paid Amount";
            Dgv.Columns[5].Width = 100;

            Dgv.Columns[6].Name = "Date";
            Dgv.Columns[6].Width = 110;

            Dgv.Columns[7].Name = "Recpt No";
            Dgv.Columns[7].Width = 80;

            Dgv.Columns[8].Name = "termuid";
            Dgv.Columns[8].Visible = false;
        }

        private void Loadlistgrid()
        {
            if (txtcid.Text != "" && txtsid.Text != "")
            {
                con.Close();
                con.Open();
                string del = "delete from Tmpstudled";
                SqlCommand cmndD = new SqlCommand(del, con);
                cmndD.ExecuteNonQuery();
                string qur = "select Refname,sum(feeamt) as FEA ,sum(concession) as Conc ,sum(finalamt) as FA ,'' as due,(SPaidamt) as PA, Colldate ,Recptno,aa.termuid    from (select a.*,Descp,Refname   from plftype  a inner join FeeMast  b on a.feeid =b.Fid inner join FeeRef c on a.termuid =c.Ruid ) aa inner join(select sum(paidamt) as SPaidamt,Recptno,Colldate,termuid  from coll_det a inner join  Coll_Mast  b on a.Colluid =b.Colluid  where  studid =" + txtsid.Text + " and classid =" + txtcid.Text + " and paidamt <> 0 group by Recptno,Colldate,termuid  ) cc on aa.termuid  =cc.termuid where studid =" + txtsid.Text + " and classid =" + txtcid.Text + "  group by Refname,Recptno,Colldate,aa.termuid,SPaidamt order by aa.termuid  ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd1);
                DataSet dt1 = new DataSet();
                adpt1.SelectCommand = cmd1;
                adpt1.Fill(dt1);
                for (j = 0; j < dt1.Tables[0].Rows.Count; j++)
                {
                    j = Dgvlist.Rows.Add();
                    Dgvlist.Rows[j].Cells[0].Value = dt1.Tables[0].Rows[j][0].ToString();
                    Dgvlist.Rows[j].Cells[1].Value = dt1.Tables[0].Rows[j][1].ToString();
                    Dgvlist.Rows[j].Cells[2].Value = dt1.Tables[0].Rows[j][2].ToString();
                    Dgvlist.Rows[j].Cells[3].Value = dt1.Tables[0].Rows[j][3].ToString();
                    Dgvlist.Rows[j].Cells[4].Value = 0;
                    Dgvlist.Rows[j].Cells[5].Value = dt1.Tables[0].Rows[j][5].ToString();
                    Dgvlist.Rows[j].Cells[6].Value = dt1.Tables[0].Rows[j][6].ToString();
                    Dgvlist.Rows[j].Cells[7].Value = dt1.Tables[0].Rows[j][7].ToString();
                    Dgvlist.Rows[j].Cells[8].Value = dt1.Tables[0].Rows[j][8].ToString();
                }
                con.Close();
                con.Open();
                for (i = 0; i < dt1.Tables[0].Rows.Count; i++)
                {
                    string qury = "select * from Tmpstudled  where refname='" + dt1.Tables[0].Rows[i][0].ToString() + "'and FEA=" + dt1.Tables[0].Rows[i][1].ToString() + " and Conc =" + dt1.Tables[0].Rows[i][2].ToString() + " and FA=" + dt1.Tables[0].Rows[i][3].ToString() + "";
                    SqlCommand cmnd1 = new SqlCommand(qury, con);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmnd1);
                    DataTable tap = new DataTable();
                    apt1.Fill(tap);
                    con.Close();
                    con.Open();
                    if (tap.Rows.Count == 0)
                    {
                        if (dt1.Tables[0].Rows[i][0].ToString() == "Term I")
                        {
                            string updat = "insert into  Tmpstudled values('" + Dgvlist.Rows[i].Cells[0].Value + "'," + Dgvlist.Rows[i].Cells[1].Value + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + "," + Dgvlist.Rows[i].Cells[4].Value + "," + Dgvlist.Rows[i].Cells[5].Value + ",'" + dt1.Tables[0].Rows[i][6].ToString() + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                            SqlCommand cmnd = new SqlCommand(updat, con);
                            cmnd.ExecuteNonQuery();
                            sum111 = Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);
                            s1 = Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                            double sss = s1 - sum111;
                            string updatp = "insert into  Tmpstudled(due,PA,REcptno,termuid,logdate) values(" + sss + "," + sum111 + ",''," + Dgvlist.Rows[i].Cells[8].Value + ",'2018-03-31')";
                            SqlCommand cmndp = new SqlCommand(updatp, con);
                            cmndp.ExecuteNonQuery();
                        }
                        else if (dt1.Tables[0].Rows[i][0].ToString() == "Term II")
                        {
                            Dgvlist.Rows[i].Cells[4].Value = sum111.ToString();
                            Dgvlist.Rows[i].Cells[4].Value = s1 - Convert.ToDouble(Dgvlist.Rows[i].Cells[4].Value);
                            string updat = "insert into  Tmpstudled values('" + Dgvlist.Rows[i].Cells[0].Value + "'," + Dgvlist.Rows[i].Cells[1].Value + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + ",0," + Dgvlist.Rows[i].Cells[5].Value + ",'" + dt1.Tables[0].Rows[i][6].ToString() + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                            SqlCommand cmnd = new SqlCommand(updat, con);
                            cmnd.ExecuteNonQuery();
                            sum222 = Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);
                            s2 = Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                            double sss1 = s2 - sum222;
                            string updatp = "insert into  Tmpstudled(due,PA,REcptno,termuid,logdate) values(" + sss1 + "," + sum222 + ",''," + Dgvlist.Rows[i].Cells[8].Value + ",'2018-03-31')";
                            SqlCommand cmndp = new SqlCommand(updatp, con);
                            cmndp.ExecuteNonQuery();
                        }
                        else if (dt1.Tables[0].Rows[i][0].ToString() == "Term III")
                        {
                            Dgvlist.Rows[i].Cells[4].Value = sum222.ToString();
                            Dgvlist.Rows[i].Cells[4].Value = s2 - Convert.ToDouble(Dgvlist.Rows[i].Cells[4].Value);
                            string updat4 = "insert into  Tmpstudled values('" + Dgvlist.Rows[i].Cells[0].Value + "'," + Dgvlist.Rows[i].Cells[1].Value + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + ",0," + Dgvlist.Rows[i].Cells[5].Value + ",'" + dt1.Tables[0].Rows[i][6].ToString() + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                            SqlCommand cmnd4 = new SqlCommand(updat4, con);
                            cmnd4.ExecuteNonQuery();
                            sum333 = Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);
                            s3 = Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                            double sss2 = s3 - sum333;
                            string updatp = "insert into  Tmpstudled(due,PA,REcptno,termuid,logdate) values(" + sss2 + "," + sum333 + ",''," + Dgvlist.Rows[i].Cells[8].Value + ",'2018-03-31')";
                            SqlCommand cmndp = new SqlCommand(updatp, con);
                            cmndp.ExecuteNonQuery();
                        }
                    }
                    else if (dt1.Tables[0].Rows[i][0].ToString() != "Term II" && dt1.Tables[0].Rows[i][0].ToString() != "Term III")
                    {
                        sum111 += Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);
                        string updat = "insert into  Tmpstudled values('',null,null,null,null," + Dgvlist.Rows[i].Cells[5].Value + ",'" + Dgvlist.Rows[i].Cells[6].Value + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                        SqlCommand cmnd = new SqlCommand(updat, con);
                        cmnd.ExecuteNonQuery();

                        string ds = "select isnull(sum(fa),0)-isnull(sum(pa),0) as dues from Tmpstudled where recptno <> 0";
                        SqlCommand cd = new SqlCommand(ds, con);
                        SqlDataAdapter at = new SqlDataAdapter(cd);
                        DataTable tapb = new DataTable();
                        at.Fill(tapb);

                        string updatl = "update Tmpstudled set due=" + tapb.Rows[0]["dues"].ToString() + ",PA=" + sum111 + " where termuid=" + Dgvlist.Rows[i].Cells[8].Value + "  and recptno =0";
                        SqlCommand cmndl = new SqlCommand(updatl, con);
                        cmndl.ExecuteNonQuery();
                    }
                    else if (dt1.Tables[0].Rows[i][0].ToString() != "Term III" && dt1.Tables[0].Rows[i][0].ToString() != "Term I")
                    {
                        sum222 += Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);

                        string updat3 = "insert into  Tmpstudled values('',null,null,null,null," + Dgvlist.Rows[i].Cells[5].Value + ",'" + Dgvlist.Rows[i].Cells[6].Value + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                        SqlCommand cmnd3 = new SqlCommand(updat3, con);
                        cmnd3.ExecuteNonQuery();

                        string ds = "select isnull(sum(fa),0)-isnull(sum(pa),0) as dues from Tmpstudled where termuid=" + Dgvlist.Rows[i].Cells[8].Value + " and recptno <> 0";
                        SqlCommand cd = new SqlCommand(ds, con);
                        SqlDataAdapter at = new SqlDataAdapter(cd);
                        DataTable tapb = new DataTable();
                        at.Fill(tapb);

                        string updatl = "update Tmpstudled set due=" + tapb.Rows[0]["dues"].ToString() + ", PA=" + sum222 + "  where termuid=" + Dgvlist.Rows[i].Cells[8].Value + " and recptno =0";
                        SqlCommand cmndl = new SqlCommand(updatl, con);
                        cmndl.ExecuteNonQuery();
                    }
                    else if (dt1.Tables[0].Rows[i][0].ToString() != "Term I" && dt1.Tables[0].Rows[i][0].ToString() != "Term II")
                    {
                        sum333 += Convert.ToDouble(Dgvlist.Rows[i].Cells[5].Value);

                        string updat3 = "insert into  Tmpstudled values('',null,null,null,null," + Dgvlist.Rows[i].Cells[5].Value + ",'" + Dgvlist.Rows[i].Cells[6].Value + "','" + Dgvlist.Rows[i].Cells[7].Value + "'," + Dgvlist.Rows[i].Cells[8].Value + ",getdate())";
                        SqlCommand cmnd3 = new SqlCommand(updat3, con);
                        cmnd3.ExecuteNonQuery();

                        string ds = "select isnull(sum(fa),0)-isnull(sum(pa),0) as dues from Tmpstudled where termuid=" + Dgvlist.Rows[i].Cells[8].Value + " and recptno <> 0";
                        SqlCommand cd = new SqlCommand(ds, con);
                        SqlDataAdapter at = new SqlDataAdapter(cd);
                        DataTable tapb = new DataTable();
                        at.Fill(tapb);


                        string updatl = "update Tmpstudled set due=" + tapb.Rows[0]["dues"].ToString() + ", PA=" + sum333 + "  where termuid=" + Dgvlist.Rows[i].Cells[8].Value + " and recptno =0";
                        SqlCommand cmndl = new SqlCommand(updatl, con);
                        cmndl.ExecuteNonQuery();
                    }
                }
                Load_grid();
            }
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select refname, FEA,Conc,FA,due,PA,CONVERT(VARCHAR, Colldate, 103)as Colldate,Recptno,termuid from Tmpstudled order by  termuid,logdate";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 9;

                Dgv.Columns[0].Name = "refname";
                Dgv.Columns[0].HeaderText = "Term";
                Dgv.Columns[0].DataPropertyName = "refname";
                Dgv.Columns[0].Width = 100;

                Dgv.Columns[1].Name = "FEA";
                Dgv.Columns[1].HeaderText = "Amount";
                Dgv.Columns[1].DataPropertyName = "FEA";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].DataPropertyName = "Conc";
                Dgv.Columns[2].HeaderText = "Concession";
                Dgv.Columns[2].DataPropertyName = "Conc";
                Dgv.Columns[2].Width = 100;

                Dgv.Columns[3].DataPropertyName = "FA";
                Dgv.Columns[3].HeaderText = "Final Amount";
                Dgv.Columns[3].DataPropertyName = "FA";
                Dgv.Columns[3].Width = 100;

                Dgv.Columns[4].DataPropertyName = "due";
                Dgv.Columns[4].HeaderText = "Due";
                Dgv.Columns[4].Width = 100;

                Dgv.Columns[5].DataPropertyName = "PA";
                Dgv.Columns[5].HeaderText = "Paid Amount";
                Dgv.Columns[5].Width = 100;

                Dgv.Columns[6].DataPropertyName = "Colldate";
                Dgv.Columns[6].HeaderText = "Date";
                Dgv.Columns[6].Width = 110;

                Dgv.Columns[7].DataPropertyName = "Recptno";
                Dgv.Columns[7].HeaderText = "Recptno";
                Dgv.Columns[7].Width = 80;

                Dgv.Columns[8].DataPropertyName = "termuid";
                Dgv.Columns[8].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void txtclass_TextChanged(object sender, EventArgs e)
        {
            Dgvlist.Rows.Clear();
            Dgvlist.DataSource = null;
            Dgv.DataSource = null;
            txtsid.Text = "";
            txtstud.Text = "";
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            GetLedger(Convert.ToInt32(txtsid.Text));
        }

        private void txtstud_TextChanged(object sender, EventArgs e)
        {
            Dgvlist.Rows.Clear();
            Dgvlist.DataSource = null;
            Dgv.DataSource = null;
            if (txtsid.Text != string.Empty)
            {
                GetLedger(Convert.ToInt32(txtsid.Text));
            }

            //Loadlistgrid();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (Dgv.Rows.Count > 0)
            {
                try
                {
                    DataTable dt5 = new DataTable();
                    StreamWriter wr = new StreamWriter(@"D:\\Student Ledger.xls");
                    DataGridViewCellStyle l_objDGVCS = new DataGridViewCellStyle();
                    System.Drawing.Font l_objFont = new System.Drawing.Font(FontFamily.GenericSansSerif, 8, FontStyle.Underline);
                    l_objDGVCS.Font = l_objFont;
                    l_objDGVCS.ForeColor = Color.Blue;
                    foreach (DataGridViewColumn col in Dgv.Columns)
                    {
                        dt5.Columns.Add(col.HeaderText, col.ValueType);
                    }
                    int count = 0;
                    foreach (DataGridViewRow row in Dgv.Rows)
                    {
                        if (count < Dgv.Rows.Count - 1)
                        {
                            dt5.Rows.Add();
                            foreach (DataGridViewCell cell in row.Cells)
                            {
                                if (cell.Value.ToString() == "")
                                {
                                    wr.Write("");
                                }
                                else
                                {
                                    dt5.Rows[dt5.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                                }
                            }
                        }
                        count++;
                    }
                    for (int i = 0; i < dt5.Columns.Count; i++)
                    {
                        if (Dgv.Columns[i].Visible == false)
                        {
                            wr.Write("");
                        }
                        else
                        {
                            wr.Write(dt5.Columns[i].ToString().ToUpper() + "\t");
                        }
                    }
                    wr.WriteLine();
                    for (int i = 0; i < (dt5.Rows.Count); i++)
                    {
                        for (int j = 0; j < dt5.Columns.Count; j++)
                        {
                            if (Dgv.Columns[j].Visible)
                            {
                                if (dt5.Rows[i][j] != null)
                                {
                                    wr.Write(Convert.ToString(dt5.Rows[i][j]) + "\t");
                                }
                                else
                                {
                                    wr.Write("");
                                }
                            }
                        }
                        wr.WriteLine();
                    }
                    MessageBox.Show("Data Exported Successfully", "Information");
                    string path = string.Empty;
                    wr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Information");
                }
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {

        }

        private void Dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int colIndex = e.ColumnIndex;
            int rowIndex = e.RowIndex;

            if (rowIndex >= 0 && colIndex >= 0)
            {
                DataGridViewRow theRow = Dgv.Rows[rowIndex];
                if (theRow.Cells[colIndex].Value.ToString() == "0")   // Check Cell Value is equal to 0
                    theRow.DefaultCellStyle.ForeColor = Color.Red;       // Change ForeColor of Cell
            }
        }

        private void Dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void Dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 13;
            loadput();
        }

        private void txtstud_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 14;
            loadput();
        }
        protected void GetLedger(int studId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetFeeLedger";
                cmd.Parameters.AddWithValue("@StudId", studId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Dgv.DataSource = null;
                Dgv.AutoGenerateColumns = false;
                Dgv.ColumnCount = 7;
                Dgv.Columns[0].Name = "Ref Name";
                Dgv.Columns[0].HeaderText = "Ref Name";
                Dgv.Columns[0].DataPropertyName = "refname";

                Dgv.Columns[1].Name = "Fees Amount";
                Dgv.Columns[1].HeaderText = "Fees Amount";
                Dgv.Columns[1].DataPropertyName = "Famt";

                Dgv.Columns[2].Name = "Concession";
                Dgv.Columns[2].HeaderText = "Concession";
                Dgv.Columns[2].DataPropertyName = "Con";

                Dgv.Columns[3].Name = "Final Amount";
                Dgv.Columns[3].HeaderText = "Final Amount";
                Dgv.Columns[3].DataPropertyName = "FiAmt";

                Dgv.Columns[4].Name = "Paid Amount";
                Dgv.Columns[4].HeaderText = "Paid Amount";
                Dgv.Columns[4].DataPropertyName = "PAmt";

                Dgv.Columns[5].Name = "Receipt Number";
                Dgv.Columns[5].HeaderText = "Receipt Number";
                Dgv.Columns[5].DataPropertyName = "recptno";

                Dgv.Columns[6].Name = "Receipt Date";
                Dgv.Columns[6].HeaderText = "Receipt Date";
                Dgv.Columns[6].DataPropertyName = "colldate";
                Dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
