﻿using SkoolRPWD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmStudbusRpt : Form
    {
        public FrmStudbusRpt()
        {
            InitializeComponent();
        }
        CommonClass db = new CommonClass();
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void FrmStudbusRpt_Load(object sender, EventArgs e)
        {
            LodClass();
            LoadRefT();
        }

        protected void LodClass()
        {
            try
            {
                string Query = "Select Class_Desc,Cid from Class_Mast order by Class_desc";
                
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.NewRow();
                dr["Class_Desc"] = "All";
                dr["Cid"] = 0;
                dt.Rows.InsertAt(dr, 0);
                cmbClass.DataSource = null;
                cmbClass.DisplayMember = "Class_Desc";
                cmbClass.ValueMember = "Cid";
                cmbClass.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void LoadRefT()
        {
            string qur = "SELECT Ruid,Refname FROM feeref";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboTerm.DataSource = null;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.DataSource = tap;
            cboTerm.SelectedIndex = -1;

        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            try
            {
                if(cmbClass.SelectedIndex != -1 || cboTerm.SelectedIndex != -1)
                {
                    int Tag = 0;
                    if(cmbClass.Text == "All")
                    {
                        Tag = 2;
                    }
                    else
                    {
                        Tag = 1;
                    }
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Tag",Tag),
                        new SqlParameter("@Classid",cmbClass.SelectedValue),
                        new SqlParameter("@Termuid",cboTerm.SelectedValue),
                    };
                    DataTable dataTable = db.GetDatabyParameter("Proc_GetBusStudents", sqlParameters, conn);
                    if(dataTable.Rows.Count > 0)
                    {
                        Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                        Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Open(Application.StartupPath + "/BusStudents.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                        xlApp.DisplayAlerts = false;
                        xlWb.SaveAs(Application.StartupPath + "/BusStudents1.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                        Microsoft.Office.Interop.Excel.Workbook xlWbN = xlApp.Workbooks.Open(Application.StartupPath + "/BusStudents1.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                        Microsoft.Office.Interop.Excel.Worksheet xlSht = xlWbN.Sheets[1];
                        xlApp.Visible = true;

                        int j = 4;
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            Microsoft.Office.Interop.Excel.Range SlNo = xlSht.Rows.Cells[j, 1];
                            Microsoft.Office.Interop.Excel.Range AdmissionNo = xlSht.Rows.Cells[j, 2];
                            Microsoft.Office.Interop.Excel.Range StudentName = xlSht.Rows.Cells[j, 3];
                            Microsoft.Office.Interop.Excel.Range Class = xlSht.Rows.Cells[j, 4];
                            Microsoft.Office.Interop.Excel.Range Phone = xlSht.Rows.Cells[j, 5];
                            Microsoft.Office.Interop.Excel.Range Amount = xlSht.Rows.Cells[j, 6];
                            SlNo.Value = i + 1;
                            AdmissionNo.Value = dataTable.Rows[i]["Admisno"].ToString();
                            StudentName.Value = dataTable.Rows[i]["SName"].ToString();
                            Class.Value = dataTable.Rows[i]["Class_desc"].ToString();
                            Phone.Value = dataTable.Rows[i]["Fphone"].ToString();
                            Amount.Value = dataTable.Rows[i]["Amount"].ToString();
                            j += 1;
                        }
                        MessageBox.Show("Exported Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
