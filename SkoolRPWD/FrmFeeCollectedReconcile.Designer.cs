﻿namespace SkoolRPWD
{
    partial class FrmFeeCollectedReconcile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFeeCollectedReconcile));
            this.grFront = new System.Windows.Forms.GroupBox();
            this.lblPaidAmount = new System.Windows.Forms.Label();
            this.lblFinalAmount = new System.Windows.Forms.Label();
            this.lblConcession = new System.Windows.Forms.Label();
            this.lblFeeAmount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grStudent = new System.Windows.Forms.GroupBox();
            this.txtStudentsearch = new System.Windows.Forms.TextBox();
            this.DataGridStudent = new System.Windows.Forms.DataGridView();
            this.cmbClass = new System.Windows.Forms.ComboBox();
            this.DgvTermFee = new System.Windows.Forms.DataGridView();
            this.cmbTerm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtstudent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grFront.SuspendLayout();
            this.grStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermFee)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.lblPaidAmount);
            this.grFront.Controls.Add(this.lblFinalAmount);
            this.grFront.Controls.Add(this.lblConcession);
            this.grFront.Controls.Add(this.lblFeeAmount);
            this.grFront.Controls.Add(this.label4);
            this.grFront.Controls.Add(this.grStudent);
            this.grFront.Controls.Add(this.cmbClass);
            this.grFront.Controls.Add(this.DgvTermFee);
            this.grFront.Controls.Add(this.cmbTerm);
            this.grFront.Controls.Add(this.label3);
            this.grFront.Controls.Add(this.label2);
            this.grFront.Controls.Add(this.txtstudent);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(12, 12);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(668, 432);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // lblPaidAmount
            // 
            this.lblPaidAmount.AutoSize = true;
            this.lblPaidAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaidAmount.Location = new System.Drawing.Point(621, 393);
            this.lblPaidAmount.Name = "lblPaidAmount";
            this.lblPaidAmount.Size = new System.Drawing.Size(13, 18);
            this.lblPaidAmount.TabIndex = 147;
            this.lblPaidAmount.Text = "-";
            // 
            // lblFinalAmount
            // 
            this.lblFinalAmount.AutoSize = true;
            this.lblFinalAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinalAmount.Location = new System.Drawing.Point(516, 393);
            this.lblFinalAmount.Name = "lblFinalAmount";
            this.lblFinalAmount.Size = new System.Drawing.Size(13, 18);
            this.lblFinalAmount.TabIndex = 146;
            this.lblFinalAmount.Text = "-";
            // 
            // lblConcession
            // 
            this.lblConcession.AutoSize = true;
            this.lblConcession.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConcession.Location = new System.Drawing.Point(419, 393);
            this.lblConcession.Name = "lblConcession";
            this.lblConcession.Size = new System.Drawing.Size(13, 18);
            this.lblConcession.TabIndex = 145;
            this.lblConcession.Text = "-";
            // 
            // lblFeeAmount
            // 
            this.lblFeeAmount.AutoSize = true;
            this.lblFeeAmount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeeAmount.Location = new System.Drawing.Point(312, 393);
            this.lblFeeAmount.Name = "lblFeeAmount";
            this.lblFeeAmount.Size = new System.Drawing.Size(13, 18);
            this.lblFeeAmount.TabIndex = 144;
            this.lblFeeAmount.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(222, 393);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 143;
            this.label4.Text = "Total";
            // 
            // grStudent
            // 
            this.grStudent.Controls.Add(this.txtStudentsearch);
            this.grStudent.Controls.Add(this.DataGridStudent);
            this.grStudent.Location = new System.Drawing.Point(372, 15);
            this.grStudent.Name = "grStudent";
            this.grStudent.Size = new System.Drawing.Size(290, 253);
            this.grStudent.TabIndex = 142;
            this.grStudent.TabStop = false;
            // 
            // txtStudentsearch
            // 
            this.txtStudentsearch.BackColor = System.Drawing.Color.White;
            this.txtStudentsearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStudentsearch.Location = new System.Drawing.Point(6, 13);
            this.txtStudentsearch.MaxLength = 250;
            this.txtStudentsearch.Name = "txtStudentsearch";
            this.txtStudentsearch.Size = new System.Drawing.Size(278, 26);
            this.txtStudentsearch.TabIndex = 143;
            this.txtStudentsearch.TextChanged += new System.EventHandler(this.txtStudentsearch_TextChanged);
            this.txtStudentsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStudentsearch_KeyDown);
            this.txtStudentsearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudentsearch_KeyPress);
            // 
            // DataGridStudent
            // 
            this.DataGridStudent.AllowUserToAddRows = false;
            this.DataGridStudent.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStudent.Location = new System.Drawing.Point(6, 42);
            this.DataGridStudent.Name = "DataGridStudent";
            this.DataGridStudent.ReadOnly = true;
            this.DataGridStudent.RowHeadersVisible = false;
            this.DataGridStudent.Size = new System.Drawing.Size(278, 204);
            this.DataGridStudent.TabIndex = 0;
            this.DataGridStudent.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStudent_CellMouseDoubleClick);
            // 
            // cmbClass
            // 
            this.cmbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClass.FormattingEnabled = true;
            this.cmbClass.Location = new System.Drawing.Point(113, 15);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Size = new System.Drawing.Size(169, 26);
            this.cmbClass.TabIndex = 141;
            this.cmbClass.SelectedIndexChanged += new System.EventHandler(this.cmbClass_SelectedIndexChanged);
            // 
            // DgvTermFee
            // 
            this.DgvTermFee.AllowUserToAddRows = false;
            this.DgvTermFee.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvTermFee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTermFee.Location = new System.Drawing.Point(15, 98);
            this.DgvTermFee.Name = "DgvTermFee";
            this.DgvTermFee.RowHeadersVisible = false;
            this.DgvTermFee.Size = new System.Drawing.Size(647, 292);
            this.DgvTermFee.TabIndex = 140;
            this.DgvTermFee.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvTermFee_CellFormatting);
            this.DgvTermFee.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTermFee_CellValueChanged);
            // 
            // cmbTerm
            // 
            this.cmbTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTerm.FormattingEnabled = true;
            this.cmbTerm.Location = new System.Drawing.Point(113, 69);
            this.cmbTerm.Name = "cmbTerm";
            this.cmbTerm.Size = new System.Drawing.Size(169, 26);
            this.cmbTerm.TabIndex = 136;
            this.cmbTerm.SelectedIndexChanged += new System.EventHandler(this.cmbTerm_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(65, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 139;
            this.label3.Text = "Term";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(47, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 138;
            this.label2.Text = "Student";
            // 
            // txtstudent
            // 
            this.txtstudent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtstudent.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstudent.Location = new System.Drawing.Point(113, 42);
            this.txtstudent.MaxLength = 250;
            this.txtstudent.Name = "txtstudent";
            this.txtstudent.Size = new System.Drawing.Size(238, 26);
            this.txtstudent.TabIndex = 135;
            this.txtstudent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtstudent_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(62, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 18);
            this.label1.TabIndex = 137;
            this.label1.Text = "Class ";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Controls.Add(this.btnUpdate);
            this.panadd.Location = new System.Drawing.Point(12, 450);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(668, 34);
            this.panadd.TabIndex = 211;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(610, 2);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(54, 30);
            this.btnexit.TabIndex = 220;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(537, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(74, 30);
            this.btnUpdate.TabIndex = 123;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Visible = false;
            // 
            // FrmFeeCollectedReconcile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(692, 493);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Name = "FrmFeeCollectedReconcile";
            this.Text = "View Term Fees";
            this.Load += new System.EventHandler(this.FrmFeeCollectedReconcile_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            this.grStudent.ResumeLayout(false);
            this.grStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTermFee)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.ComboBox cmbTerm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtstudent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DgvTermFee;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox cmbClass;
        private System.Windows.Forms.GroupBox grStudent;
        private System.Windows.Forms.DataGridView DataGridStudent;
        private System.Windows.Forms.Button btnexit;
        internal System.Windows.Forms.TextBox txtStudentsearch;
        private System.Windows.Forms.Label lblFinalAmount;
        private System.Windows.Forms.Label lblConcession;
        private System.Windows.Forms.Label lblFeeAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPaidAmount;
    }
}