﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace SkoolRPWD
{
    public partial class FeeReports : Form
    {
        public FeeReports()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand qur1 = new SqlCommand();
        private void FeeReports_Load(object sender, EventArgs e)
        {
            qur1.Connection = con;
        }
        private void cmdprt_Click(object sender, EventArgs e)
        {
            Module.Dtype = 30;
            con.Close();
            con.Open();
            if (cboTerm.Text == "Daily Fee Collection Report")
            {
                Module.theDate = dtp.Value.ToString("dd-MMM-yyyy");
                Module.toDate = dtp1.Value.ToString("dd-MMM-yyyy");
                CRViewer crv = new CRViewer();
                crv.MdiParent = this.MdiParent;
                crv.Show();
                con.Close();
            }
            else
            {
                Module.Dtype = 31;
                Module.theDate = dtp.Value.ToString("dd-MMM-yyyy");
                Module.toDate = dtp1.Value.ToString("dd-MMM-yyyy");
                CRViewer crv = new CRViewer();
                crv.MdiParent = this.MdiParent;
                crv.Show();
                con.Close();
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
