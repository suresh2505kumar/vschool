﻿namespace SkoolRPWD
{
    partial class FrmFeeConcessionBulk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFeeConcessionBulk));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.RichTextBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.grStudent = new System.Windows.Forms.GroupBox();
            this.txtStudentsearch = new System.Windows.Forms.TextBox();
            this.DataGridStudent = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.DataGridTermIII = new System.Windows.Forms.DataGridView();
            this.DataGridTermII = new System.Windows.Forms.DataGridView();
            this.DataGridTermI = new System.Windows.Forms.DataGridView();
            this.txtTermIII = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTermII = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStudentName = new System.Windows.Forms.TextBox();
            this.cmbClass = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTermI = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.grStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).BeginInit();
            this.panadd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermIII)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermII)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermI)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.btnShow);
            this.groupBox1.Controls.Add(this.grStudent);
            this.groupBox1.Controls.Add(this.panadd);
            this.groupBox1.Controls.Add(this.DataGridTermIII);
            this.groupBox1.Controls.Add(this.DataGridTermII);
            this.groupBox1.Controls.Add(this.DataGridTermI);
            this.groupBox1.Controls.Add(this.txtTermIII);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtTermII);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtStudentName);
            this.groupBox1.Controls.Add(this.cmbClass);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtTermI);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1189, 519);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(55, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 18);
            this.label7.TabIndex = 939;
            this.label7.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.CalendarFont = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.CustomFormat = "dd-MMM-yyyy hh:mm:ss";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(106, 51);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(101, 26);
            this.dtpDate.TabIndex = 938;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(760, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 937;
            this.label6.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(823, 13);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(334, 62);
            this.txtRemarks.TabIndex = 936;
            this.txtRemarks.Text = "";
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(643, 19);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(76, 25);
            this.btnShow.TabIndex = 935;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // grStudent
            // 
            this.grStudent.Controls.Add(this.txtStudentsearch);
            this.grStudent.Controls.Add(this.DataGridStudent);
            this.grStudent.Location = new System.Drawing.Point(334, 42);
            this.grStudent.Name = "grStudent";
            this.grStudent.Size = new System.Drawing.Size(290, 253);
            this.grStudent.TabIndex = 934;
            this.grStudent.TabStop = false;
            this.grStudent.Enter += new System.EventHandler(this.grStudent_Enter);
            // 
            // txtStudentsearch
            // 
            this.txtStudentsearch.BackColor = System.Drawing.Color.White;
            this.txtStudentsearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStudentsearch.Location = new System.Drawing.Point(6, 13);
            this.txtStudentsearch.MaxLength = 250;
            this.txtStudentsearch.Name = "txtStudentsearch";
            this.txtStudentsearch.Size = new System.Drawing.Size(278, 26);
            this.txtStudentsearch.TabIndex = 143;
            this.txtStudentsearch.TextChanged += new System.EventHandler(this.TxtStudentsearch_TextChanged);
            this.txtStudentsearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStudentsearch_KeyDown);
            // 
            // DataGridStudent
            // 
            this.DataGridStudent.AllowUserToAddRows = false;
            this.DataGridStudent.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStudent.Location = new System.Drawing.Point(6, 42);
            this.DataGridStudent.Name = "DataGridStudent";
            this.DataGridStudent.ReadOnly = true;
            this.DataGridStudent.RowHeadersVisible = false;
            this.DataGridStudent.Size = new System.Drawing.Size(278, 204);
            this.DataGridStudent.TabIndex = 0;
            this.DataGridStudent.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStudent_CellMouseDoubleClick);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(6, 481);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1178, 34);
            this.panadd.TabIndex = 933;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(1108, 2);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(67, 30);
            this.btnaddrcan.TabIndex = 124;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(1034, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(73, 30);
            this.btnsave.TabIndex = 125;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // DataGridTermIII
            // 
            this.DataGridTermIII.AllowUserToAddRows = false;
            this.DataGridTermIII.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTermIII.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTermIII.Location = new System.Drawing.Point(796, 117);
            this.DataGridTermIII.Name = "DataGridTermIII";
            this.DataGridTermIII.ReadOnly = true;
            this.DataGridTermIII.RowHeadersVisible = false;
            this.DataGridTermIII.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTermIII.Size = new System.Drawing.Size(388, 351);
            this.DataGridTermIII.TabIndex = 12;
            // 
            // DataGridTermII
            // 
            this.DataGridTermII.AllowUserToAddRows = false;
            this.DataGridTermII.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTermII.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTermII.Location = new System.Drawing.Point(401, 117);
            this.DataGridTermII.Name = "DataGridTermII";
            this.DataGridTermII.ReadOnly = true;
            this.DataGridTermII.RowHeadersVisible = false;
            this.DataGridTermII.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTermII.Size = new System.Drawing.Size(391, 351);
            this.DataGridTermII.TabIndex = 11;
            // 
            // DataGridTermI
            // 
            this.DataGridTermI.AllowUserToAddRows = false;
            this.DataGridTermI.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTermI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTermI.Location = new System.Drawing.Point(6, 117);
            this.DataGridTermI.Name = "DataGridTermI";
            this.DataGridTermI.ReadOnly = true;
            this.DataGridTermI.RowHeadersVisible = false;
            this.DataGridTermI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTermI.Size = new System.Drawing.Size(391, 351);
            this.DataGridTermI.TabIndex = 10;
            // 
            // txtTermIII
            // 
            this.txtTermIII.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTermIII.Location = new System.Drawing.Point(858, 88);
            this.txtTermIII.Name = "txtTermIII";
            this.txtTermIII.Size = new System.Drawing.Size(134, 26);
            this.txtTermIII.TabIndex = 9;
            this.txtTermIII.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTermIII_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(798, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Term III";
            // 
            // txtTermII
            // 
            this.txtTermII.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTermII.Location = new System.Drawing.Point(454, 89);
            this.txtTermII.Name = "txtTermII";
            this.txtTermII.Size = new System.Drawing.Size(121, 26);
            this.txtTermII.TabIndex = 7;
            this.txtTermII.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTermII_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(398, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Term II";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Term I";
            // 
            // txtStudentName
            // 
            this.txtStudentName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStudentName.Location = new System.Drawing.Point(334, 19);
            this.txtStudentName.Name = "txtStudentName";
            this.txtStudentName.Size = new System.Drawing.Size(303, 26);
            this.txtStudentName.TabIndex = 3;
            this.txtStudentName.Click += new System.EventHandler(this.txtStudentName_Click);
            // 
            // cmbClass
            // 
            this.cmbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbClass.FormattingEnabled = true;
            this.cmbClass.Location = new System.Drawing.Point(106, 19);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Size = new System.Drawing.Size(149, 26);
            this.cmbClass.TabIndex = 2;
            this.cmbClass.SelectedIndexChanged += new System.EventHandler(this.cmbClass_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(262, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Student";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class";
            // 
            // txtTermI
            // 
            this.txtTermI.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTermI.Location = new System.Drawing.Point(58, 88);
            this.txtTermI.Name = "txtTermI";
            this.txtTermI.Size = new System.Drawing.Size(121, 26);
            this.txtTermI.TabIndex = 5;
            this.txtTermI.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTermI_KeyDown);
            this.txtTermI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTermI_KeyPress);
            // 
            // FrmFeeConcessionBulk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1214, 534);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmFeeConcessionBulk";
            this.Text = "FrmFeeConcessionBulk";
            this.Load += new System.EventHandler(this.FrmFeeConcessionBulk_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grStudent.ResumeLayout(false);
            this.grStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).EndInit();
            this.panadd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermIII)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermII)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTermI)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTermIII;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTermII;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTermI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtStudentName;
        private System.Windows.Forms.ComboBox cmbClass;
        private System.Windows.Forms.DataGridView DataGridTermIII;
        private System.Windows.Forms.DataGridView DataGridTermII;
        private System.Windows.Forms.DataGridView DataGridTermI;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.GroupBox grStudent;
        internal System.Windows.Forms.TextBox txtStudentsearch;
        private System.Windows.Forms.DataGridView DataGridStudent;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox txtRemarks;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }
}