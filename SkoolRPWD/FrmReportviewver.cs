﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmReportviewver : Form
    {
        ReportDocument doc = new ReportDocument();
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        public FrmReportviewver()
        {
            InitializeComponent();
        }
        private void FrmReportviewver_Load(object sender, EventArgs e)
        {
            LodCombo();
            LodComboTerm();
            cmbClass.SelectedIndex = -1;
            cmbTerm.SelectedIndex = -1;
        }

        protected void LodCombo()
        {
            try
            {
                string Query = "Select Class_Desc,Cid from Class_Mast order by Class_desc";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.NewRow();
                dr["Class_Desc"] = "All";
                dr["Cid"] = 0;
                dt.Rows.InsertAt(dr, 0);
                cmbClass.DataSource = null;
                cmbClass.DisplayMember = "Class_Desc";
                cmbClass.ValueMember = "Cid";
                cmbClass.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
        protected void LodComboTerm()
        {
            try
            {
                string Query = "Select Ruid,Refname from FeeRef";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cmbTerm.DataSource = null;
                cmbTerm.DisplayMember = "Refname";
                cmbTerm.ValueMember = "Ruid";
                cmbTerm.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            int Tag = 0;
            int classId = 0;
            int TermUid = 0;
            if(cmbClass.Text == "All")
            {
                Tag = 0;
                classId = Convert.ToInt32(cmbClass.SelectedValue.ToString());
                TermUid = Convert.ToInt32(cmbTerm.SelectedValue.ToString());
            }
            else if(cmbClass.SelectedIndex == -1 && cmbTerm.SelectedIndex == -1)
            {
                 Tag = 2;
                classId = 0;
                TermUid = 0;
            }
            else
            {
                Tag = 1;
                classId = Convert.ToInt32(cmbClass.SelectedValue.ToString());
                TermUid = Convert.ToInt32(cmbTerm.SelectedValue.ToString()); ;
            }

            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("SPFeeTempRecipt", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@ClassId", SqlDbType.Int).Value = classId;
            da.SelectCommand.Parameters.Add("@TermID", SqlDbType.Int).Value = TermUid;
            da.SelectCommand.Parameters.Add("@SelectDate", SqlDbType.DateTime).Value = Convert.ToDateTime(dtpFromDate.Text);
            da.SelectCommand.Parameters.Add("@Tag", SqlDbType.Int).Value = Tag;
            da.SelectCommand.Connection = conn;
            DataSet ds = new DataSet();
            da.Fill(ds, "FeeReceipt");
            var id = string.Empty;
            if (ds.Tables["FeeReceipt"].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables["FeeReceipt"].Rows.Count; i++)
                {
                    if (id == string.Empty)
                    {
                        id += ds.Tables["FeeReceipt"].Rows[i]["Colluid"].ToString();
                    }
                    else
                    {
                        id += ',' + ds.Tables["FeeReceipt"].Rows[i]["Colluid"].ToString();
                    }
                }
                string Query = "Select Coll_det.Colluid,FeeMast.Descp,Coll_det.paidamt from Coll_det inner join FeeMast on Coll_det.feeid = FeeMast.Fid Where Coll_det.paidamt <> '0.00' and Coll_det.Colluid  in(" + id + ")order by Coll_det.Colluid";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter dadet = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dadet.Fill(dt);
                doc.Load(Application.StartupPath + "\\Reports\\CryReceiptBulk.rpt");
                //doc.Load(@"D:\Karthik\Vschool\SkoolRPWD\SkoolRPWD\TC\CryReceiptBulk.rpt");
                doc.SetDataSource(ds);
                doc.Subreports["FeeReciptDetails.rpt"].SetDataSource(dt);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc;
            }
            else
            {
                MessageBox.Show("No Records Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbTerm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                cmbTerm.SelectedIndex = -1;
            }
        }

        private void CmbClass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                cmbClass.SelectedIndex = -1;
            }
        }
    }
}
