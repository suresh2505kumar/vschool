﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace SkoolRPWD
{
    public partial class FrmCRViewer2 : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer2;
        private Container components = null;
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        ReportDocument doc = new ReportDocument();
        public FrmCRViewer2()
        {
            InitializeComponent();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            if (Module.Dtype == 25)
            {
                FrmCRViewer2 Crv = new FrmCRViewer2();
                createReport11();
            }
            else if (Module.Dtype == 35)
            {
                FrmCRViewer2 Crv = new FrmCRViewer2();
                createReport12();
            }

        }
        private void createReport11()
        {
            if (Module.Dtype == 25)
            {
                ReportDocument doc7 = new ReportDocument();


                string sql = "select  'SRVSCHOOL' as SchoolName, 'Trichy' as EducationalDistrict, 'Trichy' as RevenueDistrict, b.SName,a.StudNameTamil,b.FName as FatherName, b.Religion as NationalityReligion,b.Community ,b.Sex,b.dob,b.Identification1 as PersonalIdentification, b.Identification2 as PersonalIdentification1,b.doa as DateofAdmission,a.EMISno as EMISNumber,a.slno as SerialNo,b.Admisno as AdmissionNo,a.TMRcode,a.RegNo as CertificateNo ,a.RegNo as RegNo,c.Class_Desc as StdLeavingSch ,a.qph as PromationHigher,a.ScholorShip as Scholarship,a.MedicalInspection, a.dol as DOLSchl,a.conduct,a.DOTC,Mname,aadharno,feesdue  from TcStudDetails a inner join StudentM b on a.StudId = b.uid  inner join Class_Mast c on b.Classid = c.Cid and b.uid =  " + Module.ColluidPT + "       ";
                SqlDataAdapter da = new SqlDataAdapter(sql, con);
                da.SelectCommand.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                da.Fill(ds, "TcDataHdet");
                doc7.Load(Application.StartupPath + "\\Reports\\srvExcel_sec_Tamil.rpt");

                doc7.SetDataSource(ds);
                crystalReportViewer2.RefreshReport();
                crystalReportViewer2.ReportSource = doc7;
                crystalReportViewer2.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;

            }
        }
        private void createReport12()
        {
            if (Module.Dtype == 35)
            {
                ReportDocument doc8 = new ReportDocument();


                string sql = "select  'SRVSCHOOL' as SchoolName, 'Trichy' as EducationalDistrict, 'Trichy' as RevenueDistrict, b.SName,a.StudNameTamil,b.FName as FatherName, b.Religion as NationalityReligion,b.Community ,b.Sex,b.dob,b.Identification1 as PersonalIdentification, b.Identification2 as PersonalIdentification1,b.doa as DateofAdmission,a.EMISno as EMISNumber,a.slno as SerialNo,b.Admisno as AdmissionNo,a.TMRcode,a.RegNo as CertificateNo ,a.RegNo as RegNo,c.Class_Desc as StdLeavingSch ,a.qph as PromationHigher,a.ScholorShip as Scholarship,a.MedicalInspection, a.dol as DOLSchl,a.conduct,a.DOTC,Mname,aadharno,feesdue  from TcStudDetails a inner join StudentM b on a.StudId = b.uid  inner join Class_Mast c on b.Classid = c.Cid and b.uid =  " + Module.ColluidPT + "       ";
                SqlDataAdapter da = new SqlDataAdapter(sql, con);
                da.SelectCommand.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                da.Fill(ds, "TcDataHdet");
                doc8.Load(Application.StartupPath + "\\Reports\\SrvExcelHSSecondPage.rpt");

                doc8.SetDataSource(ds);
                crystalReportViewer2.RefreshReport();
                crystalReportViewer2.ReportSource = doc8;
                crystalReportViewer2.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;

            }
        }
    }
}
