﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace SkoolRPWD
{
    public partial class FrmStudBus : Form
    {
        public FrmStudBus()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();

        private void FrmStudBus_Load(object sender, EventArgs e)
        {
            LoadFeeGrp();

            Load_grids ();
            LoadRefT();
            //Titlep2();
            DgvT .AllowUserToAddRows = false;
            
            
            this.DgvT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.DgvT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            DgvT.AutoResizeColumns();
            DgvT.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;

            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panadd);
            panadd.Visible = true;

        }


        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;
            con.Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtdesc_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtfuid_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcid_TextChanged(object sender, EventArgs e)
        {

        }

        private void Load_grids()
        {
            try
            {
                con.Close();
                con.Open();

                string qur = "select a.uid,studid,Borduid,concession,a.rent,termuid,feeid,vanuid,SName,Class_Desc ,Refname,Descp ,a.classid,BoardingP ,vanname  from StudentBus a inner join BoardingPM b on a.Borduid=b.buid inner join VanM c on a.vanuid=c.vuid inner join StudentM d on a.studid=d.uid inner join Class_Mast e on a.classid=e.cid inner join FeeRef f on a.termuid=f.Ruid inner join feemast g on a.feeid=g.fid ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);

                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 15;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].DataPropertyName = "studid";
                Dgv.Columns[1].Visible = false;

                Dgv.Columns[2].DataPropertyName = "Borduid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "concession";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "rent";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "termuid";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "feeid";
                Dgv.Columns[6].Visible = false;

                Dgv.Columns[7].DataPropertyName = "vanuid";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "SName";
                Dgv.Columns[8].HeaderText = "Name";
                Dgv.Columns[8].DataPropertyName = "SName";
                Dgv.Columns[8].Width = 300;


                Dgv.Columns[9].DataPropertyName = "Class_Desc";
                Dgv.Columns[9].HeaderText = "Class";
                Dgv.Columns[9].DataPropertyName = "Class_Desc";
                Dgv.Columns[9].Width = 220;


                Dgv.Columns[10].DataPropertyName = "Refname";
                Dgv.Columns[10].HeaderText = "Term";
                Dgv.Columns[10].DataPropertyName = "Refname";
                Dgv.Columns[10].Width = 170;

                Dgv.Columns[11].DataPropertyName = "Descp";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].DataPropertyName = "classid";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].DataPropertyName = "Descp";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].DataPropertyName = "vanname";
                Dgv.Columns[14].Visible = false;


                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }


        private void Titlep2()
        {

            
            
            
            DataGridViewCheckBoxColumn CBColumn = new DataGridViewCheckBoxColumn();
            CBColumn.HeaderText = "Chk";
            CBColumn.FalseValue = "0";
            CBColumn.TrueValue = "1";
            CBColumn.Width = 50;
            CBColumn.ReadOnly = false;
            CBColumn.FillWeight = 10;
            DgvT.Columns.Insert(0, CBColumn);

            con.Close();
            con.Open();
            string qur = "SELECT  Refname, Ruid  FROM  FeeRef";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);


            this.DgvT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.DgvT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            DgvT.AutoGenerateColumns = false;
            DgvT.DataSource = null;
            DgvT.ColumnCount = 3;

            DgvT.Columns[1].Name = "Refname";
            DgvT.Columns[1].HeaderText = "Term";
            DgvT.Columns[1].DataPropertyName = "Refname";
            DgvT.Columns[1].Width = 150;




            DgvT.Columns[2].DataPropertyName = "Ruid";
            DgvT.Columns[2].Visible = false;

            DgvT.DataSource = tap;

        }

        
        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Module.type = 15;
            //    loadput();
            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtclass.Text = "";
            //    txtclass.Focus();
            //}
        }

        private void txtsname_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Module.type = 16;
            //    loadput();
            //    //LoadRefT();

            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtsname.Text = "";
            //    txtsname.Focus();
            //}
        }

        private void loadput()
        {

            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";
            con.Close();
            con.Open();
            if (Module.type == 15)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 16)
            {                
                FunC.Partylistviewcont3("uid", "name", "classid", Module.strsql, this, txtsid, txtsname , txtcid, GBList);
                Module.strsql = "select uid, sname + '/' + Admisno as name,classid from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
            }
            else if (Module.type == 17)
            {
                FunC.Classmaster("Vuid", "Vanname", Module.strsql, this, txtvid , txtvan , GBList);
                Module.strsql = "select Vuid,Vanname from VanM ";
                Module.FSSQLSortStr = "Vanname";
            }
            else if (Module.type == 18)
            {
                FunC.Partylistviewcont3("Buid", "BoardingP", "Rent", Module.strsql, this, txtbid, txtboar, txtrent , GBList);
                Module.strsql = "select Buid,BoardingP,Rent from BoardingPM ";
                Module.FSSQLSortStr = "BoardingP";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 15)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }

            else if (Module.type == 16)
            {
                dt.Columns[1].Width = 261;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }

            else if (Module.type == 17)
            {
                dt.Columns[1].Width = 265;
                dt.Columns[0].Visible = false;
            }

            else if (Module.type == 18)
            {
                dt.Columns[1].Width = 265;
                dt.Columns[0].Visible = false;
                dt.Columns[2].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 15)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 16)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }
                else if (Module.type == 17)
                {
                    dt.Columns[Module.i].HeaderText = "Van Name";
                }
                else if (Module.type == 18)
                {
                    dt.Columns[Module.i].HeaderText = "Boarding Point";
                }

                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadFeeGrp()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Fid,descp FROM feemast where fid=1";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cbofees.DataSource = null;
            cbofees.DataSource = tap;
            cbofees.DisplayMember = "descp";
            cbofees.ValueMember = "Fid";
            cbofees.SelectedIndex = -1;
            con.Close();

        }

        public void clearTxt()
        {
            txtvan .Text = "";
            txtboar .Text = "";
            txtamt.Text = "";
            txtclass.Text = "";
            txtsname .Text = "";

            cbofees.SelectedIndex = -1;
            

        }

        private void txtvan_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Module.type = 17;
            //    loadput();
            //    //LoadRefT();

            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtvan.Text = "";
            //    txtvan.Focus();
            //}
        }

        private void txtboar_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Module.type = 18;
            //    loadput();
            //    //LoadRefT();

            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtvan.Text = "";
            //    txtvan.Focus();
            //}
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;

            clearTxt();
            

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;    
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;

            btnsave.Visible = true;
            btnaddrcan.Visible = true;


            btnsave.Text = "Save";
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")  
            {
                con.Close();
                con.Open();
                //string qur = "select * from BoardingPM where  BoardingP='" + txtboard.Text + "' ";
                //SqlCommand cmd1 = new SqlCommand(qur, con);
                //SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                //DataTable tap = new DataTable();
                //apt1.Fill(tap);
                //con.Close();
                //if (tap.Rows.Count == 0)
                //{


                string ar = "select c.FSuid  from feestructurem a inner join feestructclass c on a.FSuid =c.FSuid where classuid =" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + "";
                SqlCommand cmnd = new SqlCommand(ar, con);
                int fsuid = (int)cmnd.ExecuteScalar();
                
                
                
                string res = "insert into StudentBus values(@studid,@Borduid,@Concession,@Rent,@Termuid,@feeid,@Vanuid,@classid)";
                    //con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@studid", SqlDbType.NVarChar).Value = txtsid.Text;
                    cmd.Parameters.AddWithValue("@Borduid", SqlDbType.NVarChar).Value = txtbid.Text;
                    if (txtamt.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@Concession", SqlDbType.NVarChar).Value = 0;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Concession", SqlDbType.NVarChar).Value = txtamt.Text;
                    }                    
                    cmd.Parameters.AddWithValue("@Rent", SqlDbType.NVarChar).Value = txtrent.Text;

                    cmd.Parameters.AddWithValue("@Termuid", SqlDbType.NVarChar).Value = cboTerm.SelectedValue;
                    
                    cmd.Parameters.AddWithValue("@feeid", SqlDbType.NVarChar).Value = cbofees .SelectedValue ;
                    cmd.Parameters.AddWithValue("@Vanuid", SqlDbType.NVarChar).Value = txtvid.Text;
                    cmd.Parameters.AddWithValue("@classid", SqlDbType.NVarChar).Value = txtcid .Text;

                    cmd.ExecuteNonQuery();

                    string qur1 = "select  studid,'' as Feesturid,feeid,Rent as Rent, concession,(Rent-concession) as finalamt,isnull('',0) as paidamt,termuid,classid as classuid from StudentBus where studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + "";
                    cmd = new SqlCommand(qur1, con);
                    adpt = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();
                    //adpt.SelectCommand = cmd;
                    adpt.Fill(dt);


                    if (dt.Rows.Count > 0)
                    {
                        DataTable DtCollection = new DataTable();
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "paidamt", DataType = typeof(decimal) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                        DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(Int32) });

                        foreach (DataRow row1 in dt.Rows)
                        {
                            DataRow row = DtCollection.NewRow();
                            row["studid"] = row1["studid"];
                            row["Feesturid"] = fsuid ;
                            row["feeid"] = row1["feeid"];
                            row["feeamt"] = row1["Rent"];
                            row["concession"] = row1["concession"]; ;
                            row["finalamt"] = row1["Rent"];
                            row["paidamt"] = 0;
                            row["termuid"] = row1["termuid"];
                            row["classid"] = row1["classuid"];
                            DtCollection.Rows.Add(row);
                        }


                        SqlCommand cmnd1 = new SqlCommand();
                        cmnd1.Connection = con;
                        con.Close();
                        con.Open();

                        for (int i = 0; i < DtCollection.Rows.Count; i++)
                        {

                            cmnd.CommandText = "exec Insert_PLF2 " + DtCollection.Rows[i]["studid"].ToString() + "," + DtCollection.Rows[i]["Feesturid"].ToString() + "," + DtCollection.Rows[i]["feeid"].ToString() + "," + DtCollection.Rows[i]["feeamt"].ToString() + "," + DtCollection.Rows[i]["concession"].ToString() + "," + DtCollection.Rows[i]["finalamt"].ToString() + "," + DtCollection.Rows[i]["paidamt"].ToString() + "," + DtCollection.Rows[i]["termuid"].ToString() + "," + DtCollection.Rows[i]["classid"].ToString() + "";

                            cmnd.ExecuteNonQuery();
                        }



                    }


                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                    clearTxt ();
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grids();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;

                }
                //else
                //{
                //    MessageBox.Show("Enterd the Details are already Exist");
                //    con.Close();
                //    cmd1.Dispose();
                //    txtboard.Text = "";
                //    txtboard.Focus();
                //}

            //}
            else
            {
                con.Close();
                con.Close();
                //string qur = "select * from StudentBus where BoardingP='" + txtboard.Text + "' and Buid <> " + txtuid.Text;
                //con.Open();
                //SqlCommand cmd1 = new SqlCommand(qur, con);
                //SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                //DataTable tap = new DataTable();
                //apt1.Fill(tap);
                //con.Close();
                //if (tap.Rows.Count == 0)
                //{
                string QueryUpdate = "Update StudentBus set Borduid=" + txtbid.Text + ",rent=" + txtrent.Text + ",Concession=" + txtamt.Text + ",Termuid=" + cboTerm.SelectedValue + ",feeid=" + cbofees.SelectedValue + " ,Vanuid=" + txtvid.Text + ",classid=" + txtcid.Text + " where   uid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Close();
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    clearTxt();
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grids();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                //else
                //{
                //    MessageBox.Show("Enterd the Details are already Exist");
                //    con.Close();
                //    cmd1.Dispose();
                //    return;
                //}

                btnsave.Text = "Save";

            }

        private void DgvT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < DgvT.Rows.Count - 1; i++)
            {

                if (DgvT.Rows[i].Cells[0].Value == null)
                {
                    DgvT.Rows[i].Cells[0].Value = true;
                }

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;


            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;

            btnsave.Visible = true;
            btnaddrcan.Visible = true;

            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtsid.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtbid .Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtamt.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtrent.Text = Dgv.Rows[i].Cells[4].Value.ToString();         

            
            txtvid.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            txtsname .Text = Dgv.Rows[i].Cells[8].Value.ToString();
            txtclass .Text = Dgv.Rows[i].Cells[9].Value.ToString();
            cboTerm.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cbofees.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtcid .Text = Dgv.Rows[i].Cells[12].Value.ToString();
            txtboar .Text = Dgv.Rows[i].Cells[13].Value.ToString();
            txtvan .Text = Dgv.Rows[i].Cells[14].Value.ToString();

            btnsave.Text = "Update";
        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 15;
            loadput();
        }

        private void txtsname_MouseClick(object sender, MouseEventArgs e)
        {


            if (txtclass.Text == "")
            {
                MessageBox.Show("Select The Class Name ");
                txtclass.Focus();
                return;
            }
            
            
            Module.type = 16;
            loadput();
        }

        private void txtvan_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 17;
            loadput();
        }

        private void txtboar_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 18;
            loadput();
        }

        private void cbofees_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
       



    }
}
