﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;


namespace SkoolRPWD
{
    public partial class FrmFeeRecp : Form
    {
        public FrmFeeRecp()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        bool CheckPriviousBal = false;
        SqlCommand qur = new SqlCommand();

        bool rest = false;

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtstud.Text == "")
            {
                MessageBox.Show("Enter the student name");
                txtstud.Focus();
                return;
            }

            if (txtBAmt.Text == "")
            {
                MessageBox.Show("Enter the Amount to be Paid");
                txtstud.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                PaidAmt(Convert.ToInt32(txtBAmt.Text), Convert.ToInt32(txtAPaid.Text));
                if (rest == true)
                {
                    return;
                }
                string query11 = "update Recptno set Rno=Rno +1";
                SqlCommand cmd11 = new SqlCommand(query11, con);
                cmd11.ExecuteNonQuery();

                string qr = "select Rno from  Recptno";
                SqlCommand cmd111 = new SqlCommand(qr, con);
                int uid1 = (int)cmd111.ExecuteScalar();
                txtrcpt.Text = uid1.ToString();
                {
                    string query = " select  a.FSuid  from FeeStructureM a inner join FeeStructclass b on a.FSuid =b.fsuid where termuid=" + cboTerm.SelectedValue + " and classuid=" + txtcid.Text + "";
                    SqlCommand cmd1 = new SqlCommand(query, con);
                    int FSMuid = (int)cmd1.ExecuteScalar();

                    string res = "insert into Coll_Mast values(@Recptno,@Colldate,@studid,@Coll_amt,@Fine_amt,@Coll_mode,@DDdate,@DDBank,@DDNo,@Tag,@Feesturid,@termuid,@Totalamt,@classid)";

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Recptno", SqlDbType.Int).Value = txtrcpt.Text;
                    cmd.Parameters.AddWithValue("@Colldate", SqlDbType.Int).Value = dtp.Value;
                    cmd.Parameters.AddWithValue("@studid", SqlDbType.Int).Value = txtsid.Text;
                    cmd.Parameters.AddWithValue("@Coll_amt", SqlDbType.Decimal).Value = txtBAmt.Text;
                    if (txtfine.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@Fine_amt", SqlDbType.Decimal).Value = 0;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Fine_amt", SqlDbType.Decimal).Value = txtfine.Text;
                    }
                    cmd.Parameters.AddWithValue("@Coll_mode", SqlDbType.NVarChar).Value = cbomode.Text;
                    cmd.Parameters.AddWithValue("@DDdate", SqlDbType.Decimal).Value = DBNull.Value;
                    cmd.Parameters.AddWithValue("@DDBank", SqlDbType.Decimal).Value = DBNull.Value;
                    cmd.Parameters.AddWithValue("@DDNo", SqlDbType.NVarChar).Value = "";
                    cmd.Parameters.AddWithValue("@Tag", SqlDbType.NVarChar).Value = "A";
                    cmd.Parameters.AddWithValue("@Feesturid", SqlDbType.Decimal).Value = FSMuid;
                    cmd.Parameters.AddWithValue("@termuid", SqlDbType.Decimal).Value = cboTerm.SelectedValue;
                    cmd.Parameters.AddWithValue("@Totalamt", SqlDbType.Decimal).Value = txttotal.Text;
                    cmd.Parameters.AddWithValue("@classid", SqlDbType.NVarChar).Value = txtcid.Text;

                    cmd.ExecuteNonQuery();


                    string strSQL = "select Colluid from Coll_Mast where termuid=" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + " and recptno=" + txtrcpt.Text + "";
                    cmd = new SqlCommand(strSQL, con);
                    int uid = (int)cmd.ExecuteScalar();

                    string Amount = convertToString(Convert.ToInt32(txtAPaid.Text));
                    txtLpt.Text = Amount + " Only";

                    string strSQL11 = "delete from NotoWords where Colluid=" + uid + "";
                    SqlCommand cm = new SqlCommand(strSQL11, con);
                    cm.ExecuteNonQuery();

                    string strSQL12 = "insert into NotoWords(words,colluid) values(' " + txtLpt.Text + "'," + uid + ")";
                    SqlCommand cm1 = new SqlCommand(strSQL12, con);
                    cm1.ExecuteNonQuery();

                    for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                    {
                        string strSQL1 = "select puid from PLFType where studid=" + txtsid.Text + " and termuid =" + cboTerm.SelectedValue + " and classid=" + txtcid.Text + " and feeid=" + Dgvlist.Rows[i].Cells[0].Value + "";
                        cmd = new SqlCommand(strSQL1, con);
                        int PLfuid = (int)cmd.ExecuteScalar();
                        res = "insert into Coll_det (Colluid,feeid,paidamt,feeDUid,PLFuid) values(" + uid + "," + Dgvlist.Rows[i].Cells[0].Value + ",  " + Dgvlist.Rows[i].Cells[7].Value + "," + Dgvlist.Rows[i].Cells[8].Value + "," + PLfuid + ")";
                        SqlCommand scmd = new SqlCommand(res, con);
                        scmd.ExecuteNonQuery();
                        string strSQL2 = "update PLFType set paidamt =paidamt+" + Dgvlist.Rows[i].Cells[7].Value + " where studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and termuid =" + cboTerm.SelectedValue + " and feeid=" + Dgvlist.Rows[i].Cells[0].Value + "";
                        SqlCommand scmd1 = new SqlCommand(strSQL2, con);
                        scmd1.ExecuteNonQuery();
                    }
                    MessageBox.Show(" Saved ");
                    clearTxt();
                    con.Close();
                    Dgvlist.Rows.Clear();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    cmdprt.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
            }
            btnsave.Text = "Save";
        }

        public void clearTxt()
        {
            txtcid.Text = "";
            cboTerm.SelectedIndex = -1;
            txtstud.Text = "";
            txttotal.Text = "";
            txtfine.Text = "";
            txtclass.Text = "";
            txtAPaid.Text = "";
            txtpaid.Text = "";
            txtrcpt.Text = "";
            txtBAmt.Text = "";
            cbomode.SelectedIndex = -1;
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            txtcid.Text = "";
            clearTxt();
            Dgvlist.Rows.Clear();
            Load_grid();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            cmdprt.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnsave.Text = "Save";
        }

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;
            con.Close();
        }

        private void FrmFeeRecp_Load(object sender, EventArgs e)
        {
            LoadRefT();
            Titlep();
            qur.Connection = con;            
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panadd);
            panadd.Visible = true;
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            cmdprt.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;

            txtclass.Focus();

            btnsave.Text = "Save";
        }

        private void Titlep()
        {
            Dgvlist.ColumnCount = 9;
            Dgvlist.Columns[0].Name = "fid";
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].DataPropertyName = "fid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "descp";
            Dgvlist.Columns[1].HeaderText = "Fee Name";
            Dgvlist.Columns[1].DataPropertyName = "descp";
            Dgvlist.Columns[1].Width = 170;

            Dgvlist.Columns[2].Name = " Amount";
            Dgvlist.Columns[2].Width = 120;

            Dgvlist.Columns[3].Name = "Concession";
            Dgvlist.Columns[3].Width = 110;

            Dgvlist.Columns[4].Name = "To be Paid";
            Dgvlist.Columns[4].Width = 110;

            Dgvlist.Columns[5].Name = "Paid Already";
            Dgvlist.Columns[5].Width = 110;

            Dgvlist.Columns[6].Name = "Due Amount";
            Dgvlist.Columns[6].Width = 110;

            Dgvlist.Columns[7].Name = "Pay Now";
            Dgvlist.Columns[7].Width = 110;

            Dgvlist.Columns[8].DataPropertyName = "FDuid";
            Dgvlist.Columns[8].Visible = false;

            var dgTextBoxCol = new DataGridViewTextBoxColumn();
            dgTextBoxCol.Name = "Amount";
            dgTextBoxCol.DefaultCellStyle.Format = "N0";
        }

        private void Titlep1()
        {
            Dgv.AutoGenerateColumns = false;
            Dgv.DataSource = null;
            Dgv.ColumnCount = 9;

            Dgv.Columns[0].Name = "Colluid";
            Dgv.Columns[0].HeaderText = "Colluid";
            Dgv.Columns[0].DataPropertyName = "Colluid";
            Dgv.Columns[0].Visible = false;

            Dgv.Columns[1].Name = "recptno";
            Dgv.Columns[1].HeaderText = "Receipt No";
            Dgv.Columns[1].DataPropertyName = "recptno";
            Dgv.Columns[1].Width = 90;

            Dgv.Columns[2].DataPropertyName = "refname";
            Dgv.Columns[2].HeaderText = "Term";
            Dgv.Columns[2].DataPropertyName = "refname";
            Dgv.Columns[2].Width = 95;

            Dgv.Columns[3].DataPropertyName = "Class_Desc";
            Dgv.Columns[3].HeaderText = "Class";
            Dgv.Columns[3].DataPropertyName = "Class_Desc";
            Dgv.Columns[3].Width = 80;

            Dgv.Columns[4].DataPropertyName = "Admisno";
            Dgv.Columns[4].HeaderText = "Admisno";
            Dgv.Columns[4].DataPropertyName = "Admisno";
            Dgv.Columns[4].Width = 120;

            Dgv.Columns[5].DataPropertyName = "sname";
            Dgv.Columns[5].HeaderText = "Name";
            Dgv.Columns[5].DataPropertyName = "sname";
            Dgv.Columns[5].Width = 250;

            Dgv.Columns[6].DataPropertyName = "coll_amt";
            Dgv.Columns[6].HeaderText = "Amount";
            Dgv.Columns[6].DataPropertyName = "coll_amt";
            Dgv.Columns[6].Width = 150;

            Dgv.Columns[7].DataPropertyName = "studid";
            Dgv.Columns[7].Visible = false;

            Dgv.Columns[8].DataPropertyName = "termuid";
            Dgv.Columns[8].Visible = false;
            Dgv.Columns[3].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
        }

        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
            Dgvlist.Refresh();
            Dgvlist.Rows.Clear();
            txttotal.Text = "";
            txtfine.Text = "";
            txtpaid.Text = "";
            txtAPaid.Text = "";
            txtBAmt.Text = "";

            if (e.KeyCode == Keys.Enter)
            {
                Module.type = 5;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtclass.Text = "";
                txtclass.Focus();
            }
        }

        private void txtstud_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";

            con.Close();
            con.Open();

            if (Module.type == 5)
            {
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.cid=b.classuid where active=1";
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 6)
            {
                FunC.Partylistviewcont3("uid", "name", "classid", Module.strsql, this, txtsid, txtstud, txtcid, GBList);
                if (checkBox2.Checked == true)
                {
                    Module.strsql = "select uid, Admisno  + '/' + sname as name,classid from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
                }
                else
                {
                    Module.strsql = "select uid, sname + '/' + Admisno as name,classid from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
                }
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 5)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            else if (Module.type == 6)
            {
                dt.Columns[1].Width = 261;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 5)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 6)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }

                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void Loadlistgrid()
        {
            if (CheckPriviousBal == false)
            {
                if (txtcid.Text != "")
                {
                    txtfine.Text = "";
                    txtAPaid.Text = "";
                    string qur = "select a.Feeid,descp,CAST(feeamt AS INT)feeamt,CAST(concession AS INT)concession,CAST(finalamt AS INT)finalamt,CAST(paidamt AS INT)paid,(cast(finalamt  AS INT)-cast(paidamt  AS INT))dueamt ,'',isnull(c.FDuid,0) as FDuid from PLFType a inner join feemast b on a.feeid =b.Fid  left join FeeStructureD c on c.feeid=a.feeid and a.Feesturid=c.FSuid   where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and a.studid = " + txtsid.Text + " order by B.forder ";
                    SqlCommand cmd1 = new SqlCommand(qur, con);
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd1);
                    DataSet dt1 = new DataSet();
                    adpt1.SelectCommand = cmd1;
                    adpt1.Fill(dt1);
                    Titlep();
                    int sum = 0;
                    int sum1 = 0;
                    int sum2 = 0;
                    for (int i = 0; i < dt1.Tables[0].Rows.Count; i++)
                    {
                        i = Dgvlist.Rows.Add();
                        Dgvlist.Rows[i].Cells[0].Value = dt1.Tables[0].Rows[i][0].ToString();
                        Dgvlist.Rows[i].Cells[1].Value = dt1.Tables[0].Rows[i][1].ToString();
                        Dgvlist.Rows[i].Cells[2].Value = dt1.Tables[0].Rows[i][2].ToString();
                        Dgvlist.Rows[i].Cells[3].Value = dt1.Tables[0].Rows[i][3].ToString();
                        Dgvlist.Rows[i].Cells[4].Value = dt1.Tables[0].Rows[i][4].ToString();
                        Dgvlist.Rows[i].Cells[5].Value = dt1.Tables[0].Rows[i][5].ToString();
                        Dgvlist.Rows[i].Cells[6].Value = dt1.Tables[0].Rows[i][6].ToString();
                        Dgvlist.Rows[i].Cells[7].Value = 0;
                        Dgvlist.Rows[i].Cells[8].Value = dt1.Tables[0].Rows[i][8].ToString();
                        sum += Convert.ToInt32(dt1.Tables[0].Rows[i][6].ToString());
                        txtAPaid.Text = sum.ToString();
                        txtAPaid.TextAlign = HorizontalAlignment.Right;
                        sum1 += Convert.ToInt32(dt1.Tables[0].Rows[i][4].ToString());
                        txttotal.Text = sum1.ToString();
                        txttotal.TextAlign = HorizontalAlignment.Right;
                        sum2 += Convert.ToInt32(Dgvlist.Rows[i].Cells[5].Value.ToString());
                        txtpaid.Text = sum2.ToString();
                        txtpaid.TextAlign = HorizontalAlignment.Right;
                    }
                }
            }
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckPriviousBal = false;
            Dgvlist.Rows.Clear();
            CheckPriviousBalance();
            if (CheckPriviousBal == false)
            {
                Loadlistgrid();
            }
            else
            {
                MessageBox.Show("Previous Term fee is pending", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTerm.SelectedIndex = 0;
            }
        }

        protected void CheckPriviousBalance()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "CheckPriviousBalance";
                cmd.Parameters.AddWithValue("@TermUid", cboTerm.SelectedValue);
                cmd.Parameters.AddWithValue("@StudUid", Convert.ToInt32(txtsid.Text));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    CheckPriviousBal = false;
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        decimal amt = Convert.ToDecimal(dt.Rows[i]["Due"].ToString());
                        if (amt == 0)
                        {
                            CheckPriviousBal = false;
                        }
                        else
                        {
                            CheckPriviousBal = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtclass_TextChanged(object sender, EventArgs e)
        {

        }

        private void Dgvlist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txttotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtfine_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtAPaid_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtpaid_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtstud_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBAmt_TextChanged(object sender, EventArgs e)
        {
            return;
        }
        public void PaidAmt(int amt, int tot)
        {
            if (amt > tot)
            {
                MessageBox.Show("Not Exceed the Total amount", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtBAmt.Text = "";
                txtBAmt.Focus();
                rest = true;
                return;
            }
            if (txtBAmt.Text != "")
            {
                int sum2 = 0;
                for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                {
                    if (Convert.ToInt32(txtBAmt.Text) <= Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value) && i == 0)
                    {
                        int s = Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value) - Convert.ToInt32(txtBAmt.Text);
                        if (s >= 0)
                        {
                            Dgvlist.Rows[i].Cells[7].Value = Convert.ToInt32(txtBAmt.Text);
                        }
                    }
                    else
                    {
                        int s1 = Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value) - Convert.ToInt32(txtBAmt.Text);
                        if (s1 <= 0)
                        {
                            for (int j = 1; j < Dgvlist.Rows.Count - 1; j++)
                            {
                                if (j == 1)
                                {
                                    int s11 = Convert.ToInt32(txtBAmt.Text) - Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value);
                                    if (Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value) >= s11)
                                    {
                                        Dgvlist.Rows[i].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value);
                                        int ss = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value) - s11;
                                        Dgvlist.Rows[j].Cells[7].Value = s11;
                                    }
                                    else
                                    {
                                        Dgvlist.Rows[i].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[i].Cells[6].Value);
                                        int s3 = s11 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                        Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                        j++;
                                        if (s3 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                        {
                                            int ss = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value) - s3;
                                            if (ss == 0)
                                            {
                                                Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                            }
                                            else
                                            {
                                                Dgvlist.Rows[j].Cells[7].Value = s3;
                                            }
                                        }
                                        else
                                        {
                                            Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                            int s4 = s3 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                            j++;
                                            if (s4 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                            {
                                                int ss1 = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value) - s4;
                                                if (ss1 == 0)
                                                {
                                                    Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                }
                                                else
                                                {
                                                    Dgvlist.Rows[j].Cells[7].Value = s4;
                                                }
                                            }
                                            else
                                            {
                                                Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                int s5 = s4 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                j++;
                                                if (s5 >= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                {
                                                    int ss1 = s5 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                    if (ss1 == 0)
                                                    {
                                                        Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                    }
                                                    else
                                                    {
                                                        Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                        int ss11 = s5 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                        j++;
                                                        if (ss11 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                        {
                                                            Dgvlist.Rows[j].Cells[7].Value = ss11;
                                                        }
                                                        else
                                                        {
                                                            Dgvlist.Rows[j].Cells[7].Value = Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                            int ss61 = ss11 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                            j++;
                                                            if (ss61 >= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                            {
                                                                Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                int ss62 = ss61 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                j++;
                                                                if (ss62 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                {
                                                                    Dgvlist.Rows[j].Cells[7].Value = ss62;
                                                                }
                                                                else
                                                                {
                                                                    Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                    int ss63 = ss62 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                    j++;
                                                                    if (ss63 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                    {
                                                                        Dgvlist.Rows[j].Cells[7].Value = ss63;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                        int ss64 = ss63 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                        j++;
                                                                        if (ss64 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                        {
                                                                            Dgvlist.Rows[j].Cells[7].Value = ss64;
                                                                        }
                                                                        else
                                                                        {
                                                                            Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                            int ss65 = ss64 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                            j++;
                                                                            if (ss65 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                            {
                                                                                Dgvlist.Rows[j].Cells[7].Value = ss65;
                                                                            }
                                                                            else
                                                                            {
                                                                                Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                                int ss66 = ss65 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                                j++;
                                                                                if (ss66 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                                {
                                                                                    Dgvlist.Rows[j].Cells[7].Value = ss66;
                                                                                }
                                                                                else
                                                                                {
                                                                                    Dgvlist.Rows[j].Cells[7].Value = Dgvlist.Rows[j].Cells[6].Value;
                                                                                    int ss67 = ss66 - Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value);
                                                                                    j++;
                                                                                    if (ss67 <= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                                                    {
                                                                                        Dgvlist.Rows[j].Cells[7].Value = ss67;
                                                                                    }
                                                                                    else
                                                                                    {

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Dgvlist.Rows[j].Cells[7].Value = ss61;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Dgvlist.Rows[j].Cells[7].Value = s5;
                                                    j++;
                                                    if (s5 >= Convert.ToInt32(Dgvlist.Rows[j].Cells[6].Value))
                                                    {
                                                    }
                                                    else
                                                    {
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i = Dgvlist.Rows.Count + 1;
                }
                for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                {
                    sum2 += Convert.ToInt32(Dgvlist.Rows[i].Cells[7].Value.ToString());
                    txtAPaid.Text = sum2.ToString();
                    txtAPaid.TextAlign = HorizontalAlignment.Right;
                }
            }
            else
            {
                MessageBox.Show("Can't paid ");
                txttotal.Text = "";
                txtfine.Text = "";
                txtpaid.Text = "";
                txtAPaid.Text = "";
                txtBAmt.Text = "";
            }
        }
        private void txtBAmt_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtBAmt_Enter(object sender, EventArgs e)
        {

        }

        private void txtBAmt_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtBAmt_KeyPress(object sender, KeyPressEventArgs e)
        {

        }


        private void Load_grid()
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                string theDate = dateTimePicker1.Value.ToString("dd/MM/yyyy");
                string qur = "select  a.Colluid ,recptno,refname,Class_Desc,Admisno ,sname,cast(coll_amt as int)coll_amt,a.studid,a.termuid   from coll_mast a inner join StudentM c on a.studid=c.uid inner join feeref d on a.termuid=d.Ruid inner join class_mast e on a.classid=e.Cid where CONVERT(VARCHAR, colldate, 103)='" + theDate + "' and a.tag='A' order by convert(integer,admisno,105)";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);

                if (tap.Rows.Count > 0)
                {
                    Dgv.AutoGenerateColumns = false;
                    Dgv.DataSource = null;
                    Dgv.ColumnCount = 9;

                    Dgv.Columns[0].Name = "Colluid";
                    Dgv.Columns[0].HeaderText = "Colluid";
                    Dgv.Columns[0].DataPropertyName = "Colluid";
                    Dgv.Columns[0].Visible = false;

                    Dgv.Columns[1].Name = "recptno";
                    Dgv.Columns[1].HeaderText = "Receipt No";
                    Dgv.Columns[1].DataPropertyName = "recptno";
                    Dgv.Columns[1].Width = 90;

                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].HeaderText = "Term";
                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].Width = 95;


                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].HeaderText = "Class";
                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].Width = 80;


                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].HeaderText = "Admisno";
                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].Width = 120;


                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].HeaderText = "Name";
                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].Width = 200;

                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].HeaderText = "Amount";
                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].Width = 120;

                    Dgv.Columns[7].DataPropertyName = "studid";
                    Dgv.Columns[7].Visible = false;

                    Dgv.Columns[8].DataPropertyName = "termuid";
                    Dgv.Columns[8].Visible = false;
                    Dgv.DataSource = tap;

                    Dgv.Columns[3].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                }
                else
                {

                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }
        private void Load_cancel()
        {
            try
            {
                con.Close();
                con.Open();
                string theDate = dateTimePicker1.Value.ToString("dd/MM/yyyy");
                string qur = "select  a.Colluid ,recptno,refname,Class_Desc,Admisno ,sname,cast(coll_amt as int)coll_amt,a.studid,a.termuid   from coll_mast a inner join StudentM c on a.studid=c.uid inner join feeref d on a.termuid=d.Ruid inner join class_mast e on a.classid=e.Cid where CONVERT(VARCHAR, colldate, 103)='" + theDate + "' and c.tag='C' order by convert(integer,admisno,105)";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    Dgv.AutoGenerateColumns = false;
                    Dgv.DataSource = null;
                    Dgv.ColumnCount = 9;

                    Dgv.Columns[0].Name = "Colluid";
                    Dgv.Columns[0].HeaderText = "Colluid";
                    Dgv.Columns[0].DataPropertyName = "Colluid";
                    Dgv.Columns[0].Visible = false;

                    Dgv.Columns[1].Name = "recptno";
                    Dgv.Columns[1].HeaderText = "Receipt No";
                    Dgv.Columns[1].DataPropertyName = "recptno";
                    Dgv.Columns[1].Width = 90;

                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].HeaderText = "Term";
                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].Width = 95;

                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].HeaderText = "Class";
                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].Width = 80;

                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].HeaderText = "Admisno";
                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].Width = 120;

                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].HeaderText = "Name";
                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].Width = 200;

                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].HeaderText = "Amount";
                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].Width = 120;
                    Dgv.Columns[7].DataPropertyName = "studid";
                    Dgv.Columns[7].Visible = false;

                    Dgv.Columns[8].DataPropertyName = "termuid";
                    Dgv.Columns[8].Visible = false;

                    Dgv.DataSource = tap;
                    Dgv.Columns[3].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                }
                else
                {
                    Titlep1();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }
        private void cboTerm_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboTerm_Click(object sender, EventArgs e)
        {

        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            qur.CommandText = "delete from  Printtmp";
            qur.ExecuteNonQuery();
            int i = Dgv.SelectedCells[0].RowIndex;
            qur.CommandText = "insert into Printtmp select a.Colluid,Recptno,colldate as dt,Coll_amt,b.feeid,b.paidamt ,c.Refname,d.Admisno ,d.SName,a.termuid,e.Descp,(a.Totalamt-a.coll_amt) as Balamt,Class_Desc,Totalamt,words  from coll_mast a inner join coll_det b on a.Colluid=b.Colluid inner join FeeRef  c on a.termuid=c.Ruid   inner join StudentM d on a.studid =d.uid inner join feemast e on b.feeid=e.Fid inner join class_mast f on d.Classid=f.Cid inner join NotoWords z on a.colluid =z.colluid where a.Colluid=" + Dgv.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            Module.strsql = "select * from Printtmp";
            CRViewer crv = new CRViewer();
            crv.Show();
            con.Close();
        }

        public string convertToString(int number)
        {
            string[] numbers = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
            string[] tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

            char[] digits = number.ToString().ToCharArray();
            string words = null;

            if (number >= 0 && number <= 19)
            {
                words = words + numbers[number];
            }
            else if (number >= 20 && number <= 99)
            {
                int firstDigit = (int)Char.GetNumericValue(digits[0]);
                int secondPart = number % 10;

                words = words + tens[firstDigit];

                if (secondPart > 0)
                {
                    words = words + " " + convertToString(secondPart);
                }
            }
            else if (number >= 100 && number <= 999)
            {
                int firstDigit = (int)Char.GetNumericValue(digits[0]);
                int secondPart = number % 100;

                words = words + numbers[firstDigit] + " Hundred";

                if (secondPart > 0)
                {
                    words = words + " and " + convertToString(secondPart);
                }
            }
            else if (number >= 1000 && number <= 19999)
            {
                int firstPart = (int)Char.GetNumericValue(digits[0]);
                if (number >= 10000)
                {
                    string twoDigits = digits[0].ToString() + digits[1].ToString();
                    firstPart = Convert.ToInt16(twoDigits);
                }
                int secondPart = number % 1000;

                words = words + numbers[firstPart] + " Thousand";

                if (secondPart > 0 && secondPart <= 99)
                {
                    words = words + " and " + convertToString(secondPart);
                }
                else if (secondPart >= 100)
                {
                    words = words + " " + convertToString(secondPart);
                }
            }
            else if (number >= 20000 && number <= 999999)
            {
                string firstStringPart = Char.GetNumericValue(digits[0]).ToString() + Char.GetNumericValue(digits[1]).ToString();

                if (number >= 100000)
                {
                    firstStringPart = firstStringPart + Char.GetNumericValue(digits[2]).ToString();
                }

                int firstPart = Convert.ToInt16(firstStringPart);
                int secondPart = number - (firstPart * 1000);

                words = words + convertToString(firstPart) + " Thousand";

                if (secondPart > 0 && secondPart <= 99)
                {
                    words = words + " and " + convertToString(secondPart);
                }
                else if (secondPart >= 100)
                {
                    words = words + " " + convertToString(secondPart);
                }

            }
            else if (number == 1000000)
            {
                words = words + "One million";
            }
            return words;
        }

        private void Dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Dgv.DataSource = null;
            Load_grid();
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            cmdprt.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;

            txtclass.Focus();

            btnsave.Text = "Save";
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do You Want to cancel this Receipt No", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                con.Close();
                con.Open();
                int i = Dgv.SelectedCells[0].RowIndex;

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_CancelFeeCollection";
                cmd.Parameters.AddWithValue("@studid", Dgv.Rows[i].Cells[7].Value.ToString());
                cmd.Parameters.AddWithValue("@Colluid", Dgv.Rows[i].Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                MessageBox.Show(" canceled  ");
            }
            else if (DialogResult == DialogResult.No)
            {

            }
        }

        private void cmdprt_Click_1(object sender, EventArgs e)
        {
            Module.Dtype = 10;
            int i = Dgv.SelectedCells[0].RowIndex;
            Module.ColluidPT = Convert.ToInt32(Dgv.Rows[i].Cells[0].Value.ToString());
            CRViewer crv = new CRViewer();
            crv.Show();
            con.Close();

        }

        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                string theDate = dateTimePicker1.Value.ToString("dd/MM/yyyy");

                string qur = "select  a.Colluid ,recptno,refname,Class_Desc,Admisno ,sname,coll_amt,a.studid,a.termuid   from coll_mast a inner join StudentM c on a.studid=c.uid inner join feeref d on a.termuid=d.Ruid inner join class_mast e on a.classid=e.Cid where recptno like '%" + txtscr.Text + "%' and refname  like '%" + txtscr2.Text + "%' and Class_Desc  like '%" + txtscr3.Text + "%' and Admisno  like '%" + txtscr4.Text + "%' and sname like '%" + txtscr5.Text + "%' and coll_amt like '%" + txtscr6.Text + "%' order by recptno";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);

                if (tap.Rows.Count > 0)
                {
                    Dgv.AutoGenerateColumns = false;
                    Dgv.DataSource = null;
                    Dgv.ColumnCount = 9;

                    Dgv.Columns[0].Name = "Colluid";
                    Dgv.Columns[0].HeaderText = "Colluid";
                    Dgv.Columns[0].DataPropertyName = "Colluid";
                    Dgv.Columns[0].Visible = false;

                    Dgv.Columns[1].Name = "recptno";
                    Dgv.Columns[1].HeaderText = "Receipt No";
                    Dgv.Columns[1].DataPropertyName = "recptno";
                    Dgv.Columns[1].Width = 90;

                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].HeaderText = "Term";
                    Dgv.Columns[2].DataPropertyName = "refname";
                    Dgv.Columns[2].Width = 95;


                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].HeaderText = "Class";
                    Dgv.Columns[3].DataPropertyName = "Class_Desc";
                    Dgv.Columns[3].Width = 80;


                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].HeaderText = "Admisno";
                    Dgv.Columns[4].DataPropertyName = "Admisno";
                    Dgv.Columns[4].Width = 120;


                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].HeaderText = "Name";
                    Dgv.Columns[5].DataPropertyName = "sname";
                    Dgv.Columns[5].Width = 250;

                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].HeaderText = "Amount";
                    Dgv.Columns[6].DataPropertyName = "coll_amt";
                    Dgv.Columns[6].Width = 120;

                    Dgv.Columns[7].DataPropertyName = "studid";
                    Dgv.Columns[7].Visible = false;

                    Dgv.Columns[8].DataPropertyName = "termuid";
                    Dgv.Columns[8].Visible = false;
                    Dgv.DataSource = tap;
                    Dgv.Columns[3].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void GBMain_Enter(object sender, EventArgs e)
        {

        }

        private void txtrcpt_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Dgvlist.Refresh();
            Dgvlist.Rows.Clear();
            txttotal.Text = "";
            txtfine.Text = "";
            txtpaid.Text = "";
            txtAPaid.Text = "";
            txtBAmt.Text = "";
            Module.type = 5;
            loadput();
        }

        private void txtstud_MouseClick(object sender, MouseEventArgs e)
        {

            if (txtclass.Text == "")
            {
                MessageBox.Show("Select The Class Name ");
                txtclass.Focus();
                return;
            }
            Module.type = 6;
            loadput();
        }

        private void cmdprt1_Click(object sender, EventArgs e)
        {
            Module.theDate = dateTimePicker1.Value.ToString("dd/MM/yyyy");
            CRViewer crv = new CRViewer();
            crv.Show();
            con.Close();
        }

        private void chkcan_CheckedChanged(object sender, EventArgs e)
        {
            if (chkcan.Checked == true)
            {
                Load_cancel();
            }
            else if (chkcan.Checked == false)
            {
                Load_grid();
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {               
                string qur = "select a.Feeid,descp,CAST(feeamt AS INT)feeamt,CAST(concession AS INT)concession,CAST(finalamt AS INT)finalamt,CAST(paidamt AS INT)paid,(cast(finalamt  AS INT)-cast(paidamt  AS INT))dueamt ,'',isnull(c.FDuid,0) as FDuid from PLFType a inner join feemast b on a.feeid =b.Fid  left join FeeStructureD c on c.feeid=a.feeid and a.Feesturid=c.FSuid   where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and a.studid = " + txtsid.Text + " order by B.forder ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter adpt1 = new SqlDataAdapter(cmd1);
                DataSet dt1 = new DataSet();
                adpt1.SelectCommand = cmd1;
                adpt1.Fill(dt1);
            }
            catch(Exception)
            {

            }
        }
    }
}
