﻿namespace SkoolRPWD
{
    partial class FrmClassTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClassTransfer));
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.btnser = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnT = new System.Windows.Forms.Button();
            this.btnF = new System.Windows.Forms.Button();
            this.cboto = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbofrom = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.DgvT = new System.Windows.Forms.DataGridView();
            this.btnexit = new System.Windows.Forms.Button();
            this.DgvF = new System.Windows.Forms.DataGridView();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvF)).BeginInit();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.btnsave);
            this.GBMain.Controls.Add(this.btnT);
            this.GBMain.Controls.Add(this.btnF);
            this.GBMain.Controls.Add(this.cboto);
            this.GBMain.Controls.Add(this.label1);
            this.GBMain.Controls.Add(this.cbofrom);
            this.GBMain.Controls.Add(this.label15);
            this.GBMain.Controls.Add(this.DgvT);
            this.GBMain.Controls.Add(this.btnexit);
            this.GBMain.Controls.Add(this.DgvF);
            this.GBMain.Location = new System.Drawing.Point(2, 1);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(642, 504);
            this.GBMain.TabIndex = 1;
            this.GBMain.TabStop = false;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.Azure;
            this.btnser.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(505, 468);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(64, 30);
            this.btnser.TabIndex = 125;
            this.btnser.Text = "Clear";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.btnser_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(443, 468);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(64, 30);
            this.btnsave.TabIndex = 124;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnT
            // 
            this.btnT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnT.Location = new System.Drawing.Point(308, 263);
            this.btnT.Name = "btnT";
            this.btnT.Size = new System.Drawing.Size(28, 24);
            this.btnT.TabIndex = 122;
            this.btnT.Text = "<";
            this.btnT.UseVisualStyleBackColor = true;
            this.btnT.Visible = false;
            this.btnT.Click += new System.EventHandler(this.btnT_Click);
            // 
            // btnF
            // 
            this.btnF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnF.Location = new System.Drawing.Point(308, 222);
            this.btnF.Name = "btnF";
            this.btnF.Size = new System.Drawing.Size(28, 24);
            this.btnF.TabIndex = 121;
            this.btnF.Text = ">";
            this.btnF.UseVisualStyleBackColor = true;
            this.btnF.Click += new System.EventHandler(this.btnF_Click);
            // 
            // cboto
            // 
            this.cboto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboto.FormattingEnabled = true;
            this.cboto.Items.AddRange(new object[] {
            "Nursery",
            "Primary",
            "Secondary",
            "Senior"});
            this.cboto.Location = new System.Drawing.Point(419, 13);
            this.cboto.Name = "cboto";
            this.cboto.Size = new System.Drawing.Size(121, 24);
            this.cboto.TabIndex = 120;
            this.cboto.SelectedIndexChanged += new System.EventHandler(this.cboto_SelectedIndexChanged);
            this.cboto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboto_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(340, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 119;
            this.label1.Text = "Class To";
            // 
            // cbofrom
            // 
            this.cbofrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofrom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofrom.FormattingEnabled = true;
            this.cbofrom.Items.AddRange(new object[] {
            "Nursery",
            "Primary",
            "Secondary",
            "Senior"});
            this.cbofrom.Location = new System.Drawing.Point(91, 15);
            this.cbofrom.Name = "cbofrom";
            this.cbofrom.Size = new System.Drawing.Size(121, 24);
            this.cbofrom.TabIndex = 118;
            this.cbofrom.SelectedIndexChanged += new System.EventHandler(this.cbofrom_SelectedIndexChanged);
            this.cbofrom.Click += new System.EventHandler(this.cbofrom_Click);
            this.cbofrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbofrom_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 16);
            this.label15.TabIndex = 117;
            this.label15.Text = "Class From";
            // 
            // DgvT
            // 
            this.DgvT.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvT.Location = new System.Drawing.Point(342, 50);
            this.DgvT.Name = "DgvT";
            this.DgvT.RowHeadersVisible = false;
            this.DgvT.Size = new System.Drawing.Size(288, 416);
            this.DgvT.TabIndex = 87;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(567, 468);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(64, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "EXIT";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // DgvF
            // 
            this.DgvF.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvF.Location = new System.Drawing.Point(14, 50);
            this.DgvF.Name = "DgvF";
            this.DgvF.RowHeadersVisible = false;
            this.DgvF.Size = new System.Drawing.Size(288, 416);
            this.DgvF.TabIndex = 0;
            this.DgvF.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvF_CellContentClick);
            this.DgvF.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.DgvF_ColumnAdded);
            // 
            // FrmClassTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(649, 509);
            this.Controls.Add(this.GBMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmClassTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Class Transfer";
            this.Load += new System.EventHandler(this.FrmClassTransfer_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvF)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.DataGridView DgvF;
        private System.Windows.Forms.DataGridView DgvT;
        private System.Windows.Forms.ComboBox cbofrom;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnF;
        private System.Windows.Forms.Button btnT;
        private System.Windows.Forms.Button btnsave;
        internal System.Windows.Forms.Button btnser;
    }
}