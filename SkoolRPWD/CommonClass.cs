﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace SkoolRPWD
{
    public class CommonClass
    {
        public DataTable GetAllClass(string ProcedureName,SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception)
            {
            }
            return dt;
        }
        public DataTable GetDatabyParameter(string ProcedureName,SqlParameter[] para, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetStudentById(int Id,string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddWithValue("@ClassId", Id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception)
            {
            }
            return dt;
        }
        public int ExecuteNonQuery(string ProcedureName, SqlParameter[] para, SqlConnection conn)
        {
            int res = 0;
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                //conn.Close();
                throw ex;
            }
            return res;
        }

        public int ExecuteNonQuery(CommandType commandType,string ProcedureName, SqlConnection conn)
        {
            int res = 0;
            try
            {
                if(conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = commandType,
                    CommandText = ProcedureName
                };
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return res;
        }

        public int ExecuteNonQuery(string ProcedureName, SqlParameter[] para,int returnIndex, SqlConnection conn)
        {
            int res = 0;
            try
            {                
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.ExecuteNonQuery();
                res = (int)cmd.Parameters[returnIndex].Value;
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return res;
        }
        public DataTable GetData(CommandType CMDType,string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = CMDType,
                    CommandText = ProcedureName
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception)
            {
            }
            return dt;
        }
        public DataSet GetTcById(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection conn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    Connection = conn,
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
    public static class WrittenNumerics
    {
        static readonly string[] ones = new string[] { "", "First", "Second", "Third", "Fouth", "Fifth", "Sixth", "Seventh", "Eighth", "Nineth" };
        static readonly string[] teens = new string[] { "Tenth", "Eleventh", "Twelveth", "Thirteenth", "Fourteenth", "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth" };
        static readonly string[] tens = new string[] { "Twentyth", "Thirtyth", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        static readonly string[] thousandsGroups = { "", " Thousand", " Million", " Billion" };

        private static string FriendlyInteger(int n, string leftDigits, int thousands)
        {
            if (n == 0)
                return leftDigits;

            string friendlyInt = leftDigits;
            if (friendlyInt.Length > 0)
                friendlyInt += " ";

            if (n < 10)
                friendlyInt += ones[n];
            else if (n < 20)
                friendlyInt += teens[n - 10];
            else if (n < 100)
                friendlyInt += FriendlyInteger(n % 10, tens[n / 10 - 2], 0);
            else if (n < 1000)
                friendlyInt += FriendlyInteger(n % 100, (ones[n / 100] + " Hundred"), 0);
            else
                friendlyInt += FriendlyInteger(n % 1000, FriendlyInteger(n / 1000, "", thousands + 1), 0);

            return friendlyInt + thousandsGroups[thousands];
        }

        public static string DateToWritten(DateTime date)
        {
            return string.Format("{0} {1} {2}", IntegerToWritten(date.Day) + " -", date.ToString("MMMM") + " -", IntegerToWritten(date.Year));
        }

        public static string IntegerToWritten(int n)
        {
            if (n == 0)
                return "Zero";
            else if (n < 0)
                return "Negative" + IntegerToWritten(-n);

            return FriendlyInteger(n, "", 0);
        }
    

    static readonly string[] one = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
    static readonly string[] teen = new string[] { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
    static readonly string[] ten = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
    static readonly string[] thousandsGroup = { "", " Thousand", " Million", " Billion" };

    private static string FriendlyInteger1(int n, string leftDigits, int thousands)
    {
        if (n == 0)
            return leftDigits;

        string friendlyInt = leftDigits;
        if (friendlyInt.Length > 0)
            friendlyInt += " ";

        if (n < 10)
            friendlyInt += one[n];
        else if (n < 20)
            friendlyInt += teen[n - 10];
        else if (n < 100)
            friendlyInt += FriendlyInteger1(n % 10, tens[n / 10 - 2], 0);
        else if (n < 1000)
            friendlyInt += FriendlyInteger1(n % 100, (ones[n / 100] + " Hundred"), 0);
        else
            friendlyInt += FriendlyInteger1(n % 1000, FriendlyInteger1(n / 1000, "", thousands + 1), 0);

        return friendlyInt + thousandsGroups[thousands];
    }

    public static string DateToWritten1(DateTime date)
    {
        return string.Format("{0} {1} {2}", IntegerToWritten1(date.Day) + " -", date.ToString("MMMM") + " -", IntegerToWritten1(date.Year));
    }

    public static string IntegerToWritten1(int n)
    {
        if (n == 0)
            return "Zero";
        else if (n < 0)
            return "Negative" + IntegerToWritten1(-n);

        return FriendlyInteger1(n, "", 0);
    }
}
}
