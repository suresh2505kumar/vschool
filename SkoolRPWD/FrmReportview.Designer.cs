﻿namespace SkoolRPWD
{
    partial class FrmReportview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crysReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crysReportViewer
            // 
            this.crysReportViewer.ActiveViewIndex = -1;
            this.crysReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crysReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crysReportViewer.Location = new System.Drawing.Point(12, 27);
            this.crysReportViewer.Name = "crysReportViewer";
            this.crysReportViewer.Size = new System.Drawing.Size(934, 445);
            this.crysReportViewer.TabIndex = 0;
            // 
            // FrmReportview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 495);
            this.Controls.Add(this.crysReportViewer);
            this.Name = "FrmReportview";
            this.Text = "FrmReportview";
            this.Load += new System.EventHandler(this.FrmReportview_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crysReportViewer;
    }
}