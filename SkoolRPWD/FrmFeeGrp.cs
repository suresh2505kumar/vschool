﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace SkoolRPWD
{
    public partial class FrmFeeGrp : Form
    {
        public FrmFeeGrp()
        {
            InitializeComponent();
        }

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void FrmFeeGrp_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;            
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtFeegp.Text == "")
            {
                MessageBox.Show("Enter the Feegroup","Information");
                txtFeegp.Focus();
                return;
            }

            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from Generalm where  FGroup='" + txtFeegp.Text + "' ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into Generalm values(@FGroup)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@FGroup", SqlDbType.NVarChar).Value = txtFeegp.Text;
              

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                    txtFeegp.Text = "";                   
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist", "Information");
                    con.Close();
                    cmd1.Dispose();
                    txtFeegp.Text = "";
                    txtFeegp.Focus();
                }

            }
            else
            {
                con.Close();
                string qur = "select * from Generalm where FGroup='" + txtFeegp.Text + "' and uid <> " + txtuid.Text;                
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string QueryUpdate = "Update Generalm set FGroup='" + txtFeegp.Text + "' where uid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtFeegp.Text = "";                   
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist", "Information");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
                btnsave.Text = "Save";
            }
        }

        private void Load_grid()
        {
            try
            {
                con.Open();
                string qur = "SELECT uid, FGroup FROM  Generalm";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);                
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 2;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "FGroup";
                Dgv.Columns[1].HeaderText = "Fee Group";
                Dgv.Columns[1].DataPropertyName = "FGroup";
                Dgv.Columns[1].Width = 325;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information");
                return;
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtFeegp.Text = Dgv.Rows[i].Cells[1].Value.ToString();      
            btnsave.Text = "Update";
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT uid, FGroup FROM  Generalm  where FGroup like '%" + txtscr.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 2;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "FGroup";
                Dgv.Columns[1].HeaderText = "Fee Group";
                Dgv.Columns[1].DataPropertyName = "FGroup";
                Dgv.Columns[1].Width = 325;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Information");
                return;
            }
        }
    }
}
