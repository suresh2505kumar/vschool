﻿namespace SkoolRPWD
{
    partial class FrmTcIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTcIssue));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.txtSeach = new System.Windows.Forms.TextBox();
            this.DataGridStudent = new System.Windows.Forms.DataGridView();
            this.dtpadmissionDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbReason = new System.Windows.Forms.ComboBox();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAdmissionDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAdmissionNo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStudName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.DataGridTC = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).BeginInit();
            this.panel1.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTC)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.dtpadmissionDate);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.cmbReason);
            this.grBack.Controls.Add(this.lblFatherName);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.lblClass);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.lblSex);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.lblAdmissionDate);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.lblAdmissionNo);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtStudName);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(12, 12);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(737, 434);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.txtSeach);
            this.grSearch.Controls.Add(this.DataGridStudent);
            this.grSearch.Location = new System.Drawing.Point(464, 40);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(266, 263);
            this.grSearch.TabIndex = 18;
            this.grSearch.TabStop = false;
            // 
            // txtSeach
            // 
            this.txtSeach.Location = new System.Drawing.Point(6, 16);
            this.txtSeach.Name = "txtSeach";
            this.txtSeach.Size = new System.Drawing.Size(254, 26);
            this.txtSeach.TabIndex = 19;
            this.txtSeach.TextChanged += new System.EventHandler(this.txtSeach_TextChanged);
            this.txtSeach.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeach_KeyDown);
            // 
            // DataGridStudent
            // 
            this.DataGridStudent.AllowUserToAddRows = false;
            this.DataGridStudent.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStudent.Location = new System.Drawing.Point(6, 46);
            this.DataGridStudent.Name = "DataGridStudent";
            this.DataGridStudent.ReadOnly = true;
            this.DataGridStudent.RowHeadersVisible = false;
            this.DataGridStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStudent.Size = new System.Drawing.Size(254, 211);
            this.DataGridStudent.TabIndex = 0;
            this.DataGridStudent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeach_KeyDown);
            this.DataGridStudent.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataGridStudent_MouseDoubleClick);
            // 
            // dtpadmissionDate
            // 
            this.dtpadmissionDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpadmissionDate.Location = new System.Drawing.Point(264, 360);
            this.dtpadmissionDate.Name = "dtpadmissionDate";
            this.dtpadmissionDate.Size = new System.Drawing.Size(130, 26);
            this.dtpadmissionDate.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(206, 364);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 18);
            this.label13.TabIndex = 16;
            this.label13.Text = "Tc Date";
            // 
            // cmbReason
            // 
            this.cmbReason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbReason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReason.FormattingEnabled = true;
            this.cmbReason.Items.AddRange(new object[] {
            "REFER MARK SHEET",
            "PROMOTED TO  UKG",
            "PROMOTED TO  I",
            "PROMOTED TO  II",
            "PROMOTED TO  III",
            "PROMOTED TO  IV",
            "PROMOTED TO  V",
            "PROMOTED TO  VI",
            "PROMOTED TO  VII",
            "PROMOTED TO  VIII",
            "PROMOTED TO IX",
            "PROMOTED TO X",
            "PROMOTED TO XII",
            "DISCONTINUED",
            "YES,  PROMOTED",
            "DETAINED"});
            this.cmbReason.Location = new System.Drawing.Point(264, 319);
            this.cmbReason.Name = "cmbReason";
            this.cmbReason.Size = new System.Drawing.Size(233, 26);
            this.cmbReason.TabIndex = 15;
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Location = new System.Drawing.Point(264, 271);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(13, 18);
            this.lblFatherName.TabIndex = 14;
            this.lblFatherName.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(161, 271);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 13;
            this.label12.Text = "Father\'s Name";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(264, 228);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(13, 18);
            this.lblClass.TabIndex = 12;
            this.lblClass.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(219, 228);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 18);
            this.label10.TabIndex = 11;
            this.label10.Text = "Class";
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(264, 183);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(13, 18);
            this.lblSex.TabIndex = 10;
            this.lblSex.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(228, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Sex";
            // 
            // lblAdmissionDate
            // 
            this.lblAdmissionDate.AutoSize = true;
            this.lblAdmissionDate.Location = new System.Drawing.Point(264, 145);
            this.lblAdmissionDate.Name = "lblAdmissionDate";
            this.lblAdmissionDate.Size = new System.Drawing.Size(13, 18);
            this.lblAdmissionDate.TabIndex = 8;
            this.lblAdmissionDate.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(153, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 18);
            this.label6.TabIndex = 7;
            this.label6.Text = "Admission Date";
            // 
            // lblAdmissionNo
            // 
            this.lblAdmissionNo.AutoSize = true;
            this.lblAdmissionNo.Location = new System.Drawing.Point(264, 100);
            this.lblAdmissionNo.Name = "lblAdmissionNo";
            this.lblAdmissionNo.Size = new System.Drawing.Size(13, 18);
            this.lblAdmissionNo.TabIndex = 6;
            this.lblAdmissionNo.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 323);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Reason";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Admission No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Student Name";
            // 
            // txtStudName
            // 
            this.txtStudName.Location = new System.Drawing.Point(264, 50);
            this.txtStudName.Name = "txtStudName";
            this.txtStudName.Size = new System.Drawing.Size(195, 26);
            this.txtStudName.TabIndex = 0;
            this.txtStudName.Click += new System.EventHandler(this.txtStudName_Click);
            this.txtStudName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtStudName_MouseClick);
            this.txtStudName.Enter += new System.EventHandler(this.txtStudName_Enter);
            this.txtStudName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSeach_KeyDown);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnedit);
            this.panel1.Controls.Add(this.btnadd);
            this.panel1.Controls.Add(this.btnexit);
            this.panel1.Controls.Add(this.btnaddrcan);
            this.panel1.Controls.Add(this.btnsave);
            this.panel1.Location = new System.Drawing.Point(12, 452);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(737, 32);
            this.panel1.TabIndex = 91;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(559, 2);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 84;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(618, 2);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 85;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(602, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(73, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(677, 2);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(671, 2);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(67, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.btnser);
            this.grFront.Controls.Add(this.txtscr);
            this.grFront.Controls.Add(this.DataGridTC);
            this.grFront.Location = new System.Drawing.Point(11, 15);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(737, 434);
            this.grFront.TabIndex = 18;
            this.grFront.TabStop = false;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(659, 17);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(73, 30);
            this.btnser.TabIndex = 91;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(7, 19);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(653, 26);
            this.txtscr.TabIndex = 90;
            // 
            // DataGridTC
            // 
            this.DataGridTC.AllowUserToAddRows = false;
            this.DataGridTC.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTC.Location = new System.Drawing.Point(6, 47);
            this.DataGridTC.Name = "DataGridTC";
            this.DataGridTC.ReadOnly = true;
            this.DataGridTC.RowHeadersVisible = false;
            this.DataGridTC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTC.Size = new System.Drawing.Size(725, 378);
            this.DataGridTC.TabIndex = 0;
            // 
            // FrmTcIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(761, 488);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmTcIssue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tc Issue";
            this.Load += new System.EventHandler(this.FrmTcIssue_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            this.grSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.DateTimePicker dtpadmissionDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbReason;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAdmissionDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAdmissionNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStudName;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridTC;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.DataGridView DataGridStudent;
        private System.Windows.Forms.TextBox txtSeach;
    }
}