﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace SkoolRPWD
{
    public partial class FrmBoardM : Form
    {
        public FrmBoardM()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void FrmBoardM_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            txtboard.Text = string.Empty;
            txtrent.Text = string.Empty;
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void Load_grid()
        {
            try
            {
                con.Open();
                string qur = "SELECT Buid, BoardingP,Rent FROM  BoardingPM";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 3;

                Dgv.Columns[0].Name = "Buid";
                Dgv.Columns[0].HeaderText = "Buid";
                Dgv.Columns[0].DataPropertyName = "Buid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "BoardingP";
                Dgv.Columns[1].HeaderText = "Bus";
                Dgv.Columns[1].DataPropertyName = "BoardingP";
                Dgv.Columns[1].Width = 270;

                Dgv.Columns[2].DataPropertyName = "Rent";
                Dgv.Columns[2].Visible = false;
                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtboard.Text == "")
            {
                MessageBox.Show("Enter the Boarding Point");
                txtboard.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from BoardingPM where  BoardingP='" + txtboard.Text + "' ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into BoardingPM values(@BoardingP,@Rent)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@BoardingP", SqlDbType.NVarChar).Value = txtboard.Text;
                    cmd.Parameters.AddWithValue("@Rent", SqlDbType.NVarChar).Value = txtrent.Text;
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                    txtboard.Text = "";
                    txtrent.Text = "";
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;

                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    txtboard.Text = "";
                    txtboard.Focus();
                }

            }
            else
            {
                con.Close();
                string qur = "select * from BoardingPM where BoardingP='" + txtboard.Text + "' and Buid <> " + txtuid.Text;
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string QueryUpdate = "Update BoardingPM set BoardingP='" + txtboard.Text + "',rent=" + txtrent.Text + " where Buid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Close();
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtboard.Text = "";
                    txtrent.Text = "";
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
                btnsave.Text = "Save";
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtboard.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtrent.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            btnsave.Text = "Update";
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Buid, BoardingP,Rent FROM  BoardingPM where BoardingP  like '%" + txtboard.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);                
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 3;

                Dgv.Columns[0].Name = "Buid";
                Dgv.Columns[0].HeaderText = "Buid";
                Dgv.Columns[0].DataPropertyName = "Buid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "BoardingP";
                Dgv.Columns[1].HeaderText = "Bus";
                Dgv.Columns[1].DataPropertyName = "BoardingP";
                Dgv.Columns[1].Width = 270;

                Dgv.Columns[2].DataPropertyName = "Rent";
                Dgv.Columns[2].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
