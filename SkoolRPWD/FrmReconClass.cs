﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace SkoolRPWD
{
    public partial class FrmReconClass : Form
    {
        public FrmReconClass()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        private void FrmReconClass_Load(object sender, EventArgs e)
        {
            LoadRefT();
        }
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;
            con.Close();
        }
        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 23;
            loadput();
        }
        private void loadput()
        {
            con.Close();
            con.Open();

            if (Module.type == 23)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 23)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }            
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 23)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.Show();
            con.Close();
        }
        private void btnok_Click(object sender, EventArgs e)
        {
            List<ReqBulkUploadProcCollclass1> BulkUploadCollectionList = new List<ReqBulkUploadProcCollclass1>();
            DynamicParameters param = new DynamicParameters();
            param.Add("@classuid", txtcid.Text);
            param.Add("@termuid", cboTerm.SelectedValue);
            BulkUploadCollectionList = con.Query<ReqBulkUploadProcCollclass1>("Prc_ExportCollectionToServerDBClass", param, null, true, 0, CommandType.StoredProcedure).ToList();
            ResUploadCollectionDetailsProclass1 objResUploadCollectionDetails = new ResUploadCollectionDetailsProclass1();
            if (BulkUploadCollectionList.Count > 0)
            {
                DataTable DtCollection = new DataTable();
                DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "Paidamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(Int32) });
                foreach (ReqBulkUploadProcCollclass1 ucd in BulkUploadCollectionList)
                {
                    DataRow row = DtCollection.NewRow();                    
                    row["studid"] = ucd.studid;
                    row["Feesturid"] = ucd.fsuid;
                    row["feeid"] = ucd.Feeid;
                    row["feeamt"] = ucd.Amt;
                    row["concession"] = 0;
                    row["finalamt"] = ucd.Amt;
                    row["Paidamt"] = 0;
                    row["termuid"] = ucd.termuid;
                    row["classid"] = ucd.classuid;
                    DtCollection.Rows.Add(row);
                }
                using (SqlCommand cmd = new SqlCommand("Prc_Test_Bulk_UploadCollectionDetailsProcclass"))
                {
                    DataTable dt = new DataTable();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@Colland", DtCollection);
                    try
                    {
                        con.Open();                        
                        var dataReader = cmd.ExecuteReader();
                        dt.Load(dataReader);
                        objResUploadCollectionDetails.ResponseCode = Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                        objResUploadCollectionDetails.ResponseMessage = Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                        string sql = "select a.uid,classid,a.studid ,a.termuid ,b.amt,b.ConAmt ,b.Feecon,b.feeid   from Feecon a inner join Feecon_det b on a.uid =b.fconid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " ";
                        SqlCommand cmnd = new SqlCommand(sql, con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmnd);
                        DataTable tab = new DataTable();
                        adpt.Fill(tab);

                        for (int i = 0; i < tab.Rows.Count; i++)
                        {
                            double s = Convert.ToDouble(tab.Rows[i]["Amt"].ToString());
                            double s1 = Convert.ToDouble(tab.Rows[i]["ConAmt"].ToString());
                            double j = s - s1;
                            string qu = "update PLFType set concession =" + tab.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + tab.Rows[i]["studid"].ToString() + "  and feeid=" + tab.Rows[i]["feeid"].ToString() + "  and termuid=" + tab.Rows[i]["termuid"].ToString() + " and classid=" + tab.Rows[i]["classid"].ToString() + "";
                            SqlCommand scmd1 = new SqlCommand(qu, con);
                            scmd1.ExecuteNonQuery();
                        }
                        string sql11 = "select feeid  ,sum(paidamt) as paidamt from Coll_Mast a inner join Coll_det b on a.colluid=b.colluid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + "  group by feeid ";
                        SqlCommand cmnd11 = new SqlCommand(sql11, con);
                        SqlDataAdapter adpt11 = new SqlDataAdapter(cmnd11);
                        DataTable tab11 = new DataTable();
                        adpt11.Fill(tab11);

                        for (int i = 0; i < tab11.Rows.Count; i++)
                        {
                            string strSQL2 = "update PLFType set paidamt =" + tab11.Rows[i]["paidamt"].ToString() + " where studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and termuid =" + cboTerm.SelectedValue + " and feeid= " + tab11.Rows[i]["feeid"].ToString() + "";                         
                            SqlCommand scmd1 = new SqlCommand(strSQL2, con);
                            scmd1.ExecuteNonQuery();
                        }                        
                    }
                    catch (Exception)
                    {
                        objResUploadCollectionDetails.ResponseCode = 101;//Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                        objResUploadCollectionDetails.ResponseMessage = "Upload Fails";//Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                    }
                    finally
                    {
                        con.Close();
                        cmd.Dispose();                        
                    }
                }
            }
            MessageBox.Show("Processed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void cmdprt_Click(object sender, EventArgs e)
        {
            Module.cid = Convert.ToInt32(txtcid.Text);
            CRViewer crv = new CRViewer();
            crv.Show();
            con.Close();
        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
    public class ReqBulkUploadProcCollclass1
    {
        public int studid { get; set; }
        public int uid { get; set; }
        public int fsuid { get; set; }
        public int Feeid { get; set; }
        public decimal Amt { get; set; }
        public decimal concession { get; set; }        
        public decimal Paidamt { get; set; }
        public int termuid { get; set; }
        public string classuid { get; set; }
    }
    public class ResUploadCollectionDetailsProclass1
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string dtp { get; set; }
    }
}

