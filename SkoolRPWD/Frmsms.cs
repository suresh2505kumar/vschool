﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Collections.Specialized;

namespace SkoolRPWD
{
    public partial class Frmsms : Form
    {
        public Frmsms()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand cmd1 = new SqlCommand();
        SqlCommand qur = new SqlCommand();
        DataTable dt2 = new DataTable();
        private void Frmsms_Load(object sender, EventArgs e)
        {

        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Load_grid()
        {
            try
            {
                string qur = "SELECT  s_no,Class_Desc as Class, Cid  FROM  Class_Mast order by Class_desc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                DgvF.AutoGenerateColumns = false;
                DgvF.DataSource = null;
                DgvF.ColumnCount = 2;
                DgvF.Columns[0].Name = "Class Name";
                DgvF.Columns[0].HeaderText = "Class Name";
                DgvF.Columns[0].DataPropertyName = "Class";
                DgvF.Columns[0].Width = 190;

                DgvF.Columns[1].Name = "Cid";
                DgvF.Columns[1].HeaderText = "Cid";
                DgvF.Columns[1].DataPropertyName = "Cid";
                DgvF.Columns[1].Visible = false;
                DgvF.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void checkboxHeader_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < DgvF.RowCount; i++)
            {
                DgvF[0, i].Value = ((CheckBox)DgvF.Controls.Find("checkboxHeader", true)[0]).Checked;
            }
            DgvF.EndEdit();
        }

        private void cboclass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboclass.Text == "Classwise")
            {
                lblName.Text = "Select Class's to Send Message";
                lblSelected.Text = "Selected Class's";
                Load_grid();
                label1.Visible = false;
                cbofrom.Visible = false;
                DgvF.ClearSelection();
            }
            else
            {
                cbofrom.Visible = true;
                LoadClassDesc();
                lblName.Text = "Select Student's to Send Message";
                lblSelected.Text = "Selected Student's";
                DgvF.ClearSelection();
            }
        }


        private void LoadClassDesc()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Cid,Class_Desc FROM Class_Mast order by Class_desc";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            DataRow row = tap.NewRow();
            row[0] = 0;
            row[1] = "All";
            tap.Rows.InsertAt(row, 0);
            cbofrom.DataSource = null;
            cbofrom.DisplayMember = "Class_Desc";
            cbofrom.ValueMember = "Cid";
            cbofrom.DataSource = tap;
            
            cbofrom.SelectedIndex = -1;
            con.Close();
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            DgvT.DataSource = null;
            if (cboclass.Text == "Classwise")
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Class").ToString().Trim();
                dt.Columns.Add("Cid").ToString().Trim();
                foreach (DataGridViewRow dr in DgvF.SelectedRows)
                {
                    DataRow gv2dr = dt.NewRow();
                    gv2dr["Class"] = dr.Cells["Class Name"].Value.ToString();
                    gv2dr["Cid"] = dr.Cells["Cid"].Value.ToString();
                    dt.Rows.Add(gv2dr);
                }
                DgvT.DataSource = dt;
                DgvT.Columns[0].Width = 200;
                DgvT.Columns[1].Visible = false;
                DgvT.ClearSelection();
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("StudentName").ToString().Trim();
                dt.Columns.Add("uid").ToString().Trim();
                dt.Columns.Add("SMS").ToString().Trim();
                foreach (DataGridViewRow dr in DgvF.SelectedRows)
                {
                    DataRow gv2dr = dt.NewRow();
                    gv2dr["StudentName"] = dr.Cells["StudentName"].Value.ToString();
                    gv2dr["uid"] = dr.Cells["uid"].Value.ToString();
                    gv2dr["SMS"] = dr.Cells["SMS"].Value.ToString();
                    dt.Rows.Add(gv2dr);
                }
                DgvT.DataSource = dt;
                DgvT.Columns[0].Width = 200;
                DgvT.Columns[1].Visible = false;
                DgvT.Columns[2].Visible = false;
                DgvT.ClearSelection();
            }
        }

        private void DgvF_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            btnsave.Enabled = false;
            string User = ConfigurationManager.AppSettings["User"];
            string Password = ConfigurationManager.AppSettings["password"];
            if (txtdesc.Text == string.Empty)
            {
                MessageBox.Show("Enter the Message", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cboclass.Text == "Classwise")
            {
                for (int i = 0; i < DgvT.Rows.Count; i++)
                {
                    con.Close();
                    con.Open();
                    string qur = "select sname,SMS,Class_Desc,cid  from studentm a inner join class_mast b on a.Classid = b.cid where classid= " + DgvT.Rows[i].Cells[1].Value.ToString() + " ";
                    cmd = new SqlCommand(qur, con);
                    adpt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        string Mobile = string.Empty;
                        foreach (DataRow row1 in dt.Rows)
                        {
                            if (Mobile == string.Empty)
                            {
                                Mobile = row1["SMS"].ToString();
                            }
                            else
                            {
                                Mobile += "," + row1["SMS"].ToString();
                            }
                        }
                        using (var client = new WebClient())
                        {
                            var values = new NameValueCollection();
                            {
                                values["user"] = User;
                                values["password"] = Password;
                                values["mobiles"] = Mobile;
                                values["sms"] = txtdesc.Text;
                            };
                            var response = client.UploadValues("http://sms.mylaschool.in/sendsms.jsp?", values);
                            var responseString = Encoding.Default.GetString(response);
                        }
                    }
                }
            }
            else
            {
                string mobile = string.Empty;
                for (int i = 0; i < DgvT.Rows.Count; i++)
                {
                    if (mobile == string.Empty)
                    {
                        mobile = DgvT.Rows[i].Cells[2].Value.ToString();
                    }
                    else
                    {
                        mobile += "," + DgvT.Rows[i].Cells[2].Value.ToString();
                    }
                }
                try
                {
                    using (var client = new WebClient())
                    {
                        var values = new NameValueCollection();
                        {
                            values["user"] = User;
                            values["password"] = Password;
                            values["mobiles"] = mobile;
                            values["sms"] = txtdesc.Text;
                        };
                        var response = client.UploadValues("http://sms.mylaschool.in/sendsms.jsp?", values);
                        var responseString = Encoding.Default.GetString(response);
                    }
                }
                catch (WebException)
                {

                }
            }
            MessageBox.Show("Message Send Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnsave.Enabled = true;

        }

        private void DgvT_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnser_Click(object sender, EventArgs e)
        {
            DgvT.Refresh();
            DgvT.Columns.Clear();
        }

        private void cbofrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbofrom.Text != string.Empty)
            {
                string res = "Select SName as StudentName,uid,SMS from StudentM Where Classid=@ClassID";
                cmd = new SqlCommand(res, con);
                adpt = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@ClassID", cbofrom.SelectedValue);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                DgvF.DataSource = null;
                DgvF.AutoGenerateColumns = false;
                DgvF.ColumnCount = 3;
                DgvF.Columns[0].Name = "StudentName";
                DgvF.Columns[0].HeaderText = "StudentName";
                DgvF.Columns[0].DataPropertyName = "StudentName";
                DgvF.Columns[0].Width = 200;

                DgvF.Columns[1].Name = "uid";
                DgvF.Columns[1].HeaderText = "uid";
                DgvF.Columns[1].DataPropertyName = "uid";
                DgvF.Columns[1].Visible = false;

                DgvF.Columns[2].Name = "SMS";
                DgvF.Columns[2].HeaderText = "SMS";
                DgvF.Columns[2].DataPropertyName = "SMS";
                DgvF.Columns[2].Visible = false;
                DgvF.DataSource = dt;
                DgvF.ClearSelection();
            }
            else
            {
                DgvF.DataSource = null;
            }

        }

        private void GBList_Enter(object sender, EventArgs e)
        {

        }

        private void txtdesc_TextChanged(object sender, EventArgs e)
        {
            string userInput = txtdesc.Text;
            userInput = userInput.Trim();
            string[] wordCount = userInput.Split(null);

            int charCount = 0;
            foreach (var word in wordCount)
                charCount += word.Length;
            //label4.Text = wordCount.Length.ToString();
            label4.Text = charCount.ToString();
        }

        private void DgvF_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                con.Close();
                con.Open();
                if (cboclass.Text == "Classwise")
                {
                    string ClassId = string.Empty;
                    for (int i = 0; i < DgvT.Rows.Count; i++)
                    {
                        if (ClassId == string.Empty)
                        {
                            ClassId = DgvT.Rows[i].Cells[1].Value.ToString();
                        }
                        else
                        {
                            ClassId += "," + DgvT.Rows[i].Cells[1].Value.ToString();
                        }
                    }
                    
                    string qur = "select sname,SMS,Class_Desc from studentm a inner join class_mast b on a.Classid = b.cid where classid In (" + ClassId + ") and a.Tag ='A' order by Class_Desc";
                    cmd = new SqlCommand(qur, con);
                    adpt = new SqlDataAdapter(cmd);                    
                    adpt.Fill(dt);
                }
                else
                {
                    if(cbofrom.Text == "All")
                    {                      
                        string qur = "select sname,SMS,Class_Desc  from studentm a inner join class_mast b on a.Classid = b.cid Where a.Tag ='A' order by Class_Desc";
                        cmd = new SqlCommand(qur, con);
                        adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(dt);
                    }
                    else
                    {
                        string qur = "select sname,SMS,Class_Desc  from studentm a inner join class_mast b on a.Classid = b.cid where classid In (" + cbofrom.SelectedValue + ") order by Class_Desc";
                        cmd = new SqlCommand(qur, con);
                        adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(dt);
                    }
                }

                FolderBrowserDialog fo = new FolderBrowserDialog();
                fo.ShowDialog();
                // Bind table data to Stream Writer to export data to respective folder
                StreamWriter wr = new StreamWriter(fo.SelectedPath + "\\SMS.xls");
                // Write Columns to excel file
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    wr.Write(dt.Columns[i].ToString().ToUpper() + "\t");
                }
                wr.WriteLine();
                //write rows to excel file
                for (int i = 0; i < (dt.Rows.Count); i++)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (dt.Rows[i][j] != null)
                        {
                            wr.Write(Convert.ToString(dt.Rows[i][j]) + "\t");
                        }
                        else
                        {
                            wr.Write("\t");
                        }
                    }
                    wr.WriteLine();
                }
                wr.Close();
                MessageBox.Show("exported");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
