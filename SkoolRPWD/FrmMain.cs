﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing1;
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        MenuStrip MnuStrip = null;
        ToolStripMenuItem MnuStripItem;
        CommonClass db = new CommonClass();
        private void FrmMain_FormClosing1(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;

            }

        }
        protected void LeadParentMenu()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@UserId", FrmLogin.UserId) };
                DataTable dt = db.GetDatabyParameter("SP_GetParentMenu", para,con);
                foreach (DataRow dr in dt.Rows)
                {
                    MnuStripItem = new ToolStripMenuItem(dr["MAINMNU"].ToString());
                    SubMenu(MnuStripItem, Convert.ToInt32(dr["MENUPARVAL"].ToString()));
                    MnuStrip.Items.Add(MnuStripItem);
                }
                this.MainMenuStrip = MnuStrip;                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }           
        }
        public void SubMenu(ToolStripMenuItem mnu, int submenu)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Submenu", submenu), new SqlParameter("@UserId", FrmLogin.UserId) };
                DataTable dtchild = db.GetDatabyParameter("SP_GetSubMenu", para,con);
                foreach (DataRow dr in dtchild.Rows)
                {
                    ToolStripMenuItem SSMenu = new ToolStripMenuItem(dr["FRM_NAME"].ToString(), null, new EventHandler(ChildClick));
                    //LoadSubMneuofSub(SSMenu, dr["MNUSUBMENU"].ToString());
                    mnu.DropDownItems.Add(SSMenu);
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }            
        }

        private void LoadSubMneuofSub(ToolStripMenuItem sSMenu, string v)
        {
            throw new NotImplementedException();
        }

        private void ChildClick(object sender, EventArgs e)
        {
            SqlParameter[] para = { new SqlParameter("@Frm_Name", sender.ToString()) };
            DataTable dtransaction = db.GetDatabyParameter("SP_getFrom", para,con);
            Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
            foreach (Type type in frmAssembly.GetTypes())
            {
                //MessageBox.Show(type.Name);
                if (type.BaseType == typeof(Form))
                {
                    if (type.Name == dtransaction.Rows[0][0].ToString())
                    {
                        Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                        // then when you want to close all of them simple call the below code

                        //foreach (Form form in this.MdiChildren)
                        //{
                        //    form.Close();
                        //}
                        frmShow.MdiParent = this;
                        frmShow.MdiParent = this;
                        frmShow.StartPosition = FormStartPosition.CenterScreen;
                        //frmShow.ControlBox = false;
                        frmShow.Show();
                    }
                }
            }
        }
    
    private void StudentToolStripMenu_Click(object sender, EventArgs e)
        {
            FrmStud stud = new FrmStud();
            stud.MdiParent = this;
            stud.Show();
        }

        private void ClasstoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmClassM classM = new FrmClassM();
            classM.MdiParent = this;
            classM.Show();
        }

        private void FeeGrouptoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmFeeGrp feeg = new FrmFeeGrp();
            feeg.MdiParent = this;
            feeg.Show();
        }

        private void FeeMasttoolStripMenu_Click(object sender, EventArgs e)
        {
            FeeMaster feem = new FeeMaster();
            feem.MdiParent = this;
            feem.Show();
        }

        private void FeeStructtoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmFeeStruct fees = new FrmFeeStruct();
            fees.MdiParent = this;
            fees.Show();
        }

        private void BustoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmStudBus sbus = new FrmStudBus();
            sbus.MdiParent = this;
            sbus.Show();
        }

        private void VantoolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FrmVanM van = new FrmVanM();
            van.MdiParent = this;
            van.Show();
        }

        private void StudentsImporttoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmStudentImprt import = new FrmStudentImprt();
            import.MdiParent = this;
            import.StartPosition = FormStartPosition.CenterScreen;
            import.Show();
        }

        private void ClassTransfertoolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmClassTransfer trans = new FrmClassTransfer();
            trans.MdiParent = this;
            trans.Show();
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            FrmOthFeestru oth = new FrmOthFeestru();
            oth.MdiParent = this;
            oth.Show();
        }

        private void FeeConsessiontoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmFeeCon con = new FrmFeeCon();
            con.MdiParent = this;
            con.Show();
        }

        private void FeeReceipttoolStripMenu_Click(object sender, EventArgs e)
        {
            Module.Dtype = 10;
            FrmFeeRecp rcp = new FrmFeeRecp();
            rcp.MdiParent = this;
            rcp.Show();
        }

        private void FeeRecontoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmReConc ree = new FrmReConc();
            ree.MdiParent = this;
            ree.Show();
        }

        private void TcIssuetoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmTcIssue tc = new FrmTcIssue();
            tc.MdiParent = this;
            tc.StartPosition = FormStartPosition.CenterScreen;
            tc.Show();
        }

        private void StudBustoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmStudBus sbus = new FrmStudBus();
            sbus.MdiParent = this;
            sbus.Show();
        }

        private void OverDuetoolStripMenu_Click(object sender, EventArgs e)
        {
            Module.Dtype = 20;
            FrmOverdue due = new FrmOverdue();
            due.MdiParent = this;
            due.Show();
        }

        private void ReceipttoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmReportviewver rpt = new FrmReportviewver();
            rpt.MdiParent = this;
            rpt.StartPosition = FormStartPosition.CenterScreen;
            rpt.Show();
        }

        private void FeeReporttoolStripMenu_Click(object sender, EventArgs e)
        {
            Module.Dtype = 30;
            FeeReports day = new FeeReports();
            day.MdiParent = this;
            day.Show();
        }

        private void StudentRpttoolStripMenu_Click(object sender, EventArgs e)
        {
            Module.Dtype = 40;
            FrmStudrpt stu = new FrmStudrpt();
            stu.MdiParent = this;
            stu.Show();
        }

        private void StudentLedgertoolStripMenu_Click(object sender, EventArgs e)
        {
            FrmStudLedger ledg = new FrmStudLedger();
            ledg.MdiParent = this;
            ledg.Show();
        }

        private void FeeRemindertoolStripMenu_Click(object sender, EventArgs e)
        {
            Module.Dtype = 43;
            Frmtermrpt rem = new Frmtermrpt();
            rem.MdiParent = this;
            rem.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            MdiClient ctlMDI;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;
                    ctlMDI.BackColor = this.BackColor;
                }
                catch (InvalidCastException)
                {
                }
            }
            MnuStrip = new MenuStrip();
            this.Controls.Add(MnuStrip);
            LeadParentMenu();
        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            Frmsms sms = new Frmsms();
            sms.MdiParent = this;
            sms.StartPosition = FormStartPosition.CenterScreen;
            sms.Show();
        }

        private void termFeeReconciliationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFeeCollectedReconcile recon = new FrmFeeCollectedReconcile();
            recon.MdiParent = this;
            recon.StartPosition = FormStartPosition.CenterScreen;
            recon.Show();
        }

        private void feeUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRecipteconsile rpt = new FrmRecipteconsile();
            rpt.MdiParent = this;
            rpt.StartPosition = FormStartPosition.CenterScreen;
            rpt.Show();
        }

        private void tCDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTcDetails tc = new FrmTcDetails();
            tc.MdiParent = this;
            tc.StartPosition = FormStartPosition.CenterScreen;
            tc.Show();
        }
    }
}
