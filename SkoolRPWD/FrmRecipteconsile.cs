﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmRecipteconsile : Form
    {
        public FrmRecipteconsile()
        {
            InitializeComponent();
        }
        DataTable dtclone;
        CommonClass cmc = new CommonClass();
        decimal FeeAmount = 0;
        decimal Concession = 0;
        decimal FinalAmount = 0;
        decimal PaidAmount = 0;
        BindingSource source = new BindingSource();
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void cmbTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridStudent.DataSource = null;
                if (cmbTerm.SelectedIndex != -1)
                {
                    FeeAmount = 0; Concession = 0;
                    FinalAmount = 0; PaidAmount = 0;
                    int Classid = (int)cmbClass.SelectedValue;
                    SqlParameter[] parameter = {
                    new SqlParameter("@ClassId",Classid),
                    new SqlParameter("@StudId",txtstudent.Tag),
                    new SqlParameter("@termuid",Convert.ToInt32(cmbTerm.SelectedValue))
                };
                    DataTable dt = cmc.GetDatabyParameter("SP_GetReceiptNumber", parameter,con);
                    LoadReceipt(dt);
                }

            }
            catch (Exception)
            {
            }
        }

        private void FrmRecipteconsile_Load(object sender, EventArgs e)
        {
            LoadClass();
        }
        protected void LoadClass()
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetAllClass("SPGetClass",con);
            cmbClass.DataSource = null;
            cmbClass.DisplayMember = "Class_Desc";
            cmbClass.ValueMember = "Cid";
            cmbClass.DataSource = dt;
        }

        private void cmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass",con);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }
        protected void LoadReceipt(DataTable dt)
        {
            try
            {
                cmbReceipt.DataSource = null;
                cmbReceipt.DisplayMember = "RecptNo";
                cmbReceipt.ValueMember = "Colluid";
                cmbReceipt.DataSource = dt;
            }
            catch (Exception)
            {

            }
        }
        protected void LoadTermFee(DataTable dt)
        {
            DgvTermFee.DataSource = null;
            DgvTermFee.AutoGenerateColumns = false;
            DgvTermFee.ColumnCount = 8;
            DgvTermFee.Columns[0].Name = "PLFuid";
            DgvTermFee.Columns[0].HeaderText = "PLFuid";
            DgvTermFee.Columns[0].DataPropertyName = "PLFuid";
            DgvTermFee.Columns[0].Visible = false;

            DgvTermFee.Columns[1].Name = "Fid";
            DgvTermFee.Columns[1].HeaderText = "Fid";
            DgvTermFee.Columns[1].DataPropertyName = "Fid";
            DgvTermFee.Columns[1].Visible = false;

            DgvTermFee.Columns[2].Name = "Fees";
            DgvTermFee.Columns[2].HeaderText = "Fees";
            DgvTermFee.Columns[2].DataPropertyName = "Descp";
            DgvTermFee.Columns[2].Width = 230;

            DgvTermFee.Columns[3].Name = "Fee Amount";
            DgvTermFee.Columns[3].HeaderText = "Fee Amount";
            DgvTermFee.Columns[3].DataPropertyName = "feeamt";
            DgvTermFee.Columns[3].Width = 110;
            DgvTermFee.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[4].Name = "Concession";
            DgvTermFee.Columns[4].HeaderText = "Concession";
            DgvTermFee.Columns[4].DataPropertyName = "concession";
            DgvTermFee.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[5].Name = "Finalamt";
            DgvTermFee.Columns[5].HeaderText = "Finalamt";
            DgvTermFee.Columns[5].DataPropertyName = "finalamt";
            DgvTermFee.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[6].Name = "Paid Amount";
            DgvTermFee.Columns[6].HeaderText = "Paid Amount";
            DgvTermFee.Columns[6].DataPropertyName = "paidamt";
            DgvTermFee.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DgvTermFee.Columns[7].Name = "Colluid";
            DgvTermFee.Columns[7].HeaderText = "Colluid";
            DgvTermFee.Columns[7].DataPropertyName = "Colluid";
            DgvTermFee.Columns[7].Visible = false;
            DgvTermFee.DataSource = dt;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeAmount += Convert.ToDecimal(dt.Rows[i]["feeamt"].ToString());
                Concession += Convert.ToDecimal(dt.Rows[i]["concession"].ToString());
                FinalAmount += Convert.ToDecimal(dt.Rows[i]["finalamt"].ToString());
                PaidAmount += Convert.ToDecimal(dt.Rows[i]["paidamt"].ToString());
            }
            lblFeeAmount.Text = FeeAmount.ToString();
            lblConcession.Text = Concession.ToString();
            lblFinalAmount.Text = FinalAmount.ToString();
            lblPaidAmount.Text = PaidAmount.ToString();

        }
        protected void LoadDataGrid(DataTable dt)
        {
            DataGridStudent.DataSource = null;
            DataGridStudent.AutoGenerateColumns = false;
            DataGridStudent.ColumnCount = 2;
            DataGridStudent.Columns[0].Name = "StudentName";
            DataGridStudent.Columns[0].HeaderText = "Student Name";
            DataGridStudent.Columns[0].DataPropertyName = "StudentName";
            DataGridStudent.Columns[0].Width = 260;
            DataGridStudent.Columns[1].Name = "Uid";
            DataGridStudent.Columns[1].HeaderText = "Uid";
            DataGridStudent.Columns[1].DataPropertyName = "Uid";
            DataGridStudent.Columns[1].Visible = false;
            source.DataSource = dt;
            DataGridStudent.DataSource = source;
        }

        private void txtStudentsearch_TextChanged(object sender, EventArgs e)
        {
            source.Filter = string.Format("StudentName LIKE '%{0}%'", txtStudentsearch.Text);
        }

        private void cmbReceipt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FeeAmount = 0;
                Concession = 0;
                FinalAmount = 0;
                PaidAmount = 0;
                if (cmbReceipt.SelectedIndex != -1)
                {
                    SqlParameter[] parameter = {

                    new SqlParameter("@Recptno",cmbReceipt.SelectedValue)
                };
                    DataTable dt = cmc.GetDatabyParameter("SP_GetCollectionByReceiptNUmber", parameter,con);
                    LoadTermFee(dt);
                    dtclone = dt.Copy();
                }
                else
                {
                    DgvTermFee.DataSource = null;
                }

            }
            catch (Exception)
            {
            }
        }

        private void DataGridStudent_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = DataGridStudent.SelectedCells[0].RowIndex;
            txtstudent.Text = DataGridStudent.Rows[index].Cells[0].Value.ToString();
            txtstudent.Tag = DataGridStudent.Rows[index].Cells[1].Value;
            DataTable dtTerm = cmc.GetAllClass("SP_GetAllTerm",con);
            cmbTerm.DataSource = null;
            cmbTerm.DisplayMember = "Refname";
            cmbTerm.ValueMember = "Ruid";
            cmbTerm.DataSource = dtTerm;
            grStudent.Visible = false;
            DgvTermFee.DataSource = null;

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DgvTermFee.Rows.Count; i++)
                {
                    int Plfuid = Convert.ToInt32(DgvTermFee.Rows[i].Cells[0].Value.ToString());
                    DataTable oldamtdt = dtclone.Select("PLFUid ='" + Plfuid + "'").CopyToDataTable();
                    decimal OldAmt = Convert.ToDecimal(oldamtdt.Rows[0]["paidamt"].ToString());
                    decimal Paidamt = Convert.ToDecimal(DgvTermFee.Rows[i].Cells[6].Value.ToString());
                    int CollUid = Convert.ToInt32(DgvTermFee.Rows[i].Cells[7].Value.ToString());
                    decimal Concession = Convert.ToDecimal(DgvTermFee.Rows[i].Cells[4].Value.ToString());
                    decimal Finalamt = Convert.ToDecimal(DgvTermFee.Rows[i].Cells[5].Value.ToString());
                    int Feeid = Convert.ToInt32(DgvTermFee.Rows[i].Cells[1].Value.ToString());
                    decimal Feeamt = Convert.ToDecimal(DgvTermFee.Rows[i].Cells[3].Value.ToString());
                    SqlParameter[] para = {
                    new SqlParameter("@paidamt",Paidamt),
                    new SqlParameter("@PLFUid",Plfuid),
                    new SqlParameter("@feeamt",Feeamt),
                    new SqlParameter("@concession",Concession),
                    new SqlParameter("@finalamt",Finalamt),
                    new SqlParameter("@FeeId",Feeid),
                    new SqlParameter("@TermUid",Convert.ToInt32(cmbTerm.SelectedValue.ToString())),
                    new SqlParameter("@StudId",txtstudent.Tag),
                    new SqlParameter("@oldpaidamt",OldAmt),
                    new SqlParameter("@colluid",CollUid)
                    };
                    int id = cmc.ExecuteNonQuery("SP_UpdateFeCollection", para,con);
                }
                MessageBox.Show("Updated Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void grStudent_Enter(object sender, EventArgs e)
        {

        }

        private void txtstudent_TextChanged(object sender, EventArgs e)
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass",con);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }

        private void txtstudent_Click(object sender, EventArgs e)
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass",con);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }

        private void txtstudent_MouseClick(object sender, MouseEventArgs e)
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass",con);
            LoadDataGrid(dt);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }
    }
}
