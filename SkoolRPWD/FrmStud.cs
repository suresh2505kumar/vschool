﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SkoolRPWD
{
    public partial class FrmStud : Form
    {
        public FrmStud()
        {
            InitializeComponent();
        }
        CommonClass db = new CommonClass();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void Load_grid()
        {
            try
            {                             
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SPGetAllStud";
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@StudUid", 0);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 23;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Admisno";
                Dgv.Columns[1].HeaderText = "Admission No";
                Dgv.Columns[1].DataPropertyName = "Admisno";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "SName";
                Dgv.Columns[2].HeaderText = "Student Name";
                Dgv.Columns[2].DataPropertyName = "SName";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "FName";
                Dgv.Columns[3].HeaderText = "Father Name";
                Dgv.Columns[3].DataPropertyName = "FName";
                Dgv.Columns[3].Width = 150;

                Dgv.Columns[4].Name = "Fphone";
                Dgv.Columns[4].HeaderText = "Father phone";
                Dgv.Columns[4].DataPropertyName = "Fphone";
                Dgv.Columns[4].Width = 100;

                Dgv.Columns[5].Name = "DOB";
                Dgv.Columns[5].HeaderText = "DOB";
                Dgv.Columns[5].DataPropertyName = "DOB";
                Dgv.Columns[5].Width = 100;


                Dgv.Columns[6].Name = "Sex";
                Dgv.Columns[6].HeaderText = "Gender";
                Dgv.Columns[6].DataPropertyName = "Sex";
                Dgv.Columns[6].Visible = false;


                Dgv.Columns[7].Name = "Community";
                Dgv.Columns[7].HeaderText = "Community";
                Dgv.Columns[7].DataPropertyName = "Community";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "MName";
                Dgv.Columns[8].HeaderText = "MName";
                Dgv.Columns[8].DataPropertyName = "MName";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "Address";
                Dgv.Columns[9].HeaderText = "Address";
                Dgv.Columns[9].DataPropertyName = "Address";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "DOA";
                Dgv.Columns[10].HeaderText = "DOA No";
                Dgv.Columns[10].DataPropertyName = "DOA";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "Class_join";
                Dgv.Columns[11].HeaderText = "Class_join";
                Dgv.Columns[11].DataPropertyName = "Class_join";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "Mphone";
                Dgv.Columns[12].HeaderText = "Mphone";
                Dgv.Columns[12].DataPropertyName = "Mphone";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "SMS";
                Dgv.Columns[13].HeaderText = "SMS";
                Dgv.Columns[13].DataPropertyName = "SMS";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].Name = "SMS1";
                Dgv.Columns[14].HeaderText = "SMS1";
                Dgv.Columns[14].DataPropertyName = "SMS1";
                Dgv.Columns[14].Visible = false;

                Dgv.Columns[15].Name = "roll_no";
                Dgv.Columns[15].HeaderText = "roll_no";
                Dgv.Columns[15].DataPropertyName = "roll_no";
                Dgv.Columns[15].Visible = false;

                Dgv.Columns[16].DataPropertyName = "Classid";
                Dgv.Columns[16].Visible = false;

                Dgv.Columns[17].DataPropertyName = "Class_Desc";
                Dgv.Columns[17].HeaderText = "Class";
                Dgv.Columns[17].DataPropertyName = "Class_Desc";
                Dgv.Columns[17].Width = 100;

                Dgv.Columns[18].Name = "Identification1";
                Dgv.Columns[18].HeaderText = "Identification1";
                Dgv.Columns[18].DataPropertyName = "Identification1";
                Dgv.Columns[18].Visible = false;

                Dgv.Columns[19].Name = "Identification2";
                Dgv.Columns[19].HeaderText = "Identification2";
                Dgv.Columns[19].DataPropertyName = "Identification2";
                Dgv.Columns[19].Visible = false;

                Dgv.Columns[20].Name = "Caste";
                Dgv.Columns[20].HeaderText = "Caste";
                Dgv.Columns[20].DataPropertyName = "Caste";
                Dgv.Columns[20].Visible = false;

                Dgv.Columns[21].Name = "Religion";
                Dgv.Columns[21].HeaderText = "Religion";
                Dgv.Columns[21].DataPropertyName = "Religion";
                Dgv.Columns[21].Visible = false;

                Dgv.Columns[22].Name = "Nationality";
                Dgv.Columns[22].HeaderText = "Nationality";
                Dgv.Columns[22].DataPropertyName = "Nationality";
                Dgv.Columns[22].Visible = false;

                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void LoadClass_join()
        {
            conn.Close();
            conn.Open();
            string qur = "SELECT * FROM class_join";
            cmd = new SqlCommand(qur, conn);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboclassJ.DataSource = null;
            cboclassJ.DataSource = tap;
            cboclassJ.DisplayMember = "classJ";
            cboclassJ.ValueMember = "uid";
            cboclassJ.SelectedIndex = -1;
            conn.Close();

        }

        private void LoadClassDesc()
        {
            conn.Close();
            conn.Open();
            string qur = "SELECT Cid,Class_Desc FROM Class_Mast order by Class_Desc";
            cmd = new SqlCommand(qur, conn);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboclass.DataSource = null;            
            cboclass.DisplayMember = "Class_Desc";
            cboclass.ValueMember = "Cid";
            cboclass.DataSource = tap;
            cboclass.SelectedIndex = -1;
            conn.Close();

        }


        private void FrmStud_Load(object sender, EventArgs e)
        {
            Load_grid ();
            LoadClassDesc();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            FunC .buttonstyleform(this);
            FunC.buttonstylepanel(panadd);
            panadd.Visible = true;
            //LoadDataTable();
            //LoadClass_join();
        }

        //protected void LoadDataTable()
        //{
        //    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetscholarshipM", conn);
        //    comboBox1.DataSource = null;
        //    comboBox1.DisplayMember = "SName";
        //    comboBox1.ValueMember = "SAmount";
        //    comboBox1.DataSource = dt;
        //}

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;

            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true ;
            GBMain.Visible = false ;
            clearTxt();

            btnsave.Text = "Save";
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtsname.Text == "")
            {
                MessageBox.Show("Enter the Stuednt name");
                txtsname.Focus();
                return;
            }
            else if (txtadno.Text == "")
            {
                MessageBox.Show("select the Admission No");
                txtadno.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                conn.Close();
                
                string Query = "Select * from StudentM Where Admisno = '" + txtadno.Text + "'";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtch = new DataTable();
                da.Fill(dtch);
                if(dtch.Rows.Count == 0)
                {
                    conn.Open();
                    string qur = "insert into StudentM OUTPUT inserted.uid  values(@Admisno,@SName,@Sex,@Community,@FName,@MName,@Address,@DOA,@DOB,@Class_join,@Fphone,@Mphone,@SMS,@SMS1,@roll_no,@Classid,@Tag,@Identification1,@Identification2,@Religion,@Caste,@Nationality)";
                    SqlCommand scmd = new SqlCommand(qur, conn);

                    scmd.Parameters.AddWithValue("@Admisno", SqlDbType.NVarChar).Value = txtadno.Text;
                    scmd.Parameters.AddWithValue("@SName", SqlDbType.NVarChar).Value = txtsname.Text;
                    scmd.Parameters.AddWithValue("@Sex", SqlDbType.NVarChar).Value = cbosex.Text;
                    scmd.Parameters.AddWithValue("@Community", SqlDbType.NVarChar).Value = cbocom.Text;
                    scmd.Parameters.AddWithValue("@FName", SqlDbType.NVarChar).Value = txtfname.Text;
                    scmd.Parameters.AddWithValue("@MName", SqlDbType.NVarChar).Value = txtmname.Text;
                    scmd.Parameters.AddWithValue("@Address", SqlDbType.NVarChar).Value = txtAddr.Text;
                    scmd.Parameters.AddWithValue("@DOA", SqlDbType.NVarChar).Value = doa.Text;
                    scmd.Parameters.AddWithValue("@DOB", SqlDbType.NVarChar).Value = dob.Text;
                    scmd.Parameters.AddWithValue("@Class_join", SqlDbType.NVarChar).Value = cboclassJ.Text;
                    scmd.Parameters.AddWithValue("@Fphone", SqlDbType.NVarChar).Value = txtfphone.Text;
                    scmd.Parameters.AddWithValue("@Mphone", SqlDbType.NVarChar).Value = txtmphone.Text;
                    scmd.Parameters.AddWithValue("@SMS", SqlDbType.NVarChar).Value = txtsms.Text;
                    scmd.Parameters.AddWithValue("@SMS1", SqlDbType.NVarChar).Value = txtsms1.Text;
                    scmd.Parameters.AddWithValue("@Tag", SqlDbType.Char).Value = "A";
                    scmd.Parameters.AddWithValue("@Identification1", SqlDbType.NVarChar).Value = txtMole.Text;
                    scmd.Parameters.AddWithValue("@Identification2", SqlDbType.NVarChar).Value = txtScar.Text;
                    scmd.Parameters.AddWithValue("@Religion", SqlDbType.NVarChar).Value = cboReligion.Text;
                    scmd.Parameters.AddWithValue("@Caste", SqlDbType.NVarChar).Value = txtCaste.Text;
                    scmd.Parameters.AddWithValue("@Nationality", SqlDbType.NVarChar).Value = cmbNation.Text;
                    if (cboroll.Text == "")
                    {
                        scmd.Parameters.AddWithValue("@roll_no", SqlDbType.NVarChar).Value = string.Empty;
                    }
                    else
                    {
                        scmd.Parameters.AddWithValue("@roll_no", SqlDbType.NVarChar).Value = cboroll.Text;
                    }
                    scmd.Parameters.AddWithValue("@Classid", SqlDbType.NVarChar).Value = cboclass.SelectedValue;

                    int UId = (int)scmd.ExecuteScalar();
                    CommonClass cmc = new CommonClass();
                    SqlParameter[] para = { new SqlParameter("@ClassId", Convert.ToInt32(cboclass.SelectedValue)) };
                    DataTable dt = cmc.GetDatabyParameter("SP_GetFeeToPlf", para,conn);
                    if (dt.Rows.Count != 0)
                    {
                        for (int k = 0; k < dt.Rows.Count; k++)
                        {
                            conn.Close();
                            conn.Open();
                            SqlCommand cmdPlf = new SqlCommand();
                            cmdPlf.Connection = conn;
                            cmdPlf.CommandType = CommandType.StoredProcedure;
                            cmdPlf.CommandText = "Insert_PLF2";
                            cmdPlf.Parameters.AddWithValue("@studid", SqlDbType.Int).Value = UId;
                            cmdPlf.Parameters.AddWithValue("@Feesturid", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[k]["Feesturid"].ToString());
                            cmdPlf.Parameters.AddWithValue("@feeid", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[k]["Feeid"].ToString());
                            cmdPlf.Parameters.AddWithValue("@feeamt", SqlDbType.Decimal).Value = Convert.ToDecimal(dt.Rows[k]["feeamt"].ToString());
                            cmdPlf.Parameters.AddWithValue("@concession", SqlDbType.Decimal).Value = Convert.ToDecimal(dt.Rows[k]["Concession"].ToString());
                            cmdPlf.Parameters.AddWithValue("@finalamt", SqlDbType.Decimal).Value = Convert.ToDecimal(dt.Rows[k]["finalamt"].ToString());
                            cmdPlf.Parameters.AddWithValue("@paidamt", SqlDbType.Decimal).Value = Convert.ToDecimal(dt.Rows[k]["paidamt"].ToString());
                            cmdPlf.Parameters.AddWithValue("@termuid", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[k]["termuid"].ToString());
                            cmdPlf.Parameters.AddWithValue("@classid", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[k]["classuid"].ToString());
                            cmdPlf.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                    clearTxt();
                    conn.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Record already exists", "Information", MessageBoxButtons.OK);
                }
               
            }
            else
            {
                string QueryUpdate = "Update StudentM set Admisno='" + txtadno.Text + "',SName='" + txtsname.Text + "',Sex='" + cbosex.Text + "',Community='" + cbocom.Text + "',FName='" + txtfname.Text + "',MName='" + txtmname.Text + "',Address='" + txtAddr.Text + "',DOA='" + doa.Text + "' ,DOB='" + dob.Text + "' , Class_join='" + cboclassJ.Text + "' ,Fphone='" + txtfphone.Text + "' ,Mphone='" + txtmphone.Text + "' ,SMS='" + txtsms.Text + "' ,SMS1='" + txtsms1.Text + "',roll_no='" + cboroll.Text + "',Classid='" + cboclass.SelectedValue + "',Identification1 ='" + txtMole.Text + "',Identification2 ='" + txtScar.Text + "',Religion ='" + cboReligion.Text + "',Caste = '" + txtCaste.Text + "',Nationality = '" + cmbNation.Text + "'   where uid='" + txtuid.Text + "'";
                conn.Close();
                conn.Open();
                cmd = new SqlCommand(QueryUpdate, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Record Updated Sucessfully");
                clearTxt();
                GBList.Visible = false;
                GBMain.Visible = true;
                Load_grid();
                btnadd.Visible = true;
                btnedit.Visible = true;
                btnexit.Visible = true;
                btnsave.Visible = false;
                btnaddrcan.Visible = false;
            }            
        }

        public void clearTxt()
        {
            txtadno.Text = "";
            txtsname.Text = "";
            txtfname.Text = "";
            txtmname.Text = "";
            txtAddr.Text = "";
            cbosex.SelectedIndex = -1;
            cboReligion.SelectedIndex = -1;
            cboclassJ.SelectedIndex = -1;
            txtfphone.Text = "";
            txtmphone.Text = "";
            txtsms.Text = "";
            txtsms1.Text = "";
            cboclass.SelectedIndex = -1;
            cboroll.SelectedIndex = -1;
            cbocom.SelectedIndex = -1;
            txtCaste.Text = string.Empty;

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true ;
            GBMain.Visible = false;

            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtadno .Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtfname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtfphone.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            dob.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[6].Value.ToString();           
            txtmname.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            txtAddr.Text = Dgv.Rows[i].Cells[9].Value.ToString();
            doa.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtmphone.Text = Dgv.Rows[i].Cells[12].Value.ToString();
            txtsms .Text = Dgv.Rows[i].Cells[13].Value.ToString();
            txtsms1.Text = Dgv.Rows[i].Cells[14].Value.ToString();
            cboroll.Text = Dgv.Rows[i].Cells[15].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[17].Value.ToString();
            txtMole.Text = Dgv.Rows[i].Cells[18].Value.ToString();
            txtScar.Text = Dgv.Rows[i].Cells[19].Value.ToString();
            txtCaste.Text = Dgv.Rows[i].Cells[20].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[21].Value.ToString();
            btnsave .Text = "Update";
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                conn.Open();                
                string qur = "SELECT uid, Admisno, SName, FName,Fphone,DOB,Sex,Community,MName,Address,DOA,Class_join, Mphone, SMS, SMS1,roll_no,Classid,Class_Desc,Identification1,Identification2,Caste,Religion,Nationality from studentm a left join Class_Mast b on a.Classid =b.cid where a.Tag ='A' and Admisno like '%" + txtscr.Text + "%' and SName like '%" + txtscr2.Text + "%' and FName like '%" + txtscr3.Text + "%' and Fphone like '%" + txtscr4.Text + "%' and dob like '%" + txtscr5.Text + "%' and Class_Desc like '%" + txtscr6.Text + "%'  order by convert(integer,Admisno,105)";
                cmd = new SqlCommand(qur, conn);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                if(tap.Rows.Count == 0)
                {
                    MessageBox.Show("No Records Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 23;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Admisno";
                Dgv.Columns[1].HeaderText = "Admission No";
                Dgv.Columns[1].DataPropertyName = "Admisno";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "SName";
                Dgv.Columns[2].HeaderText = "Student Name";
                Dgv.Columns[2].DataPropertyName = "SName";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "FName";
                Dgv.Columns[3].HeaderText = "Father Name";
                Dgv.Columns[3].DataPropertyName = "FName";
                Dgv.Columns[3].Width = 150;

                Dgv.Columns[4].Name = "Fphone";
                Dgv.Columns[4].HeaderText = "Father phone";
                Dgv.Columns[4].DataPropertyName = "Fphone";
                Dgv.Columns[4].Width = 100;

                Dgv.Columns[5].Name = "DOB";
                Dgv.Columns[5].HeaderText = "DOB";
                Dgv.Columns[5].DataPropertyName = "DOB";
                Dgv.Columns[5].Width = 100;


                Dgv.Columns[6].Name = "Sex";
                Dgv.Columns[6].HeaderText = "Gender";
                Dgv.Columns[6].DataPropertyName = "Sex";
                Dgv.Columns[6].Visible = false;


                Dgv.Columns[7].Name = "Community";
                Dgv.Columns[7].HeaderText = "Community";
                Dgv.Columns[7].DataPropertyName = "Community";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "MName";
                Dgv.Columns[8].HeaderText = "MName";
                Dgv.Columns[8].DataPropertyName = "MName";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "Address";
                Dgv.Columns[9].HeaderText = "Address";
                Dgv.Columns[9].DataPropertyName = "Address";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "DOA";
                Dgv.Columns[10].HeaderText = "DOA No";
                Dgv.Columns[10].DataPropertyName = "DOA";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "Class_join";
                Dgv.Columns[11].HeaderText = "Class_join";
                Dgv.Columns[11].DataPropertyName = "Class_join";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "Mphone";
                Dgv.Columns[12].HeaderText = "Mphone";
                Dgv.Columns[12].DataPropertyName = "Mphone";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "SMS";
                Dgv.Columns[13].HeaderText = "SMS";
                Dgv.Columns[13].DataPropertyName = "SMS";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].Name = "SMS1";
                Dgv.Columns[14].HeaderText = "SMS1";
                Dgv.Columns[14].DataPropertyName = "SMS1";
                Dgv.Columns[14].Visible = false;


                Dgv.Columns[15].Name = "roll_no";
                Dgv.Columns[15].HeaderText = "roll_no";
                Dgv.Columns[15].DataPropertyName = "roll_no";
                Dgv.Columns[15].Visible = false;

                Dgv.Columns[16].DataPropertyName = "Classid";
                Dgv.Columns[16].Visible = false;

                Dgv.Columns[17].DataPropertyName = "Class_Desc";
                Dgv.Columns[17].HeaderText = "Class";
                Dgv.Columns[17].DataPropertyName = "Class_Desc";
                Dgv.Columns[17].Width  = 100;

                Dgv.Columns[18].Name = "Identification1";
                Dgv.Columns[18].HeaderText = "Identification1";
                Dgv.Columns[18].DataPropertyName = "Identification1";
                Dgv.Columns[18].Visible = false;

                Dgv.Columns[19].Name = "Identification2";
                Dgv.Columns[19].HeaderText = "Identification2";
                Dgv.Columns[19].DataPropertyName = "Identification2";
                Dgv.Columns[19].Visible = false;

                Dgv.Columns[20].Name = "Caste";
                Dgv.Columns[20].HeaderText = "Caste";
                Dgv.Columns[20].DataPropertyName = "Caste";
                Dgv.Columns[20].Visible = false;

                Dgv.Columns[21].Name = "Religion";
                Dgv.Columns[21].HeaderText = "Religion";
                Dgv.Columns[21].DataPropertyName = "Religion";
                Dgv.Columns[21].Visible = false;

                Dgv.Columns[22].Name = "Nationality";
                Dgv.Columns[22].HeaderText = "Nationality";
                Dgv.Columns[22].DataPropertyName = "Nationality";
                Dgv.Columns[22].Visible = false;
                Dgv.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void cboclassJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cboclass_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbosex_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbocom_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cboroll_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetAllClass("SP_GetAdmissionNo", conn);
            txtadno.Text = dt.Rows[0]["AdmissionNO"].ToString();
            //label21.Visible = false;
            //textBox1.Visible = false;
        }

        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnedit_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;


            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;

            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            clearTxt();
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtadno.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtfname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtfphone.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            dob.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txtmname.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            txtAddr.Text = Dgv.Rows[i].Cells[9].Value.ToString();
            doa.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtmphone.Text = Dgv.Rows[i].Cells[12].Value.ToString();
            txtsms.Text = Dgv.Rows[i].Cells[13].Value.ToString();
            txtsms1.Text = Dgv.Rows[i].Cells[14].Value.ToString();
            cboroll.Text = Dgv.Rows[i].Cells[15].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[17].Value.ToString();
            txtMole.Text = Dgv.Rows[i].Cells[18].Value.ToString();
            txtScar.Text = Dgv.Rows[i].Cells[19].Value.ToString();
            txtCaste.Text = Dgv.Rows[i].Cells[20].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[21].Value.ToString();
            cbocom.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            cmbNation.Text = Dgv.Rows[i].Cells[22].Value.ToString();
            btnsave.Text = "Update";
        }

        private void GBMain_Enter(object sender, EventArgs e)
        {

        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        //private void cmbScholarship_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    textBox1.Text = Convert.ToInt32(comboBox1.SelectedValue).ToString();
        //    label21.Visible = true;
        //    textBox1.Visible = true;
        //}
    }
}
