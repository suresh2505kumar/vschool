﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;
namespace SkoolRPWD
{
    public partial class FrmOverdue : Form
    {
        public FrmOverdue()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand qur1 = new SqlCommand();

        private void FrmOverdue_Load(object sender, EventArgs e)
        {
            LoadRefT();

            qur1.Connection = con;
        }

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;
            con.Close();

        }

        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Module.type = 11;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtclass.Text = "";
                txtclass.Focus();
            }
        }

        private void loadput()
        {
            con.Close();
            con.Open();

            if (Module.type == 11)
            {
                
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";

                Module.FSSQLSortStr = "Class_Desc";

            }
           


            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 11)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }

            
            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 11)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
               

                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }

            dt.DataSource = tap;


            contc.Show();
            con.Close();


        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtcid.Text != "")
            {

            }
        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            Module.Dtype = 20;
            if (cbbo.Text == "ClassWise")
            {
                if (txtclass.Text == "")
                {
                    MessageBox.Show("Select The Class Name ");
                    txtclass.Focus();
                    return;
                }
            }

            if (cbbo.Text == "ClassWise")
            {
                Module.Dtype = 20;
                Module.cid = Convert.ToInt32(txtcid.Text);
                Module.termuid = Convert.ToInt32(cboTerm.SelectedValue);
                CRViewer crv = new CRViewer
                {
                    MdiParent = this.MdiParent
                };
                crv.Show();
                con.Close();
            }
            else
            {
                Module.Dtype = 51;
                Module.termuid = Convert.ToInt32(cboTerm.SelectedValue);
                CRViewer crv = new CRViewer
                {
                    MdiParent = this.MdiParent
                };
                crv.Show();
                con.Close();
            }
        }

        private void txtclass_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Module.type = 11;
            //    loadput();


            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtclass.Text = "";
            //    txtclass.Focus();
            //}
        }

        private void txtclass_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtp_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 11;
            loadput();

        }

        private void cbbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbo.Text == "ClassWise")
            {
                label1.Visible = true;
                txtclass .Visible = true;
            }
            else
            {
                label1.Visible = false;
                txtclass.Visible = false;
            }
        }

    }
}
