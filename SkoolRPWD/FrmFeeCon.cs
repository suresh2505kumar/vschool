﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace SkoolRPWD
{
    public partial class FrmFeeCon : Form
    {
        public FrmFeeCon()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboTerm.DataSource = null;           
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.DataSource = tap;
            cboTerm.SelectedIndex = -1;
            con.Close();

        }

        private void FrmFeeCon_Load(object sender, EventArgs e)
        {
            GBMain.Visible = true;
            GBList.Visible = false;
            LoadRefT();
            Titlep();
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            BtnDelete.Visible = false;
            btnaddrcan.Visible = true;
            txtclass.Focus();
            btnsave.Text = "Save";
        }

        private void txtclass_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void Loadlistgrid()
        {
            if (txtcid.Text != string.Empty)
            {
                txtCAmt.Text = string.Empty;
                txtFamt.Text = string.Empty;               
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetFeesForConcession";
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@StudId", Convert.ToInt32(txtsid.Text));
                cmd.Parameters.AddWithValue("@TermId", cboTerm.SelectedValue);
                cmd.Parameters.AddWithValue("@ClassId", Convert.ToInt32(txtcid.Text));
                adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);
                Titlep();
                double sum = 0;
                double sum1 = 0;
                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {
                    i = Dgvlist.Rows.Add();
                    Dgvlist.Rows[i].Cells[0].Value = dt.Tables[0].Rows[i][0].ToString();
                    Dgvlist.Rows[i].Cells[1].Value = dt.Tables[0].Rows[i][1].ToString();
                    Dgvlist.Rows[i].Cells[2].Value = dt.Tables[0].Rows[i][2].ToString();
                    Dgvlist.Rows[i].Cells[3].Value = dt.Tables[0].Rows[i][3].ToString();
                    Dgvlist.Rows[i].Cells[4].Value = dt.Tables[0].Rows[i][4].ToString();
                    sum += Convert.ToDouble(dt.Tables[0].Rows[i][2].ToString());
                    txtFamt.Text = sum.ToString();
                    Dgvlist.Rows[i].Cells[4].Value = Dgvlist.Rows[i].Cells[2].Value;
                    sum1 += Convert.ToDouble(Dgvlist.Rows[i].Cells[4].Value);
                    txtfeeconA.Text = sum1.ToString();
                }
            }
            else
            {

            }

        }

        private void loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";

            con.Close();
            con.Open();
            if (Module.type == 3)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1 order by Class_Desc";
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 4)
            {
                FunC.Partylistviewcont3("uid", "name", "classid", Module.strsql, this, txtsid, txtstud, txtcid, GBList);
                Module.strsql = "select uid, sname + '/' + Admisno as name,classid from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 3)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }

            else if (Module.type == 4)
            {
                dt.Columns[1].Width = 261;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 3)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 4)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }

                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void txtstud_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            BtnDelete.Visible = true;
        }

        private void Titlep()
        {
            Dgvlist.ColumnCount = 5;
            Dgvlist.Columns[0].Name = "fid";
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].DataPropertyName = "fid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "descp";
            Dgvlist.Columns[1].HeaderText = "Fee";
            Dgvlist.Columns[1].DataPropertyName = "descp";
            Dgvlist.Columns[1].Width = 200;

            Dgvlist.Columns[2].Name = "Fee Amount";
            Dgvlist.Columns[2].Width = 130;

            Dgvlist.Columns[3].Name = "Concession Amount";
            Dgvlist.Columns[3].Width = 140;

            Dgvlist.Columns[4].Name = "Fee Concession";
            Dgvlist.Columns[4].Width = 140;
            this.Dgvlist.Columns["Fee Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.Dgvlist.Columns["Concession Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.Dgvlist.Columns["Fee Concession"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        public void clearTxt()
        {
            txtdesc.Text = string.Empty;
            txtstud.Text = string.Empty;
            txtFamt.Text = string.Empty;
            txtCAmt.Text = string.Empty;
            txtfeeconA.Text = string.Empty;
            txtcid.Text = string.Empty;
            txtclass.Text = string.Empty;
            cboTerm.SelectedIndex = -1;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtdesc.Text == string.Empty)
            {
                MessageBox.Show("Enter the Description","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
                txtdesc.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                string qur = "select * from Feecon where  classid=" + txtcid.Text + "and Termuid =" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + "";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into Feecon values(@classid,@studid,@termuid,@Amt,@ConAmt,@descp,@DT,@FeeconA)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@classid", SqlDbType.Int).Value = txtcid.Text;
                    cmd.Parameters.AddWithValue("@studid", SqlDbType.Int).Value = txtsid.Text;
                    cmd.Parameters.AddWithValue("@termuid", SqlDbType.Int).Value = cboTerm.SelectedValue;
                    if (txtFamt.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@Amt", SqlDbType.Decimal).Value = "";
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Amt", SqlDbType.Decimal).Value = txtFamt.Text;
                    }
                    if (txtCAmt.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@ConAmt", SqlDbType.Decimal).Value = 0;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ConAmt", SqlDbType.Decimal).Value = txtCAmt.Text;
                    }
                    cmd.Parameters.AddWithValue("@descp", SqlDbType.NVarChar).Value = txtdesc.Text;
                    cmd.Parameters.AddWithValue("@DT", SqlDbType.NVarChar).Value = dtp.Value;
                    if (txtfeeconA.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@FeeconA", SqlDbType.Decimal).Value = 0;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@FeeconA", SqlDbType.Decimal).Value = txtfeeconA.Text;
                    }
                    cmd.ExecuteNonQuery();

                    string strSQL = "select uid from Feecon where classid=" + txtcid.Text + " and studid=" + txtsid.Text + " and Termuid =" + cboTerm.SelectedValue + "";
                    cmd = new SqlCommand(strSQL, con);
                    int uid = (int)cmd.ExecuteScalar();

                    for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                    {
                        if (Dgvlist.Rows[i].Cells[3].Value.ToString() == "")
                        {
                            Dgvlist.Rows[i].Cells[3].Value = 0;
                            res = "insert into Feecon_det (fconid,Amt,ConAmt,FeeCon,feeid) values(" + uid + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + "," + Dgvlist.Rows[i].Cells[4].Value + "," + Dgvlist.Rows[i].Cells[0].Value + ")";
                            SqlCommand scmd = new SqlCommand(res, con);
                            scmd.ExecuteNonQuery();
                        }
                        else
                        {
                            res = "insert into Feecon_det (fconid,Amt,ConAmt,FeeCon,feeid) values(" + uid + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + "," + Dgvlist.Rows[i].Cells[4].Value + "," + Dgvlist.Rows[i].Cells[0].Value + ")";
                            SqlCommand scmd = new SqlCommand(res, con);
                            scmd.ExecuteNonQuery();
                        }
                    }
                    string qur1 = "select a.uid as fuid,a.studid ,a.termuid,b.ConAmt ,b.feeid ,c.descp,b.Amt  from Feecon a inner join Feecon_det b on a.uid=b.fconid inner join FeeMast c on b.feeid=c.Fid where a.uid =" + uid + " ";
                    cmd = new SqlCommand(qur1, con);
                    adpt = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    SqlCommand cmnd = new SqlCommand();
                    cmnd.Connection = con;
                    con.Close();
                    con.Open();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        double s = Convert.ToDouble(dt.Rows[i]["Amt"].ToString());
                        double s1 = Convert.ToDouble(dt.Rows[i]["ConAmt"].ToString());
                        double j = s - s1;

                        string qu = "update PLFType set concession =" + dt.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + dt.Rows[i]["studid"].ToString() + "  and feeid=" + dt.Rows[i]["feeid"].ToString() + "  and termuid=" + dt.Rows[i]["termuid"].ToString() + " ";
                        SqlCommand scmd1 = new SqlCommand(qu, con);
                        scmd1.ExecuteNonQuery();
                    }
                    MessageBox.Show("Concession Posted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //clearTxt();
                    con.Close();
                    Dgvlist.Rows.Clear();
                    Load_grid();
                   
                }
                else
                {
                    MessageBox.Show("Concession alredy Posted to this student", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearTxt();
                    con.Close();
                    Dgvlist.Rows.Clear();
                }
            }
            else
            {
                con.Close();
                string qur = "select * from Feecon where  classid=" + txtcid.Text + " and studid=" + txtsid.Text + "and Termuid = " + cboTerm.SelectedValue + "";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count != 0)
                {
                    string res = "update Feecon set classid =" + txtcid.Text + ",studid=" + txtsid.Text + ",termuid=" + cboTerm.SelectedValue + ",Amt=" + txtFamt.Text + ",ConAmt=" + txtCAmt.Text + ",descp='" + txtdesc.Text + "',DT='" + dtp.Value + "',FeeconA=" + txtfeeconA.Text + " where uid=" + txtuid.Text + "";
                    cmd = new SqlCommand(res, con);
                    con.Open();
                    cmd.ExecuteNonQuery();

                    string strSQL = "select uid from Feecon where classid=" + txtcid.Text + " and studid=" + txtsid.Text + " and Termuid =" + cboTerm.SelectedValue + "";
                    cmd = new SqlCommand(strSQL, con);
                    int uid = (int)cmd.ExecuteScalar();

                    string str = "Delete from Feecon_det where fconid=" + uid + "";
                    cmd = new SqlCommand(str, con);
                    cmd.ExecuteNonQuery();
                    for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                    {
                        string res2 = "insert into Feecon_det (fconid,Amt,ConAmt,FeeCon,feeid) values(" + uid + "," + Dgvlist.Rows[i].Cells[2].Value + "," + Dgvlist.Rows[i].Cells[3].Value + "," + Dgvlist.Rows[i].Cells[4].Value + "," + Dgvlist.Rows[i].Cells[0].Value + ")";
                        SqlCommand scmd = new SqlCommand(res2, con);
                        scmd.ExecuteNonQuery();
                    }
                    string qur1 = "select a.uid as fuid,a.studid ,a.termuid,b.ConAmt ,b.feeid ,c.descp,b.amt  from Feecon a inner join Feecon_det b on a.uid=b.fconid inner join FeeMast c on b.feeid=c.Fid where a.uid =" + uid + " ";
                    cmd = new SqlCommand(qur1, con);
                    adpt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    SqlCommand cmnd = new SqlCommand();
                    cmnd.Connection = con;
                    con.Close();
                    con.Open();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        double s = Convert.ToDouble(dt.Rows[i]["Amt"].ToString());
                        double s1 = Convert.ToDouble(dt.Rows[i]["ConAmt"].ToString());
                        double j = s - s1;
                        string qu = "update PLFType set concession =" + dt.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + dt.Rows[i]["studid"].ToString() + "  and feeid=" + dt.Rows[i]["feeid"].ToString() + "  and termuid=" + dt.Rows[i]["termuid"].ToString() + " ";
                        SqlCommand scmd1 = new SqlCommand(qu, con);
                        scmd1.ExecuteNonQuery();
                    }
                    MessageBox.Show("Record Updated Sucessfully");
                    //clearTxt();
                    Dgvlist.Rows.Clear();
                    con.Close();
                    Load_grid();
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
            }
            GBList.Visible = true;
            GBMain.Visible = false;
            Dgvlist.Rows.Clear();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            BtnDelete.Visible = false;
            txtclass.Focus();
            btnsave.Text = "Save";
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            BtnDelete.Visible = false;
            btnaddrcan.Visible = true;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtstud.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            cboTerm.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtFamt.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            txtCAmt.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            txtfeeconA.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            dtp.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            txtdesc.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txtsid.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            txtclass.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtcid.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            LoadFeeGrid();
            btnsave.Text = "Update";
        }

        private void LoadFeeGrid()
        {
            con.Close();
            con.Open();
            string qur = "select a.feeid,Descp,Cast(a.amt as decimal(18,0)) amt,Cast(a.conamt as decimal(18,0))conamt,Cast(feecon as decimal(18,0)) feecon  from Feecon_det a inner join FeeMast b on a.feeid=b.Fid  where fconid=" + Dgv.CurrentRow.Cells[0].Value.ToString() + "";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.SelectCommand = cmd;
            adpt.Fill(dt);
            Dgvlist.Refresh();
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].Visible = false;
            Dgvlist.Columns[1].Name = "descp";
            Dgvlist.Columns[1].HeaderText = "Description";
            Dgvlist.Columns[1].DataPropertyName = "descp";
            Dgvlist.Columns[1].Width = 200;

            Dgvlist.Columns[2].HeaderText = "Fee Amount";
            Dgvlist.Columns[2].Width = 130;

            Dgvlist.Columns[3].HeaderText = "Concession Amount";
            Dgvlist.Columns[3].Width = 140;

            Dgvlist.Columns[4].HeaderText = "Fee Concession";
            Dgvlist.Columns[4].Width = 140;

            for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
            {
                i = Dgvlist.Rows.Add();
                Dgvlist.Rows[i].Cells[0].Value = dt.Tables[0].Rows[i][0].ToString();
                Dgvlist.Rows[i].Cells[1].Value = dt.Tables[0].Rows[i][1].ToString();
                Dgvlist.Rows[i].Cells[2].Value = dt.Tables[0].Rows[i][2].ToString();
                Dgvlist.Rows[i].Cells[3].Value = dt.Tables[0].Rows[i][3].ToString();
                Dgvlist.Rows[i].Cells[4].Value = dt.Tables[0].Rows[i][4].ToString();
            }
        }

        private void Load_grid()
        {
            try
            {
                con.Open();
                string qur = "select c.Admisno,a.uid,class_desc,sname,refname,cast(amt as int)amt,cast(conamt as int)conamt,descp,a.classid,a.studid,a.termuid,DT,cast(FeeconA as int)FeeconA from Feecon a inner join Class_Mast b on a.classid=b.Cid inner join StudentM c on a.studid=c.uid inner join feeref d on a.termuid=d.Ruid  ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 13;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "class_desc";
                Dgv.Columns[1].HeaderText = "class";
                Dgv.Columns[1].DataPropertyName = "class_desc";
                Dgv.Columns[1].Width = 110;

                Dgv.Columns[2].DataPropertyName = "sname";
                Dgv.Columns[2].HeaderText = "Student Name";
                Dgv.Columns[2].DataPropertyName = "sname";
                Dgv.Columns[2].Width = 200;

                Dgv.Columns[3].DataPropertyName = "refname";
                Dgv.Columns[3].HeaderText = "Term";
                Dgv.Columns[3].DataPropertyName = "refname";
                Dgv.Columns[3].Width = 100;

                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].HeaderText = "Amount";
                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].Width = 110;

                Dgv.Columns[5].DataPropertyName = "conamt";
                Dgv.Columns[5].HeaderText = "Concession";
                Dgv.Columns[5].DataPropertyName = "conamt";
                Dgv.Columns[5].Width = 110;

                Dgv.Columns[6].DataPropertyName = "descp";
                Dgv.Columns[6].HeaderText = "descp";
                Dgv.Columns[6].DataPropertyName = "descp";
                Dgv.Columns[6].Visible = false;

                Dgv.Columns[7].DataPropertyName = "classid";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].DataPropertyName = "studid";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].DataPropertyName = "termuid";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].DataPropertyName = "DT";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].DataPropertyName = "FeeconA";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].DataPropertyName = "Admission No";
                Dgv.Columns[12].HeaderText = "Admission No";
                Dgv.Columns[12].DataPropertyName = "Admisno";                
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void txtclass_TextChanged(object sender, EventArgs e)
        {
        }

        private void Dgvlist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgvlist_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Dgvlist_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Dgvlist_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            double sum = 0;
            double sum1 = 0;

            for (int i = 0; i < Dgvlist.RowCount - 1; i++)
            {
                double s = Convert.ToDouble(Dgvlist.Rows[i].Cells[2].Value);
                if ((Dgvlist.Rows[i].Cells[3].Value.ToString() == ""))
                {
                    Dgvlist.Rows[i].Cells[3].Value = 0;
                    double s1 = Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                    Dgvlist.Rows[i].Cells[4].Value = Convert.ToString(s - s1);
                }
                else
                {
                    double s1 = Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                    Dgvlist.Rows[i].Cells[4].Value = Convert.ToString(s - s1);
                }
                sum += Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                txtCAmt.Text = sum.ToString();
                sum1 += Convert.ToDouble(Dgvlist.Rows[i].Cells[4].Value);
                txtfeeconA.Text = sum1.ToString();
                if (Dgvlist.Rows[i].Cells[0].Value.ToString() == "")
                {
                    Dgvlist.ReadOnly = true;
                }

            }
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select  a.uid,class_desc,sname,refname,amt,conamt,descp,a.classid,a.studid,a.termuid,DT,FeeconA from Feecon a inner join Class_Mast b on a.classid=b.Cid inner join StudentM c on a.studid=c.uid inner join feeref d on a.termuid=d.Ruid where class_desc like '%" + txtscr.Text + "%' and sname like '%" + txtscr1.Text + "%' and refname like '%" + txtscr2.Text + "%' and amt like '%" + txtscr3.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 12;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "class_desc";
                Dgv.Columns[1].HeaderText = "class";
                Dgv.Columns[1].DataPropertyName = "class_desc";
                Dgv.Columns[1].Width = 110;

                Dgv.Columns[2].DataPropertyName = "sname";
                Dgv.Columns[2].HeaderText = "Student Name";
                Dgv.Columns[2].DataPropertyName = "sname";
                Dgv.Columns[2].Width = 200;

                Dgv.Columns[3].DataPropertyName = "refname";
                Dgv.Columns[3].HeaderText = "Term";
                Dgv.Columns[3].DataPropertyName = "refname";
                Dgv.Columns[3].Width = 100;

                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].HeaderText = "Amount";
                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].Width = 120;

                Dgv.Columns[5].DataPropertyName = "conamt";
                Dgv.Columns[5].HeaderText = "Concession";
                Dgv.Columns[5].DataPropertyName = "conamt";
                Dgv.Columns[5].Width = 120;

                Dgv.Columns[6].DataPropertyName = "descp";
                Dgv.Columns[6].HeaderText = "descp";
                Dgv.Columns[6].DataPropertyName = "descp";
                Dgv.Columns[6].Visible = false;

                Dgv.Columns[7].DataPropertyName = "classid";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].DataPropertyName = "studid";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].DataPropertyName = "termuid";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].DataPropertyName = "DT";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].DataPropertyName = "FeeconA";
                Dgv.Columns[11].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dgvlist.Rows.Clear();
            Loadlistgrid();
        }

        private void Dgvlist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Dgvlist.Rows[e.RowIndex].ReadOnly = false;

            if (Dgvlist.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
            {

                Dgvlist.Rows[e.RowIndex].ReadOnly = true;

            }
        }

        private void txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Dgvlist.Refresh();
            Dgvlist.Rows.Clear();

            Module.type = 3;
            loadput();


        }

        private void txtstud_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Select The Class Name ");
                txtclass.Focus();
                return;
            }

            Module.type = 4;
            loadput();
        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = Dgv.SelectedCells[0].RowIndex;
                int uid = Convert.ToInt32(Dgv.Rows[Index].Cells[8].Value.ToString());
                int TermUid = Convert.ToInt32(Dgv.Rows[Index].Cells[9].Value.ToString());
                SqlParameter[] sqlParameters = { new SqlParameter("@Studid", uid), new SqlParameter("@termuid", TermUid) };
                CommonClass cmc = new CommonClass();
                cmc.ExecuteNonQuery("Proc_deleteconcessoin", sqlParameters, con);
                MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
