﻿namespace SkoolRPWD
{
    partial class FrmStud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStud));
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.GBList = new System.Windows.Forms.GroupBox();
            this.cmbNation = new System.Windows.Forms.ComboBox();
            this.Nation = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbocom = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCaste = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtScar = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMole = new System.Windows.Forms.TextBox();
            this.cboroll = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cboclass = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtsms1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtsms = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtmphone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtfphone = new System.Windows.Forms.TextBox();
            this.cboclassJ = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dob = new System.Windows.Forms.DateTimePicker();
            this.doa = new System.Windows.Forms.DateTimePicker();
            this.txtAddr = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmname = new System.Windows.Forms.TextBox();
            this.cboReligion = new System.Windows.Forms.ComboBox();
            this.Caste = new System.Windows.Forms.Label();
            this.cbosex = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtadno = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.GBList.SuspendLayout();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBMain.Controls.Add(this.txtscr6);
            this.GBMain.Controls.Add(this.txtscr5);
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.txtscr3);
            this.GBMain.Controls.Add(this.txtscr4);
            this.GBMain.Controls.Add(this.txtscr2);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Location = new System.Drawing.Point(0, 3);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(800, 463);
            this.GBMain.TabIndex = 1;
            this.GBMain.TabStop = false;
            this.GBMain.Enter += new System.EventHandler(this.GBMain_Enter);
            // 
            // txtscr6
            // 
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(629, 11);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(92, 26);
            this.txtscr6.TabIndex = 90;
            // 
            // txtscr5
            // 
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(529, 11);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(100, 26);
            this.txtscr5.TabIndex = 89;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(721, 9);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(73, 30);
            this.btnser.TabIndex = 88;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.btnser_Click);
            // 
            // txtscr3
            // 
            this.txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(278, 11);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(150, 26);
            this.txtscr3.TabIndex = 87;
            // 
            // txtscr4
            // 
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(428, 11);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(100, 26);
            this.txtscr4.TabIndex = 86;
            // 
            // txtscr2
            // 
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(128, 11);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(150, 26);
            this.txtscr2.TabIndex = 85;
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(28, 11);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(100, 26);
            this.txtscr.TabIndex = 84;
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.GridColor = System.Drawing.SystemColors.Control;
            this.Dgv.Location = new System.Drawing.Point(27, 39);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(767, 418);
            this.Dgv.TabIndex = 0;
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.cmbNation);
            this.GBList.Controls.Add(this.Nation);
            this.GBList.Controls.Add(this.label19);
            this.GBList.Controls.Add(this.cbocom);
            this.GBList.Controls.Add(this.label4);
            this.GBList.Controls.Add(this.txtCaste);
            this.GBList.Controls.Add(this.label18);
            this.GBList.Controls.Add(this.txtScar);
            this.GBList.Controls.Add(this.label17);
            this.GBList.Controls.Add(this.txtMole);
            this.GBList.Controls.Add(this.cboroll);
            this.GBList.Controls.Add(this.label16);
            this.GBList.Controls.Add(this.cboclass);
            this.GBList.Controls.Add(this.label15);
            this.GBList.Controls.Add(this.txtuid);
            this.GBList.Controls.Add(this.label14);
            this.GBList.Controls.Add(this.txtsms1);
            this.GBList.Controls.Add(this.label13);
            this.GBList.Controls.Add(this.txtsms);
            this.GBList.Controls.Add(this.label12);
            this.GBList.Controls.Add(this.txtmphone);
            this.GBList.Controls.Add(this.label11);
            this.GBList.Controls.Add(this.txtfphone);
            this.GBList.Controls.Add(this.cboclassJ);
            this.GBList.Controls.Add(this.label8);
            this.GBList.Controls.Add(this.label10);
            this.GBList.Controls.Add(this.label9);
            this.GBList.Controls.Add(this.dob);
            this.GBList.Controls.Add(this.doa);
            this.GBList.Controls.Add(this.txtAddr);
            this.GBList.Controls.Add(this.label7);
            this.GBList.Controls.Add(this.label6);
            this.GBList.Controls.Add(this.txtfname);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.txtmname);
            this.GBList.Controls.Add(this.cboReligion);
            this.GBList.Controls.Add(this.Caste);
            this.GBList.Controls.Add(this.cbosex);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtsname);
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.txtadno);
            this.GBList.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBList.Location = new System.Drawing.Point(1, -1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(799, 467);
            this.GBList.TabIndex = 2;
            this.GBList.TabStop = false;
            // 
            // cmbNation
            // 
            this.cmbNation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNation.FormattingEnabled = true;
            this.cmbNation.Items.AddRange(new object[] {
            "INDIAN"});
            this.cmbNation.Location = new System.Drawing.Point(138, 326);
            this.cmbNation.Name = "cmbNation";
            this.cmbNation.Size = new System.Drawing.Size(121, 26);
            this.cmbNation.TabIndex = 359;
            // 
            // Nation
            // 
            this.Nation.AutoSize = true;
            this.Nation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nation.Location = new System.Drawing.Point(56, 330);
            this.Nation.Name = "Nation";
            this.Nation.Size = new System.Drawing.Size(76, 18);
            this.Nation.TabIndex = 358;
            this.Nation.Text = "Nationality";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(52, 397);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 18);
            this.label19.TabIndex = 119;
            this.label19.Text = "Community";
            // 
            // cbocom
            // 
            this.cbocom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocom.FormattingEnabled = true;
            this.cbocom.Items.AddRange(new object[] {
            "BC",
            "MBC",
            "OC",
            "SC",
            "Others"});
            this.cbocom.Location = new System.Drawing.Point(137, 393);
            this.cbocom.Name = "cbocom";
            this.cbocom.Size = new System.Drawing.Size(121, 26);
            this.cbocom.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(73, 365);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 18);
            this.label4.TabIndex = 117;
            this.label4.Text = "Religion";
            // 
            // txtCaste
            // 
            this.txtCaste.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaste.Location = new System.Drawing.Point(136, 427);
            this.txtCaste.Name = "txtCaste";
            this.txtCaste.Size = new System.Drawing.Size(346, 26);
            this.txtCaste.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(39, 295);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 18);
            this.label18.TabIndex = 115;
            this.label18.Text = "Idendification";
            // 
            // txtScar
            // 
            this.txtScar.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScar.Location = new System.Drawing.Point(137, 291);
            this.txtScar.MaxLength = 250;
            this.txtScar.Name = "txtScar";
            this.txtScar.Size = new System.Drawing.Size(268, 26);
            this.txtScar.TabIndex = 16;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(39, 259);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 18);
            this.label17.TabIndex = 113;
            this.label17.Text = "Idendification";
            // 
            // txtMole
            // 
            this.txtMole.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMole.Location = new System.Drawing.Point(137, 255);
            this.txtMole.MaxLength = 250;
            this.txtMole.Name = "txtMole";
            this.txtMole.Size = new System.Drawing.Size(268, 26);
            this.txtMole.TabIndex = 15;
            // 
            // cboroll
            // 
            this.cboroll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboroll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboroll.FormattingEnabled = true;
            this.cboroll.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50"});
            this.cboroll.Location = new System.Drawing.Point(513, 199);
            this.cboroll.Name = "cboroll";
            this.cboroll.Size = new System.Drawing.Size(121, 26);
            this.cboroll.TabIndex = 13;
            this.cboroll.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboroll_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(416, 203);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 18);
            this.label16.TabIndex = 111;
            this.label16.Text = "Roll No";
            // 
            // cboclass
            // 
            this.cboclass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboclass.FormattingEnabled = true;
            this.cboclass.Location = new System.Drawing.Point(507, 15);
            this.cboclass.Name = "cboclass";
            this.cboclass.Size = new System.Drawing.Size(121, 26);
            this.cboclass.TabIndex = 8;
            this.cboclass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboclass_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(414, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 18);
            this.label15.TabIndex = 109;
            this.label15.Text = "Class ";
            // 
            // txtuid
            // 
            this.txtuid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuid.Location = new System.Drawing.Point(427, 302);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 26);
            this.txtuid.TabIndex = 108;
            this.txtuid.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(412, 338);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 18);
            this.label14.TabIndex = 107;
            this.label14.Text = "SMS1 Number";
            this.label14.Visible = false;
            // 
            // txtsms1
            // 
            this.txtsms1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsms1.Location = new System.Drawing.Point(513, 334);
            this.txtsms1.MaxLength = 250;
            this.txtsms1.Name = "txtsms1";
            this.txtsms1.Size = new System.Drawing.Size(213, 26);
            this.txtsms1.TabIndex = 7;
            this.txtsms1.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(44, 222);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 18);
            this.label13.TabIndex = 105;
            this.label13.Text = "SMS Number";
            // 
            // txtsms
            // 
            this.txtsms.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsms.Location = new System.Drawing.Point(137, 218);
            this.txtsms.MaxLength = 250;
            this.txtsms.Name = "txtsms";
            this.txtsms.Size = new System.Drawing.Size(247, 26);
            this.txtsms.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 103;
            this.label12.Text = "Mother Phone";
            // 
            // txtmphone
            // 
            this.txtmphone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmphone.Location = new System.Drawing.Point(137, 182);
            this.txtmphone.MaxLength = 250;
            this.txtmphone.Name = "txtmphone";
            this.txtmphone.Size = new System.Drawing.Size(247, 26);
            this.txtmphone.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(41, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 18);
            this.label11.TabIndex = 101;
            this.label11.Text = "Father Phone";
            // 
            // txtfphone
            // 
            this.txtfphone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfphone.Location = new System.Drawing.Point(137, 148);
            this.txtfphone.MaxLength = 250;
            this.txtfphone.Name = "txtfphone";
            this.txtfphone.Size = new System.Drawing.Size(247, 26);
            this.txtfphone.TabIndex = 4;
            // 
            // cboclassJ
            // 
            this.cboclassJ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboclassJ.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboclassJ.FormattingEnabled = true;
            this.cboclassJ.Items.AddRange(new object[] {
            "LKG",
            "UKG",
            "I",
            "II",
            "III",
            "IV",
            "V",
            "VI",
            "VII",
            "VIII",
            "IX",
            "X",
            "XI",
            "XII"});
            this.cboclassJ.Location = new System.Drawing.Point(511, 159);
            this.cboclassJ.Name = "cboclassJ";
            this.cboclassJ.Size = new System.Drawing.Size(121, 26);
            this.cboclassJ.TabIndex = 12;
            this.cboclassJ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboclassJ_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(416, 163);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 18);
            this.label8.TabIndex = 98;
            this.label8.Text = "Class Join";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(416, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 18);
            this.label10.TabIndex = 97;
            this.label10.Text = "DOB";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(416, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 18);
            this.label9.TabIndex = 96;
            this.label9.Text = "DOA";
            // 
            // dob
            // 
            this.dob.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dob.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dob.Location = new System.Drawing.Point(511, 125);
            this.dob.Name = "dob";
            this.dob.Size = new System.Drawing.Size(118, 26);
            this.dob.TabIndex = 11;
            // 
            // doa
            // 
            this.doa.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doa.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.doa.Location = new System.Drawing.Point(510, 88);
            this.doa.Name = "doa";
            this.doa.Size = new System.Drawing.Size(120, 26);
            this.doa.TabIndex = 10;
            // 
            // txtAddr
            // 
            this.txtAddr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddr.Location = new System.Drawing.Point(512, 240);
            this.txtAddr.Name = "txtAddr";
            this.txtAddr.Size = new System.Drawing.Size(275, 78);
            this.txtAddr.TabIndex = 14;
            this.txtAddr.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(416, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 18);
            this.label7.TabIndex = 91;
            this.label7.Text = "Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 18);
            this.label6.TabIndex = 89;
            this.label6.Text = "Father  Name";
            // 
            // txtfname
            // 
            this.txtfname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfname.Location = new System.Drawing.Point(137, 81);
            this.txtfname.MaxLength = 250;
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(247, 26);
            this.txtfname.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(38, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 18);
            this.label5.TabIndex = 87;
            this.label5.Text = "Mother Name";
            // 
            // txtmname
            // 
            this.txtmname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmname.Location = new System.Drawing.Point(137, 114);
            this.txtmname.MaxLength = 250;
            this.txtmname.Name = "txtmname";
            this.txtmname.Size = new System.Drawing.Size(247, 26);
            this.txtmname.TabIndex = 3;
            // 
            // cboReligion
            // 
            this.cboReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReligion.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboReligion.FormattingEnabled = true;
            this.cboReligion.Items.AddRange(new object[] {
            "Hindu",
            "Muslim",
            "Christian",
            "Others"});
            this.cboReligion.Location = new System.Drawing.Point(138, 361);
            this.cboReligion.Name = "cboReligion";
            this.cboReligion.Size = new System.Drawing.Size(121, 26);
            this.cboReligion.TabIndex = 17;
            this.cboReligion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbocom_KeyPress);
            // 
            // Caste
            // 
            this.Caste.AutoSize = true;
            this.Caste.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Caste.Location = new System.Drawing.Point(90, 430);
            this.Caste.Name = "Caste";
            this.Caste.Size = new System.Drawing.Size(42, 18);
            this.Caste.TabIndex = 84;
            this.Caste.Text = "Caste";
            // 
            // cbosex
            // 
            this.cbosex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosex.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosex.FormattingEnabled = true;
            this.cbosex.Items.AddRange(new object[] {
            "F",
            "M"});
            this.cbosex.Location = new System.Drawing.Point(508, 53);
            this.cbosex.Name = "cbosex";
            this.cbosex.Size = new System.Drawing.Size(121, 26);
            this.cbosex.TabIndex = 9;
            this.cbosex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbosex_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(414, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 82;
            this.label3.Text = "Gender";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 81;
            this.label1.Text = "Student  Name";
            // 
            // txtsname
            // 
            this.txtsname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsname.Location = new System.Drawing.Point(137, 47);
            this.txtsname.MaxLength = 250;
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(247, 26);
            this.txtsname.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 18);
            this.label2.TabIndex = 79;
            this.label2.Text = "Admission No";
            // 
            // txtadno
            // 
            this.txtadno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadno.Location = new System.Drawing.Point(138, 14);
            this.txtadno.MaxLength = 250;
            this.txtadno.Name = "txtadno";
            this.txtadno.Size = new System.Drawing.Size(247, 26);
            this.txtadno.TabIndex = 0;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(660, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(72, 30);
            this.btnsave.TabIndex = 77;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(731, 2);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 30);
            this.btnaddrcan.TabIndex = 76;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(1, 470);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(799, 35);
            this.panadd.TabIndex = 211;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(618, 2);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 80;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click_1);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(732, 2);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(63, 30);
            this.btnexit.TabIndex = 83;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click_1);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(677, 2);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 81;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click_1);
            // 
            // FrmStud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(805, 509);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GBMain);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student";
            this.Load += new System.EventHandler(this.FrmStud_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        internal System.Windows.Forms.TextBox txtadno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbosex;
        private System.Windows.Forms.ComboBox cboReligion;
        private System.Windows.Forms.Label Caste;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtmname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtAddr;
        private System.Windows.Forms.DateTimePicker dob;
        private System.Windows.Forms.DateTimePicker doa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboclassJ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        internal System.Windows.Forms.TextBox txtfphone;
        private System.Windows.Forms.Label label12;
        internal System.Windows.Forms.TextBox txtmphone;
        private System.Windows.Forms.Label label14;
        internal System.Windows.Forms.TextBox txtsms1;
        private System.Windows.Forms.Label label13;
        internal System.Windows.Forms.TextBox txtsms;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.ComboBox cboclass;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboroll;
        private System.Windows.Forms.Label label16;
        internal System.Windows.Forms.TextBox txtscr;
        internal System.Windows.Forms.TextBox txtscr2;
        internal System.Windows.Forms.TextBox txtscr3;
        internal System.Windows.Forms.TextBox txtscr4;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnexit;
        internal System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Label label18;
        internal System.Windows.Forms.TextBox txtScar;
        private System.Windows.Forms.Label label17;
        internal System.Windows.Forms.TextBox txtMole;
        private System.Windows.Forms.TextBox txtCaste;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbocom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbNation;
        private System.Windows.Forms.Label Nation;
    }
}

