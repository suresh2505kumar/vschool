﻿namespace SkoolRPWD
{
    partial class FrmStudLedger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStudLedger));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtstud = new System.Windows.Forms.TextBox();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.btnok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtoid = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.Dgvlist = new System.Windows.Forms.DataGridView();
            this.GBList.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).BeginInit();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.panel1);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txtstud);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.btnok);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtoid);
            this.GBList.Controls.Add(this.Dgv);
            this.GBList.Controls.Add(this.Dgvlist);
            this.GBList.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBList.Location = new System.Drawing.Point(-1, 1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(863, 384);
            this.GBList.TabIndex = 5;
            this.GBList.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnadd);
            this.panel1.Controls.Add(this.btnexit);
            this.panel1.Location = new System.Drawing.Point(714, 351);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(141, 30);
            this.panel1.TabIndex = 156;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(2, 0);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(74, 30);
            this.btnadd.TabIndex = 155;
            this.btnadd.Text = "Export";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(74, 0);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(64, 30);
            this.btnexit.TabIndex = 151;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(817, 33);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 26);
            this.txtssno.TabIndex = 152;
            this.txtssno.Visible = false;
            // 
            // txtsid
            // 
            this.txtsid.Location = new System.Drawing.Point(780, 13);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 26);
            this.txtsid.TabIndex = 150;
            this.txtsid.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 144;
            this.label3.Text = "Student Name";
            // 
            // txtstud
            // 
            this.txtstud.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtstud.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstud.Location = new System.Drawing.Point(326, 33);
            this.txtstud.MaxLength = 250;
            this.txtstud.Name = "txtstud";
            this.txtstud.Size = new System.Drawing.Size(247, 26);
            this.txtstud.TabIndex = 1;
            this.txtstud.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtstud_MouseClick);
            this.txtstud.TextChanged += new System.EventHandler(this.txtstud_TextChanged);
            this.txtstud.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtstud_KeyDown);
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(741, 14);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 26);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(50, 33);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 26);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.txtclass_TextChanged);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtclass_KeyDown);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.White;
            this.btnok.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Image = ((System.Drawing.Image)(resources.GetObject("btnok.Image")));
            this.btnok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnok.Location = new System.Drawing.Point(579, 31);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(54, 30);
            this.btnok.TabIndex = 126;
            this.btnok.Text = "Ok";
            this.btnok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 117;
            this.label1.Text = "Class";
            // 
            // txtoid
            // 
            this.txtoid.Location = new System.Drawing.Point(700, 15);
            this.txtoid.Name = "txtoid";
            this.txtoid.Size = new System.Drawing.Size(35, 26);
            this.txtoid.TabIndex = 109;
            this.txtoid.Visible = false;
            // 
            // Dgv
            // 
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(7, 68);
            this.Dgv.Name = "Dgv";
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.Size = new System.Drawing.Size(847, 282);
            this.Dgv.TabIndex = 154;
            this.Dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_CellContentClick);
            this.Dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Dgv_CellFormatting);
            this.Dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_CellValueChanged);
            // 
            // Dgvlist
            // 
            this.Dgvlist.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgvlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgvlist.Location = new System.Drawing.Point(7, 183);
            this.Dgvlist.Name = "Dgvlist";
            this.Dgvlist.RowHeadersVisible = false;
            this.Dgvlist.Size = new System.Drawing.Size(847, 109);
            this.Dgvlist.TabIndex = 153;
            this.Dgvlist.Visible = false;
            // 
            // FrmStudLedger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(872, 386);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStudLedger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Ledger";
            this.Load += new System.EventHandler(this.FrmStudLedger_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtsid;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtstud;
        private System.Windows.Forms.TextBox txtcid;
        internal System.Windows.Forms.TextBox txtclass;
        internal System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtoid;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.DataGridView Dgvlist;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Panel panel1;
    }
}