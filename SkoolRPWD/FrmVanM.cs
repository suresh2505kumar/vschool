﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace SkoolRPWD
{
    public partial class FrmVanM : Form
    {
        public FrmVanM()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();

        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");

        private void FrmVanM_Load(object sender, EventArgs e)
        {
            Load_grid();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;

            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            txtvan.Text = "";
            txtreg.Text = "";
            txtdriver.Text = "";

            GBList.Visible = true;
            GBMain.Visible = false;

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;

            btnsave.Text = "Save";
        }

        private void Load_grid()
        {
            try
            {
                con.Open();
                string qur = "SELECT Vuid, Vanname,Regno,Driver FROM  VanM";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 4;

                Dgv.Columns[0].Name = "Vuid";
                Dgv.Columns[0].HeaderText = "Vuid";
                Dgv.Columns[0].DataPropertyName = "Vuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Vanname";
                Dgv.Columns[1].HeaderText = "Van Name";
                Dgv.Columns[1].DataPropertyName = "Vanname";
                Dgv.Columns[1].Width = 250;

                Dgv.Columns[2].DataPropertyName = "Regno";
                Dgv.Columns[2].HeaderText = "Regno";
                Dgv.Columns[2].DataPropertyName = "Regno";
                Dgv.Columns[2].Width = 140;

                Dgv.Columns[3].DataPropertyName = "Driver";
                Dgv.Columns[3].Visible = false;


                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtvan.Text == "")
            {
                MessageBox.Show("Enter the Vanname");
                txtvan.Focus();
                return;

            }

            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from VanM where  Vanname='" + txtvan.Text + "' ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into VanM values(@Vanname,@Regno,@Driver)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Vanname", SqlDbType.NVarChar).Value = txtvan.Text;
                    cmd.Parameters.AddWithValue("@Regno", SqlDbType.NVarChar).Value = txtreg.Text;
                    cmd.Parameters.AddWithValue("@Driver", SqlDbType.NVarChar).Value = txtdriver.Text;

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                    txtvan.Text = "";
                    txtreg.Text = "";
                    txtdriver.Text = "";
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;

                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    txtvan.Text = "";
                    txtvan.Focus();
                }

            }
            else
            {
                con.Close();
                string qur = "select * from VanM where Vanname='" + txtvan.Text + "' and Vuid <> " + txtuid.Text;
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string QueryUpdate = "Update VanM set Vanname='" + txtvan.Text + "',Regno='" + txtreg.Text + "',Driver='" + txtdriver.Text + "'  where Vuid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Close();
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtvan.Text = "";
                    txtreg.Text = "";
                    txtdriver.Text = "";
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }

                btnsave.Text = "Save";

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;

            btnsave.Visible = true;
            btnaddrcan.Visible = true;

            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtvan.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtreg.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtdriver.Text = Dgv.Rows[i].Cells[3].Value.ToString();

            btnsave.Text = "Update";
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Vuid, Vanname,Regno,Driver FROM  VanM where Vanname like '%" + txtscr.Text + "%' and Regno like '%" + txtscr1.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 4;

                Dgv.Columns[0].Name = "Vuid";
                Dgv.Columns[0].HeaderText = "Vuid";
                Dgv.Columns[0].DataPropertyName = "Vuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Vanname";
                Dgv.Columns[1].HeaderText = "Van Name";
                Dgv.Columns[1].DataPropertyName = "Vanname";
                Dgv.Columns[1].Width = 250;

                Dgv.Columns[2].DataPropertyName = "Regno";
                Dgv.Columns[2].HeaderText = "Regno";
                Dgv.Columns[2].DataPropertyName = "Regno";
                Dgv.Columns[2].Width = 140;

                Dgv.Columns[3].DataPropertyName = "Driver";
                Dgv.Columns[3].Visible = false;


                Dgv.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }


    }
}
