﻿namespace SkoolRPWD
{
    partial class FrmReconClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReconClass));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.cmdprt1 = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.GBList.Controls.Add(this.cmdprt1);
            this.GBList.Controls.Add(this.btnok);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Location = new System.Drawing.Point(0, -3);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(389, 178);
            this.GBList.TabIndex = 155;
            this.GBList.TabStop = false;
            // 
            // cmdprt1
            // 
            this.cmdprt1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt1.Image = ((System.Drawing.Image)(resources.GetObject("cmdprt1.Image")));
            this.cmdprt1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdprt1.Location = new System.Drawing.Point(280, 74);
            this.cmdprt1.Name = "cmdprt1";
            this.cmdprt1.Size = new System.Drawing.Size(58, 30);
            this.cmdprt1.TabIndex = 219;
            this.cmdprt1.Text = "Print";
            this.cmdprt1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt1.UseVisualStyleBackColor = false;
            this.cmdprt1.Visible = false;
            this.cmdprt1.Click += new System.EventHandler(this.cmdprt_Click);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.White;
            this.btnok.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Image = ((System.Drawing.Image)(resources.GetObject("btnok.Image")));
            this.btnok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnok.Location = new System.Drawing.Point(121, 129);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(64, 30);
            this.btnok.TabIndex = 173;
            this.btnok.Text = "Add";
            this.btnok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // txtsid
            // 
            this.txtsid.Location = new System.Drawing.Point(348, 45);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 20);
            this.txtsid.TabIndex = 171;
            this.txtsid.Visible = false;
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(303, 19);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 168;
            this.txtssno.Visible = false;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(195, 130);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(62, 30);
            this.btnexit.TabIndex = 160;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(344, 19);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 159;
            this.txtcid.Visible = false;
            // 
            // cboTerm
            // 
            this.cboTerm.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(121, 78);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(136, 24);
            this.cboTerm.TabIndex = 1;
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboTerm_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(80, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 155;
            this.label5.Text = "Term";
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(121, 35);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 22);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtclass_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(81, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 153;
            this.label1.Text = "Class";
            // 
            // FrmReconClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 175);
            this.Controls.Add(this.GBList);
            this.Name = "FrmReconClass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmReconClass";
            this.Load += new System.EventHandler(this.FrmReconClass_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        internal System.Windows.Forms.Button btnok;
        private System.Windows.Forms.TextBox txtsid;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.ComboBox cboTerm;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cmdprt1;
    }
}