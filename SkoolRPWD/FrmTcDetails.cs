﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
namespace SkoolRPWD
{
    public partial class FrmTcDetails : Form
    {
        public FrmTcDetails()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        CommonClass db = new CommonClass();
        BindingSource bsStudet = new BindingSource();
        int TcDUid = 0;
        public void clear()
        {
            cmbTcType.SelectedIndex = -1;
            txtstudent.Text = string.Empty;
            txttamilname.Text = string.Empty;
            txtsno.Text = string.Empty;
            txtregNo.Text = string.Empty;
            cmbqph.SelectedIndex = -1;
            cmbfeesdue.SelectedIndex = -1;
            cmbconduct.SelectedIndex = -1;
           cmbScholarship.SelectedIndex =-1;
            cmbMedical.SelectedIndex = -1;
            cmbAY1.SelectedIndex = -1;
            cmbAY2.SelectedIndex = -1;
            cmbAY3.SelectedIndex = -1;
            cmbAY4.SelectedIndex = -1;
            cmbAY5.SelectedIndex = -1;
            cmbSstud1.SelectedIndex = -1;
            cmbSstud2.SelectedIndex = -1;
            cmbSstud3.SelectedIndex = -1;
            cmbSstud4.SelectedIndex = -1;
            cmbSstud5.SelectedIndex = -1;
            cmbFL1.SelectedIndex = -1;
            cmbFL2.SelectedIndex = -1;
            cmbFL3.SelectedIndex = -1;
            cmbFL4.SelectedIndex = -1;
            cmbFL5.SelectedIndex = -1;
            cmbMI1.SelectedIndex = -1;
            cmbMI2.SelectedIndex = -1;
            txtTNWD.Text = string.Empty;
            txtNDP.Text = string.Empty;
            txtpercentage.Text = string.Empty;
            txtEMIS.Text = string.Empty;
            txtAADHAR.Text = string.Empty;
            txtTMR.Text = string.Empty;
            txtPWS.Text = string.Empty;
            cmbGenedu.Text = string.Empty;
            txtVocEdu.Text = string.Empty;
          
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string Mode = string.Empty;
                if (btnsave.Text == "Save")
                {
                    Mode = "Save";
                }
                else
                {
                    Mode = "Update";
                }
                SqlParameter[] para = {
                    new SqlParameter("@TcDuId",TcDUid),
                    new SqlParameter("@StudId",txtstudent.Tag),
                    new SqlParameter("@TcType",cmbTcType.Text),
                    new SqlParameter("@StudNameTamil",txttamilname.Text),
                    new SqlParameter("@SlNo",txtsno.Text),
                    new SqlParameter("@TMRcode",txtTMR.Text),
                    new SqlParameter("@RegNo",txtregNo.Text),
                    new SqlParameter("@QPH",cmbqph.Text),
                    new SqlParameter("@FeesDue",cmbfeesdue.Text),
                    new SqlParameter("@Conduct",cmbconduct.Text),
                    new SqlParameter("@ScholorShip",cmbScholarship.Text),
                    new SqlParameter("@MedicalInspection",cmbMedical.Text),
                    new SqlParameter("@DOL",Convert.ToDateTime(dtDol.Text)),
                    new SqlParameter("@DOA",Convert.ToDateTime(dtDaTc.Text)),
                    new SqlParameter("@DOTC",Convert.ToDateTime(dtDtc.Text)),
                    new SqlParameter("@Grp",cmbgroup.Text),
                    new SqlParameter("@AcYear1",cmbAY1.Text),
                    new SqlParameter("@AcYear2",cmbAY2.Text),
                    new SqlParameter("@AcYear3",cmbAY3.Text),
                    new SqlParameter("@AcYear4",cmbAY4.Text),
                    new SqlParameter("@AcYear5",cmbAY5.Text),
                    new SqlParameter("@Sstud1",cmbSstud1.Text),
                    new SqlParameter("@Sstud2",cmbSstud2.Text),
                    new SqlParameter("@Sstud3",cmbSstud3.Text),
                    new SqlParameter("@Sstud4",cmbSstud4.Text),
                    new SqlParameter("@Sstud5",cmbSstud5.Text),
                    new SqlParameter("@FirstLang1",cmbFL1.Text),
                    new SqlParameter("@FirstLang2",cmbFL2.Text),
                    new SqlParameter("@FirstLang3",cmbFL3.Text),
                    new SqlParameter("@FirstLang4",cmbFL4.Text),
                    new SqlParameter("@FirstLang5",cmbFL5.Text),
                    new SqlParameter("@MedOfIns1",cmbMI1.Text),
                    new SqlParameter("@MedOfIns2",cmbMI2.Text),
                    new SqlParameter("@TNWD",txtTNWD.Text),
                    new SqlParameter("@NDP",txtNDP.Text),
                    new SqlParameter("@Percentage",txtpercentage.Text),
                    new SqlParameter("@EMISno",txtEMIS.Text),
                    new SqlParameter("@AadharNo",txtAADHAR.Text),
                    new SqlParameter("@tag",Mode),
                    new SqlParameter("@LouP_1",LP_1.Text),
                    new SqlParameter("@PWS",txtPWS.Text),
                    new SqlParameter("@GenEdu",cmbGenedu.Text),
                    new SqlParameter("@GroupCode",cmbgroup.Text),
                    new SqlParameter("@VocationalEdu",txtVocEdu.Text)
              
                };
                DialogResult res = ValidateText();
                if(res == DialogResult.Yes)
                {
                    db.ExecuteNonQuery("SP_TcDetails", para,con);
                    MessageBox.Show("Record Save Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clear();
                    btnsave.Text = "Save";
                }
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DialogResult ValidateText()
        {
            DialogResult res = DialogResult.Yes;
            if (txtAADHAR.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Aadhar Number ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if(txtEMIS.Text==string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out EMIS Number ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbGenedu.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out General Education ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtPWS.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Pupil Was Studying ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (LP_1.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Language offered under Part-1 ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtVocEdu.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Vocation Education ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtsno.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Serial No ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txttamilname.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Student Name In Tamil ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtTMR.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out T.M.R  Code No ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtregNo.Text == string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Reg No ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbconduct.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Conduct & Character ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbScholarship.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Scholarships ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbMedical.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Medical Inspection ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbgroup.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Group ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (cmbMI1.SelectedIndex == -1)
            {
                res = MessageBox.Show("Do You Want save With out Medium of Instruction ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            if (txtTNWD.Text==string.Empty)
            {
                res = MessageBox.Show("Do You Want save With out Total Number of Working Days ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            return res;
        }


        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmStud2_Load(object sender, EventArgs e)
        {
            var SclName = new List<string>(ConfigurationManager.AppSettings["SclName"].Split(new char[] { ';' }));
            var EducationalDist = new List<string>(ConfigurationManager.AppSettings["EducationalDist"].Split(new char[] { ';' }));
            var REvenDist = new List<string>(ConfigurationManager.AppSettings["REvenDist"].Split(new char[] { ';' }));

            txtsclname.Text = SclName.ToString();
            txteddis.Text = EducationalDist.ToString();
            txteddis.Text = REvenDist.ToString();
            cmbTcType.SelectedIndex = -1;
            grFront.Visible = true;
            panadd.Visible = true;
            btnadd.Visible = true;
            grBack.Visible = false;
            grFront.Visible = true;
            loadgrid();
            btnsave.Text = "Save";
        }
        private void loadgrid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.uid,admisno as  AdmissionNo,sname as StudentName,Tctype,SLno as SerialNo,b.AadharNo,b.TcDuId  from StudentM  a inner join TcStudDetails b on  a.uid=b.StudId  order by TcDuid desc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "AdmissionNo";
                Dgv.Columns[1].HeaderText = "AdmissionNo";
                Dgv.Columns[1].DataPropertyName = "AdmissionNo";
                Dgv.Columns[1].Width = 140;

                Dgv.Columns[2].Name = "StudentName";
                Dgv.Columns[2].HeaderText = "StudentName";
                Dgv.Columns[2].DataPropertyName = "StudentName";
                Dgv.Columns[2].Width = 250;

                Dgv.Columns[3].Name = "Tctype";
                Dgv.Columns[3].HeaderText = "Tctype";
                Dgv.Columns[3].DataPropertyName = "Tctype";
                Dgv.Columns[3].Width = 158;

                Dgv.Columns[4].Name = "SerialNo";
                Dgv.Columns[4].HeaderText = "SerialNo";
                Dgv.Columns[4].DataPropertyName = "SerialNo";
                Dgv.Columns[4].Width = 172;

                Dgv.Columns[5].Name = "AadharNo";
                Dgv.Columns[5].HeaderText = "Aadhar";
                Dgv.Columns[5].DataPropertyName = "AadharNo";
                Dgv.Columns[5].Width = 200;
                Dgv.Columns[6].Name = "TcDuId";
                Dgv.Columns[6].HeaderText = "TcDuId";
                Dgv.Columns[6].DataPropertyName = "TcDuId";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }
        private void cmbTcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbTcType.SelectedIndex != -1)
                {
                    string Type = string.Empty;
                    if (cmbTcType.Text == "Higher Secondary")
                    {
                        Type = "Senior";
                    }
                    else
                    {
                        Type = cmbTcType.Text;
                    }

                    SqlParameter[] para = { new SqlParameter("@ClassType", Type) };
                    DataTable dt = db.GetDatabyParameter("SP_GetStudentForTC", para,con);
                    bsStudet.DataSource = dt;
                    DataGridStudent.DataSource = null;
                    DataGridStudent.AutoGenerateColumns = false;
                    DataGridStudent.ColumnCount = 4;
                    DataGridStudent.Columns[0].Name = "StudId";
                    DataGridStudent.Columns[0].HeaderText = "StudId";
                    DataGridStudent.Columns[0].DataPropertyName = "StudId";
                    DataGridStudent.Columns[0].Visible = false;

                    DataGridStudent.Columns[1].Name = "AdmisNo";
                    DataGridStudent.Columns[1].HeaderText = "AdmisNo";
                    DataGridStudent.Columns[1].DataPropertyName = "AdmisNo";

                    DataGridStudent.Columns[2].Name = "SName";
                    DataGridStudent.Columns[2].HeaderText = "SName";
                    DataGridStudent.Columns[2].DataPropertyName = "SName";
                    DataGridStudent.Columns[2].Width = 150;
                    DataGridStudent.Columns[3].Name = "Class_Desc";
                    DataGridStudent.Columns[3].HeaderText = "Class_Desc";
                    DataGridStudent.Columns[3].DataPropertyName = "Class_Desc";
                    DataGridStudent.DataSource = bsStudet;
                    Point p = FindLocation(txtstudent);
                    grSearch.Location = new Point(p.X, p.Y + 10);
                    grSearch.Visible = true;
                    txtStudNameSearch.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
            txtStudNameSearch.Text = string.Empty;
        }

        private void txtStudNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
                txtStudNameSearch.Text = string.Empty;
            }

        }

        private void DataGridStudent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
                txtStudNameSearch.Text = string.Empty;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
            {
                return ctrl.Location;
            }
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtstudent_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStudent_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridStudent.SelectedCells[0].RowIndex;
                txtstudent.Text = DataGridStudent.Rows[Index].Cells[2].Value.ToString();
                txtstudent.Tag = DataGridStudent.Rows[Index].Cells[0].Value.ToString();
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            grFront.Visible = false;
            panadd.Visible = false;
            btnaddrcan.Visible = true;
            int i = Dgv.SelectedCells[0].RowIndex;
            cmbTcType.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            grBack.Visible = true;
            Loaddet();
            btnsave.Text = "Update";
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void Loaddet()
        {
            con.Close();
            con.Open();
            string qur = "select *  from StudentM  a inner join   TcStudDetails b on  a.uid=b.StudId  where a.uid=" + Dgv.CurrentRow.Cells[0].Value.ToString() + "";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            txttamilname.Text = tap.Rows[0]["StudNameTamil"].ToString();
            txtsno.Text = tap.Rows[0]["slno"].ToString();
            txtTMR.Text = tap.Rows[0]["tmrcode"].ToString();
            txtregNo.Text = tap.Rows[0]["regno"].ToString();
            cmbqph.Text = tap.Rows[0]["QPH"].ToString();
            cmbfeesdue.Text = tap.Rows[0]["FeesDue"].ToString();
            cmbScholarship.Text = tap.Rows[0]["ScholorShip"].ToString();
            cmbconduct.Text = tap.Rows[0]["Conduct"].ToString();
            cmbMedical.Text = tap.Rows[0]["MedicalInspection"].ToString();
            dtDol.Text = tap.Rows[0]["DOL"].ToString();
            dtDaTc.Text = tap.Rows[0]["DOA"].ToString();
            dtDtc.Text = tap.Rows[0]["DOTC"].ToString();
            cmbgroup.Text = tap.Rows[0]["Grp"].ToString();
            cmbAY1.Text = tap.Rows[0]["AcYear1"].ToString();
            txtstudent.Text = tap.Rows[0]["SName"].ToString();
            cmbAY2.Text = tap.Rows[0]["AcYear2"].ToString();
            cmbAY3.Text = tap.Rows[0]["AcYear3"].ToString();
            cmbAY4.Text = tap.Rows[0]["AcYear4"].ToString();
            cmbAY5.Text = tap.Rows[0]["AcYear5"].ToString();
            cmbSstud1.Text = tap.Rows[0]["Sstud1"].ToString();
            cmbSstud2.Text = tap.Rows[0]["Sstud2"].ToString();
            cmbSstud3.Text = tap.Rows[0]["Sstud3"].ToString();
            cmbSstud4.Text = tap.Rows[0]["Sstud4"].ToString();
            cmbSstud5.Text = tap.Rows[0]["Sstud5"].ToString();
            cmbFL1.Text = tap.Rows[0]["FirstLang1"].ToString();
            cmbFL2.Text = tap.Rows[0]["FirstLang2"].ToString();
            cmbFL3.Text = tap.Rows[0]["FirstLang3"].ToString();
            cmbFL4.Text = tap.Rows[0]["FirstLang4"].ToString();
            cmbFL5.Text = tap.Rows[0]["FirstLang5"].ToString();
            cmbMI1.Text = tap.Rows[0]["MedOfIns1"].ToString();
            cmbMI2.Text = tap.Rows[0]["MedOfIns2"].ToString();
            txtTNWD.Text = tap.Rows[0]["TNWD"].ToString();
            txtNDP.Text = tap.Rows[0]["NDP"].ToString();
            txtpercentage.Text = tap.Rows[0]["Percentage"].ToString();
            txtEMIS.Text = tap.Rows[0]["EMISno"].ToString();
            txtAADHAR.Text = tap.Rows[0]["AadharNo"].ToString();
            txtstudent.Tag = tap.Rows[0]["StudId"].ToString();
            txtstudent.Tag = tap.Rows[0]["StudId"].ToString();
            txtPWS.Text = tap.Rows[0]["PWS"].ToString();
            cmbGenedu.Text = tap.Rows[0]["GenEdu"].ToString();
            cmbgroup.Text = tap.Rows[0]["GroupCode"].ToString();
            LP_1.Text = tap.Rows[0]["LouP_1"].ToString();
            txtVocEdu.Text = tap.Rows[0]["VocationalEdu"].ToString();
            TcDUid = Convert.ToInt32(tap.Rows[0]["TcDuid"].ToString());
            grSearch.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int i = Dgv.SelectedCells[0].RowIndex;
            Module.Dtype = 35;
            Module.strsql = Dgv.Rows[i].Cells[3].Value.ToString();
            Module.ColluidPT = Convert.ToInt32(Dgv.Rows[i].Cells[0].Value.ToString());
            CRViewer crv = new CRViewer();
            crv.MdiParent = this.MdiParent;
            crv.Show();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            grFront.Visible = false;
            grBack.Visible = true;
            panadd.Visible = false;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            panadd.Visible = true;
            loadgrid();
            btnadd.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Module.Dtype = 25;

            Module.ColluidPT = Convert.ToInt32(Dgv.CurrentRow.Cells[0].Value.ToString());
            CRViewer crv = new CRViewer();
            crv.MdiParent = this.MdiParent;
            crv.Show();
            FrmCRViewer2 crv1 = new FrmCRViewer2();
            crv1.Show();

        }

        private void btnSecondPage_Click(object sender, EventArgs e)
        {
            int i = Dgv.SelectedCells[0].RowIndex;
            Module.Dtype = 36;
            Module.strsql = Dgv.Rows[i].Cells[3].Value.ToString();
            Module.ColluidPT = Convert.ToInt32(Dgv.Rows[i].Cells[0].Value.ToString());
            CRViewer crv = new CRViewer();
            crv.MdiParent = this.MdiParent;
            crv.Show();
        }

        private void txtStudNameSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsStudet.Filter = string.Format("AdmisNo LIKE '%{0}%' or SName LIKE '%{1}%' ", txtStudNameSearch.Text, txtStudNameSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you want to Delete the TcDetails ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if(result == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int TcDUid = Convert.ToInt32(Dgv.Rows[Index].Cells[6].Value.ToString());
                    SqlParameter[] para = { new SqlParameter("@TcDUid", TcDUid) };
                    db.ExecuteNonQuery("SP_DeletTcStudDetails", para, con);
                    MessageBox.Show("Tc Deails Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loadgrid();
                }
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void grBack_Enter(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void cmbScholarship_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
