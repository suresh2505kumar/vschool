﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace SkoolRPWD
{
    public partial class FrmClassTransfer : Form
    {
        public FrmClassTransfer()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        DataTable dt2 = new DataTable();
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void LoadClassDesc()
        {
            conn.Close();
            conn.Open();
            string qur = "SELECT Cid,Class_Desc FROM Class_Mast order by Class_Desc";
            cmd = new SqlCommand(qur, conn);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cbofrom .DataSource = null;
            cbofrom.DataSource = tap;
            cbofrom.DisplayMember = "Class_Desc";
            cbofrom.ValueMember = "Cid";
            cbofrom.SelectedIndex = -1;
            conn.Close();

        }

        private void LoadClassDescTo()
        {
            conn.Close();
            conn.Open();
            string qur = "SELECT Cid,Class_Desc FROM Class_Mast order by Class_Desc";
            cmd = new SqlCommand(qur, conn);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboto.DataSource = null;
            cboto.DataSource = tap;
            cboto.DisplayMember = "Class_Desc";
            cboto.ValueMember = "Cid";
            cboto.SelectedIndex = -1;
            DataRow dr = tap.NewRow();
            dr["Class_Desc"] = "TC ISSUE";
            dr["Cid"] = 0;
            tap.Rows.InsertAt(dr, 0);
            conn.Close();

        }

        private void FrmClassTransfer_Load(object sender, EventArgs e)
        {
            this.LoadClassDesc();
            this.DgvF.AllowUserToAddRows = false;
            Chk();
            this.LoadClassDescTo();
            cboto.SelectedIndex = -1;
            this.DgvT.AllowUserToAddRows = false;
            DgvT.AutoResizeColumns();
            DgvT.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;     
            dt2.Columns.Add("StudentName");
            dt2.Columns.Add("uid");
            DgvT.DataSource = dt2;
            DgvT.Columns["uid"].Visible = false;
        }

        private void Chk()
        {
            DataGridViewCheckBoxColumn checkboxColumn = new DataGridViewCheckBoxColumn();
            checkboxColumn.Width = 30;
            checkboxColumn.Name = "chk";
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DgvF.Columns.Insert(0, checkboxColumn);            
            Rectangle rect = DgvF.GetCellDisplayRectangle(0, -1, true);
            rect.X = rect.Location.X + (rect.Width / 4);
            CheckBox checkboxHeader = new CheckBox();
            checkboxHeader.Name = "checkboxHeader";
            checkboxHeader.Size = new Size(18, 18);
            checkboxHeader.Location = rect.Location;
            checkboxHeader.CheckedChanged += new EventHandler(checkboxHeader_CheckedChanged);
            DgvF.Controls.Add(checkboxHeader);            
        }

        private void checkboxHeader_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < DgvF.RowCount; i++)
            {
                DgvF[0, i].Value = ((CheckBox)DgvF.Controls.Find("checkboxHeader", true)[0]).Checked;
            }
            DgvF.EndEdit();
        }

        private void cbofrom_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cboto_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbofrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            string res = "select SName as StudentName,uid from studentm a inner join Class_Mast b on a.Classid =b.cid where a.Tag ='A' and class_desc=@class_desc";
            cmd = new SqlCommand(res, conn);
            adpt = new SqlDataAdapter(cmd);            
            cmd.Parameters.AddWithValue("@class_desc", this.cbofrom.Text);
            DataTable dt = new DataTable();            
            adpt.Fill(dt);
            DgvF.AutoResizeColumns();
            DgvF.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;            
            this.DgvF.DataSource = dt;
            DgvF.Columns["uid"].Visible = false;
        }

        private void cbofrom_Click(object sender, EventArgs e)
        {
           
        }

        private void cboto_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void DgvF_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {            
        }

        private void btnF_Click(object sender, EventArgs e)
        {
            DgvT.Refresh();
            this.dt2.Clear();
            foreach (DataGridViewRow dr in DgvF.Rows)
            {
                if (Convert.ToBoolean(dr.Cells["chk"].Value) == true)
                {
                    DataRow gv2dr = dt2.NewRow();
                    gv2dr["uid"] = dr.Cells["uid"].Value.ToString();
                    gv2dr["StudentName"] = dr.Cells["StudentName"].Value.ToString();
                    dt2.Rows.Add(gv2dr);                   
                    dr.Cells["chk"].Value =0;
                }
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DgvT.Rows.Count ; i++)
            {
                SqlParameter[] para = {
                    new SqlParameter("@StudUid",DgvT.Rows[i].Cells[1].Value.ToString()),
                    new SqlParameter("@ClassId",cboto.SelectedValue)
                };
                CommonClass db = new CommonClass();
                db.ExecuteNonQuery("SP_UpdateClassTransfer", para, conn);
                DgvF.Refresh();
                this.cbofrom_SelectedIndexChanged(sender, e);
            }
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            DgvT.Refresh();
            dt2.Clear();
        }

        private void btnT_Click(object sender, EventArgs e)
        {
        }

        private void DgvF_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
    }
}
