﻿namespace SkoolRPWD
{
    partial class FrmStudBus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStudBus));
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.GBList = new System.Windows.Forms.GroupBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtrent = new System.Windows.Forms.TextBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.DgvT = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.txtbid = new System.Windows.Forms.TextBox();
            this.txtvid = new System.Windows.Forms.TextBox();
            this.txtvan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtboar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbofees = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.panadd.SuspendLayout();
            this.GBList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvT)).BeginInit();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBMain.Controls.Add(this.txtscr1);
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Location = new System.Drawing.Point(0, 1);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(749, 355);
            this.GBMain.TabIndex = 3;
            this.GBMain.TabStop = false;
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(307, 12);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(345, 26);
            this.txtscr1.TabIndex = 90;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(658, 9);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(82, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(6, 12);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(300, 26);
            this.txtscr.TabIndex = 87;
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(6, 41);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(734, 310);
            this.Dgv.TabIndex = 0;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Location = new System.Drawing.Point(0, 357);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(749, 32);
            this.panadd.TabIndex = 116;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(680, 1);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(562, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 84;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(621, 1);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 85;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(604, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(74, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(676, 2);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(65, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label8);
            this.GBList.Controls.Add(this.txtrent);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.DgvT);
            this.GBList.Controls.Add(this.label7);
            this.GBList.Controls.Add(this.txtamt);
            this.GBList.Controls.Add(this.txtbid);
            this.GBList.Controls.Add(this.txtvid);
            this.GBList.Controls.Add(this.txtvan);
            this.GBList.Controls.Add(this.label4);
            this.GBList.Controls.Add(this.txtboar);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.label6);
            this.GBList.Controls.Add(this.cbofees);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txtsname);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.txtuid);
            this.GBList.Location = new System.Drawing.Point(-1, 0);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(752, 355);
            this.GBList.TabIndex = 117;
            this.GBList.TabStop = false;
            // 
            // cboTerm
            // 
            this.cboTerm.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(398, 190);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(136, 24);
            this.cboTerm.TabIndex = 7;
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboTerm_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(85, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 16);
            this.label8.TabIndex = 169;
            this.label8.Text = "Rent";
            // 
            // txtrent
            // 
            this.txtrent.BackColor = System.Drawing.SystemColors.Window;
            this.txtrent.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrent.Location = new System.Drawing.Point(126, 148);
            this.txtrent.MaxLength = 250;
            this.txtrent.Name = "txtrent";
            this.txtrent.Size = new System.Drawing.Size(120, 22);
            this.txtrent.TabIndex = 4;
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(665, 33);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 167;
            this.txtssno.Visible = false;
            // 
            // DgvT
            // 
            this.DgvT.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvT.Location = new System.Drawing.Point(561, 191);
            this.DgvT.Name = "DgvT";
            this.DgvT.ReadOnly = true;
            this.DgvT.RowHeadersVisible = false;
            this.DgvT.Size = new System.Drawing.Size(170, 131);
            this.DgvT.TabIndex = 166;
            this.DgvT.Visible = false;
            this.DgvT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvT_CellContentClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(49, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 16);
            this.label7.TabIndex = 165;
            this.label7.Text = "Concession";
            // 
            // txtamt
            // 
            this.txtamt.BackColor = System.Drawing.SystemColors.Window;
            this.txtamt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(126, 191);
            this.txtamt.MaxLength = 250;
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(120, 22);
            this.txtamt.TabIndex = 6;
            // 
            // txtbid
            // 
            this.txtbid.Location = new System.Drawing.Point(665, 89);
            this.txtbid.Name = "txtbid";
            this.txtbid.Size = new System.Drawing.Size(35, 20);
            this.txtbid.TabIndex = 163;
            this.txtbid.Visible = false;
            // 
            // txtvid
            // 
            this.txtvid.Location = new System.Drawing.Point(665, 59);
            this.txtvid.Name = "txtvid";
            this.txtvid.Size = new System.Drawing.Size(35, 20);
            this.txtvid.TabIndex = 162;
            this.txtvid.Visible = false;
            // 
            // txtvan
            // 
            this.txtvan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtvan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvan.Location = new System.Drawing.Point(126, 109);
            this.txtvan.MaxLength = 250;
            this.txtvan.Name = "txtvan";
            this.txtvan.ReadOnly = true;
            this.txtvan.Size = new System.Drawing.Size(170, 22);
            this.txtvan.TabIndex = 2;
            this.txtvan.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtvan_MouseClick);
            this.txtvan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtvan_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(91, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 16);
            this.label4.TabIndex = 160;
            this.label4.Text = "Van";
            // 
            // txtboar
            // 
            this.txtboar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtboar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtboar.Location = new System.Drawing.Point(398, 106);
            this.txtboar.MaxLength = 250;
            this.txtboar.Name = "txtboar";
            this.txtboar.ReadOnly = true;
            this.txtboar.Size = new System.Drawing.Size(170, 22);
            this.txtboar.TabIndex = 3;
            this.txtboar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtboar_MouseClick);
            this.txtboar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtboar_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(310, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 158;
            this.label1.Text = "Boading Point";
            // 
            // txtsid
            // 
            this.txtsid.Location = new System.Drawing.Point(706, 85);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 20);
            this.txtsid.TabIndex = 157;
            this.txtsid.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(359, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 156;
            this.label6.Text = "Fee";
            // 
            // cbofees
            // 
            this.cbofees.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofees.FormattingEnabled = true;
            this.cbofees.Location = new System.Drawing.Point(398, 146);
            this.cbofees.Name = "cbofees";
            this.cbofees.Size = new System.Drawing.Size(247, 24);
            this.cbofees.TabIndex = 5;
            this.cbofees.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbofees_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(359, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 153;
            this.label5.Text = "Term";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(308, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 16);
            this.label3.TabIndex = 152;
            this.label3.Text = "Student Name";
            // 
            // txtsname
            // 
            this.txtsname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtsname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsname.Location = new System.Drawing.Point(398, 65);
            this.txtsname.MaxLength = 250;
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(247, 22);
            this.txtsname.TabIndex = 1;
            this.txtsname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtsname_MouseClick);
            this.txtsname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsname_KeyDown);
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(126, 65);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 22);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtclass_MouseClick);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtclass_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(82, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 149;
            this.label2.Text = "Class";
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(706, 33);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            this.txtcid.TextChanged += new System.EventHandler(this.txtcid_TextChanged);
            // 
            // txtuid
            // 
            this.txtuid.Location = new System.Drawing.Point(706, 59);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 20);
            this.txtuid.TabIndex = 109;
            this.txtuid.Visible = false;
            // 
            // FrmStudBus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(752, 389);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GBMain);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStudBus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student Bus";
            this.Load += new System.EventHandler(this.FrmStudBus_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.panadd.ResumeLayout(false);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbofees;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtsname;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtsid;
        internal System.Windows.Forms.TextBox txtboar;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox txtvan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbid;
        private System.Windows.Forms.TextBox txtvid;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.DataGridView DgvT;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txtrent;
        private System.Windows.Forms.ComboBox cboTerm;
    }
}