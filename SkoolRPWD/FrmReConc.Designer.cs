﻿namespace SkoolRPWD
{
    partial class FrmReConc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReConc));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboclass = new System.Windows.Forms.ComboBox();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.cmdprt = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.cboclass);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txtsname);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.cmdprt);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Location = new System.Drawing.Point(-1, -2);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(389, 215);
            this.GBList.TabIndex = 154;
            this.GBList.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 18);
            this.label2.TabIndex = 172;
            this.label2.Text = "Type";
            // 
            // cboclass
            // 
            this.cboclass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboclass.FormattingEnabled = true;
            this.cboclass.Items.AddRange(new object[] {
            "Classwise",
            "Studentwise"});
            this.cboclass.Location = new System.Drawing.Point(121, 33);
            this.cboclass.Name = "cboclass";
            this.cboclass.Size = new System.Drawing.Size(167, 26);
            this.cboclass.TabIndex = 0;
            this.cboclass.SelectedIndexChanged += new System.EventHandler(this.cboclass_SelectedIndexChanged);
            // 
            // txtsid
            // 
            this.txtsid.Location = new System.Drawing.Point(348, 56);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 20);
            this.txtsid.TabIndex = 171;
            this.txtsid.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 170;
            this.label3.Text = "Student Name";
            this.label3.Visible = false;
            // 
            // txtsname
            // 
            this.txtsname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtsname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsname.Location = new System.Drawing.Point(121, 133);
            this.txtsname.MaxLength = 250;
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(247, 26);
            this.txtsname.TabIndex = 2;
            this.txtsname.Visible = false;
            this.txtsname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtsname_MouseClick);
            this.txtsname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsname_KeyDown);
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(303, 30);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 168;
            this.txtssno.Visible = false;
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.Color.White;
            this.cmdprt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdprt.Location = new System.Drawing.Point(118, 171);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(102, 30);
            this.cmdprt.TabIndex = 161;
            this.cmdprt.Text = "Reconciliation";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.cmdprt_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(226, 171);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(62, 30);
            this.btnexit.TabIndex = 160;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(344, 30);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 159;
            this.txtcid.Visible = false;
            // 
            // cboTerm
            // 
            this.cboTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(121, 67);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(167, 26);
            this.cboTerm.TabIndex = 3;
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cboTerm_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(71, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 18);
            this.label5.TabIndex = 155;
            this.label5.Text = "Term";
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(121, 100);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 26);
            this.txtclass.TabIndex = 1;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.txtclass_TextChanged);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtclass_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(71, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 153;
            this.label1.Text = "Class";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FrmReConc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(388, 213);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmReConc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reconciliation";
            this.Load += new System.EventHandler(this.FrmReConc_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.TextBox txtsid;
        private System.Windows.Forms.ComboBox cboclass;
        private System.Windows.Forms.Label label2;
    }
}