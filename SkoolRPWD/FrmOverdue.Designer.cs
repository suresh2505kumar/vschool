﻿namespace SkoolRPWD
{
    partial class FrmOverdue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOverdue));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.cbbo = new System.Windows.Forms.ComboBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.cmdprt = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.cbbo);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.cmdprt);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Location = new System.Drawing.Point(-1, -1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(389, 196);
            this.GBList.TabIndex = 153;
            this.GBList.TabStop = false;
            // 
            // cbbo
            // 
            this.cbbo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbo.FormattingEnabled = true;
            this.cbbo.Items.AddRange(new object[] {
            "ClassWise",
            "Termwise"});
            this.cbbo.Location = new System.Drawing.Point(93, 31);
            this.cbbo.Name = "cbbo";
            this.cbbo.Size = new System.Drawing.Size(227, 26);
            this.cbbo.TabIndex = 0;
            this.cbbo.SelectedIndexChanged += new System.EventHandler(this.cbbo_SelectedIndexChanged);
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(346, 164);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 168;
            this.txtssno.Visible = false;
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.Image = ((System.Drawing.Image)(resources.GetObject("cmdprt.Image")));
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdprt.Location = new System.Drawing.Point(129, 138);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(62, 30);
            this.cmdprt.TabIndex = 161;
            this.cmdprt.Text = "Print";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.cmdprt_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(191, 138);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(62, 30);
            this.btnexit.TabIndex = 160;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(346, 138);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 159;
            this.txtcid.Visible = false;
            // 
            // cboTerm
            // 
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(56, 79);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(106, 26);
            this.cboTerm.TabIndex = 1;
            this.cboTerm.SelectedIndexChanged += new System.EventHandler(this.cboTerm_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 18);
            this.label5.TabIndex = 155;
            this.label5.Text = "Term";
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(208, 79);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 26);
            this.txtclass.TabIndex = 2;
            this.txtclass.Visible = false;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.txtclass_TextChanged);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtclass_KeyDown_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(168, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 153;
            this.label1.Text = "Class";
            this.label1.Visible = false;
            // 
            // FrmOverdue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(389, 194);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOverdue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Overdue";
            this.Load += new System.EventHandler(this.FrmOverdue_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.ComboBox cbbo;

    }
}