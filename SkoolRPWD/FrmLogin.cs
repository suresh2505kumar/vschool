﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.NetworkInformation;

namespace SkoolRPWD
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        bool connI = NetworkInterface.GetIsNetworkAvailable();
        public static int UserId;
        private void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtuname.Text != "" && txtpasswd.Text != "")
                {
                    FunC.CallIntChk();
                    if (connI == false)
                    {
                        return;
                    }
                    Module.ServerName = ConfigurationManager.AppSettings["ServerName"];
                    Module.DbName = cmbYear.SelectedValue.ToString();
                    Module.UserName = ConfigurationManager.AppSettings["UserName"];
                    Module.Password = ConfigurationManager.AppSettings["SqlPassword"];
                    SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
                    string query = "select * from User_Login where Username='" + txtuname.Text + "' and Password='" + txtpasswd.Text + "'";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.CommandTimeout = 120;
                    SqlDataAdapter dt = new SqlDataAdapter(cmd);                    
                    DataTable tab = new DataTable();
                    dt.Fill(tab);
                    if (tab.Rows.Count > 0)
                    {
                        this.Hide();
                        conn.Close();
                        UserId =Convert.ToInt32(tab.Rows[0]["Uid"].ToString());
                        FrmMain main = new FrmMain();
                        main.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid name and password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Enter user name and password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtuname.Focus();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Server not connected try after few minutes", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void CallIntChk()
        {
            if (connI == true)
            {
                Module.Intchk = "Internet is Connected";
                return;
            }
            else
            {
                Module.Intchk = "Internet is DisConnected";
                MessageBox.Show("Internet Connection is Not Available");
                return;
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            GetYear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        protected void GetYear()
        {
            try
            {
                CommonClass db = new CommonClass();
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetYear", conn);
                cmbYear.DataSource = null;
                cmbYear.DisplayMember = "AcYear";
                cmbYear.ValueMember = "DbName";
                cmbYear.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
