﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmStudentImprt : Form
    {
        public FrmStudentImprt()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void FrmStudentImprt_Load(object sender, EventArgs e)
        {
            LodCombo();
        }
        protected void LodCombo()
        {
            try
            {
                string Query = "Select Class_Desc,Cid from Class_Mast order by Class_desc";
                SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.NewRow();
                dr["Class_Desc"] = "All";
                dr["Cid"] = 0;
                dt.Rows.InsertAt(dr, 0);
                cmbClass.DataSource = null;
                cmbClass.DisplayMember = "Class_Desc";
                cmbClass.ValueMember = "Cid";
                cmbClass.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {           
            ImportStudentClassWise(connstring, lblFileName.Text);
            //MessageBox.Show("Student Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        protected void ImportStudentClassWise(string ConnectionString,string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();
                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    new DataColumn("AdmissionNumber", typeof(string));
                    new DataColumn("StudentName", typeof(string));
                    new DataColumn("Class", typeof(string));
                    new DataColumn("Sex", typeof(string));
                    new DataColumn("DateofBirth", typeof(DateTime));
                    new DataColumn("FatherName", typeof(string));
                    new DataColumn("MotherName", typeof(string));
                    new DataColumn("Address", typeof(string));
                    new DataColumn("DateofAdmission", typeof(DateTime));
                    new DataColumn("ClassJoin", typeof(string));
                    new DataColumn("FatherMobile", typeof(string));
                    new DataColumn("MotherMobile", typeof(int));
                    new DataColumn("SMSMobile", typeof(string));                    

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        string query = "Delete from ImportStudentM where ClassId='" + cmbClass.SelectedValue + "'";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();

                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {

                            sqlbulkcopy.DestinationTableName = "dbo.ImportStudentM";                            
                            sqlbulkcopy.ColumnMappings.Add("AdmissionNumber", "Admisno");
                            sqlbulkcopy.ColumnMappings.Add("StudentName", "SName");
                            sqlbulkcopy.ColumnMappings.Add("Class", "Classid");
                            sqlbulkcopy.ColumnMappings.Add("Sex", "Sex");
                            sqlbulkcopy.ColumnMappings.Add("DateofBirth", "DOB");
                            sqlbulkcopy.ColumnMappings.Add("FatherName", "FName");
                            sqlbulkcopy.ColumnMappings.Add("MotherName", "MName");
                            sqlbulkcopy.ColumnMappings.Add("Address", "Address");
                            sqlbulkcopy.ColumnMappings.Add("DateofAdmission", "DOA");
                            sqlbulkcopy.ColumnMappings.Add("ClassJoin", "Class_join");
                            sqlbulkcopy.ColumnMappings.Add("FatherMobile", "Fphone");
                            sqlbulkcopy.ColumnMappings.Add("MotherMobile", "Mphone");
                            sqlbulkcopy.ColumnMappings.Add("SMSMobile", "SMS");                            
                            conn.Open();                            
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();                            
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
            dialog.ShowDialog();
            lblFileName.Text = dialog.FileName;
            lblFileName.Visible = true;
            string extensionpath = Path.GetExtension(dialog.FileName);
            switch (extensionpath)
            {
                case ".xls":
                    connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx":
                    connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    return;
            }
        }
    }
}
