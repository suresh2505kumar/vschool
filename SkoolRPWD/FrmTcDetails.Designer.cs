﻿namespace SkoolRPWD
{
    partial class FrmTcDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTcDetails));
            this.panadd = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSecondPage = new System.Windows.Forms.Button();
            this.btnFirstPage = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.cmbFL5 = new System.Windows.Forms.ComboBox();
            this.cmbFL4 = new System.Windows.Forms.ComboBox();
            this.cmbFL3 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsno = new System.Windows.Forms.TextBox();
            this.txtAADHAR = new System.Windows.Forms.TextBox();
            this.txtregNo = new System.Windows.Forms.TextBox();
            this.txtTMR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txttamilname = new System.Windows.Forms.TextBox();
            this.cmbqph = new System.Windows.Forms.ComboBox();
            this.cmbfeesdue = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbconduct = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dtDol = new System.Windows.Forms.DateTimePicker();
            this.dtDaTc = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbgroup = new System.Windows.Forms.ComboBox();
            this.dtDtc = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbAY1 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cmbMI1 = new System.Windows.Forms.ComboBox();
            this.cmbMI2 = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_totalNo_WD = new System.Windows.Forms.TextBox();
            this.txtTNWD = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtEMIS = new System.Windows.Forms.TextBox();
            this.txtNDP = new System.Windows.Forms.TextBox();
            this.txtpercentage = new System.Windows.Forms.TextBox();
            this.cmbAY2 = new System.Windows.Forms.ComboBox();
            this.cmbAY3 = new System.Windows.Forms.ComboBox();
            this.cmbAY4 = new System.Windows.Forms.ComboBox();
            this.cmbAY5 = new System.Windows.Forms.ComboBox();
            this.cmbSstud1 = new System.Windows.Forms.ComboBox();
            this.cmbSstud2 = new System.Windows.Forms.ComboBox();
            this.cmbSstud3 = new System.Windows.Forms.ComboBox();
            this.cmbSstud4 = new System.Windows.Forms.ComboBox();
            this.cmbSstud5 = new System.Windows.Forms.ComboBox();
            this.cmbFL1 = new System.Windows.Forms.ComboBox();
            this.cmbFL2 = new System.Windows.Forms.ComboBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.txtStudNameSearch = new System.Windows.Forms.TextBox();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridStudent = new System.Windows.Forms.DataGridView();
            this.txtsclname = new System.Windows.Forms.TextBox();
            this.txteddis = new System.Windows.Forms.TextBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.txtstudent = new System.Windows.Forms.TextBox();
            this.cmbTcType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LP_1 = new System.Windows.Forms.ComboBox();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.txtVocEdu = new System.Windows.Forms.TextBox();
            this.cmbGenedu = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPWS = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbMedical = new System.Windows.Forms.ComboBox();
            this.cmbScholarship = new System.Windows.Forms.ComboBox();
            this.panadd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).BeginInit();
            this.grBack.SuspendLayout();
            this.SuspendLayout();
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Controls.Add(this.btnSecondPage);
            this.panadd.Controls.Add(this.btnFirstPage);
            this.panadd.Controls.Add(this.btnAll);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panadd.Location = new System.Drawing.Point(7, 587);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(959, 39);
            this.panadd.TabIndex = 212;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(883, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(67, 30);
            this.btnDelete.TabIndex = 87;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSecondPage
            // 
            this.btnSecondPage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSecondPage.Location = new System.Drawing.Point(239, 7);
            this.btnSecondPage.Name = "btnSecondPage";
            this.btnSecondPage.Size = new System.Drawing.Size(158, 25);
            this.btnSecondPage.TabIndex = 86;
            this.btnSecondPage.Text = "Preview SecondPage";
            this.btnSecondPage.UseVisualStyleBackColor = true;
            this.btnSecondPage.Click += new System.EventHandler(this.btnSecondPage_Click);
            // 
            // btnFirstPage
            // 
            this.btnFirstPage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirstPage.Location = new System.Drawing.Point(97, 7);
            this.btnFirstPage.Name = "btnFirstPage";
            this.btnFirstPage.Size = new System.Drawing.Size(136, 25);
            this.btnFirstPage.TabIndex = 85;
            this.btnFirstPage.Text = "Preview first Page";
            this.btnFirstPage.UseVisualStyleBackColor = true;
            this.btnFirstPage.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnAll
            // 
            this.btnAll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAll.Location = new System.Drawing.Point(9, 6);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(83, 27);
            this.btnAll.TabIndex = 84;
            this.btnAll.Text = "Print All";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(704, 5);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 80;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(763, 5);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 81;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(822, 5);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(61, 30);
            this.btnexit.TabIndex = 83;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // grFront
            // 
            this.grFront.BackColor = System.Drawing.SystemColors.Control;
            this.grFront.Controls.Add(this.textBox1);
            this.grFront.Controls.Add(this.txtscr3);
            this.grFront.Controls.Add(this.txtscr2);
            this.grFront.Controls.Add(this.txtscr1);
            this.grFront.Controls.Add(this.btnser);
            this.grFront.Controls.Add(this.txtscr);
            this.grFront.Controls.Add(this.Dgv);
            this.grFront.Location = new System.Drawing.Point(7, 7);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(959, 578);
            this.grFront.TabIndex = 290;
            this.grFront.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(666, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(188, 26);
            this.textBox1.TabIndex = 93;
            // 
            // txtscr3
            // 
            this.txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(492, 16);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(188, 26);
            this.txtscr3.TabIndex = 92;
            // 
            // txtscr2
            // 
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(328, 16);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(165, 26);
            this.txtscr2.TabIndex = 91;
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(126, 16);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(201, 26);
            this.txtscr1.TabIndex = 90;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(858, 12);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(73, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(15, 16);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(115, 26);
            this.txtscr.TabIndex = 87;
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(13, 44);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(940, 528);
            this.Dgv.TabIndex = 0;
            // 
            // cmbFL5
            // 
            this.cmbFL5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFL5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFL5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFL5.FormattingEnabled = true;
            this.cmbFL5.Items.AddRange(new object[] {
            "TAMIL",
            "HINDI",
            "FRENCH"});
            this.cmbFL5.Location = new System.Drawing.Point(790, 469);
            this.cmbFL5.Name = "cmbFL5";
            this.cmbFL5.Size = new System.Drawing.Size(119, 26);
            this.cmbFL5.TabIndex = 344;
            // 
            // cmbFL4
            // 
            this.cmbFL4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFL4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFL4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFL4.FormattingEnabled = true;
            this.cmbFL4.Items.AddRange(new object[] {
            "TAMIL",
            "HINDI",
            "FRENCH"});
            this.cmbFL4.Location = new System.Drawing.Point(656, 469);
            this.cmbFL4.Name = "cmbFL4";
            this.cmbFL4.Size = new System.Drawing.Size(119, 26);
            this.cmbFL4.TabIndex = 343;
            // 
            // cmbFL3
            // 
            this.cmbFL3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFL3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFL3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFL3.FormattingEnabled = true;
            this.cmbFL3.Items.AddRange(new object[] {
            "TAMIL",
            "HINDI",
            "FRENCH"});
            this.cmbFL3.Location = new System.Drawing.Point(521, 469);
            this.cmbFL3.Name = "cmbFL3";
            this.cmbFL3.Size = new System.Drawing.Size(119, 26);
            this.cmbFL3.TabIndex = 342;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(243, 36);
            this.label12.TabIndex = 299;
            this.label12.Text = "Qualified For Promotion to Higher\r\nStandard under Hr.Sec Education Rules";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(89, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 18);
            this.label2.TabIndex = 287;
            this.label2.Text = "Student Name (In Tamil)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(621, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 18);
            this.label13.TabIndex = 298;
            this.label13.Text = "S No.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(48, 269);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(199, 36);
            this.label14.TabIndex = 301;
            this.label14.Text = "Pupil Has Paid All the Fees Due\r\nof the School";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(162, 319);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 18);
            this.label16.TabIndex = 305;
            this.label16.Text = "Scholarships";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(174, 211);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 290;
            this.label4.Text = "Aadhar No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(521, 211);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 289;
            this.label1.Text = "EMIS No";
            // 
            // txtsno
            // 
            this.txtsno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsno.Location = new System.Drawing.Point(672, 15);
            this.txtsno.MaxLength = 250;
            this.txtsno.Name = "txtsno";
            this.txtsno.Size = new System.Drawing.Size(221, 26);
            this.txtsno.TabIndex = 2;
            // 
            // txtAADHAR
            // 
            this.txtAADHAR.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAADHAR.Location = new System.Drawing.Point(253, 207);
            this.txtAADHAR.MaxLength = 250;
            this.txtAADHAR.Name = "txtAADHAR";
            this.txtAADHAR.Size = new System.Drawing.Size(221, 26);
            this.txtAADHAR.TabIndex = 11;
            // 
            // txtregNo
            // 
            this.txtregNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtregNo.Location = new System.Drawing.Point(672, 80);
            this.txtregNo.MaxLength = 250;
            this.txtregNo.Name = "txtregNo";
            this.txtregNo.Size = new System.Drawing.Size(221, 26);
            this.txtregNo.TabIndex = 4;
            // 
            // txtTMR
            // 
            this.txtTMR.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTMR.Location = new System.Drawing.Point(672, 46);
            this.txtTMR.MaxLength = 250;
            this.txtTMR.Name = "txtTMR";
            this.txtTMR.Size = new System.Drawing.Size(221, 26);
            this.txtTMR.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(562, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 18);
            this.label6.TabIndex = 295;
            this.label6.Text = "T.M.R  Code No ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(607, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 18);
            this.label11.TabIndex = 296;
            this.label11.Text = "Reg No :";
            // 
            // txttamilname
            // 
            this.txttamilname.Font = new System.Drawing.Font("TSCu_SaiIndira", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttamilname.Location = new System.Drawing.Point(253, 57);
            this.txttamilname.MaxLength = 250;
            this.txttamilname.Name = "txttamilname";
            this.txttamilname.Size = new System.Drawing.Size(221, 22);
            this.txttamilname.TabIndex = 5;
            // 
            // cmbqph
            // 
            this.cmbqph.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbqph.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbqph.FormattingEnabled = true;
            this.cmbqph.Items.AddRange(new object[] {
            "REFER MARKSHEET",
            "PROMOTED TO UKG",
            "PROMOTED TO I",
            "PROMOTED TO II",
            "PROMOTED TO III",
            "PROMOTED TO IV",
            "PROMOTED TO V",
            "PROMOTED TO VI",
            "PROMOTED TO VII",
            "PROMOTED TO VIII",
            "PROMOTED TO IX",
            "PROMOTED TO X",
            "PROMOTED TO XII",
            "DISCONTINUED",
            "YES, PROMOTED",
            "DETAINED",
            "IX STANDARD",
            "XI STANDARD",
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD"});
            this.cmbqph.Location = new System.Drawing.Point(253, 95);
            this.cmbqph.Name = "cmbqph";
            this.cmbqph.Size = new System.Drawing.Size(340, 26);
            this.cmbqph.TabIndex = 6;
            // 
            // cmbfeesdue
            // 
            this.cmbfeesdue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbfeesdue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbfeesdue.FormattingEnabled = true;
            this.cmbfeesdue.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbfeesdue.Location = new System.Drawing.Point(253, 274);
            this.cmbfeesdue.Name = "cmbfeesdue";
            this.cmbfeesdue.Size = new System.Drawing.Size(155, 26);
            this.cmbfeesdue.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(485, 142);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 18);
            this.label15.TabIndex = 302;
            this.label15.Text = "Conduct & Character";
            // 
            // cmbconduct
            // 
            this.cmbconduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbconduct.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbconduct.FormattingEnabled = true;
            this.cmbconduct.Items.AddRange(new object[] {
            "GOOD",
            "BETTER",
            "SATISFACTORY"});
            this.cmbconduct.Location = new System.Drawing.Point(615, 138);
            this.cmbconduct.Name = "cmbconduct";
            this.cmbconduct.Size = new System.Drawing.Size(155, 26);
            this.cmbconduct.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(454, 278);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 18);
            this.label17.TabIndex = 307;
            this.label17.Text = "Medical Inspection";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(144, 354);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 18);
            this.label18.TabIndex = 308;
            this.label18.Text = "Date of Leaving";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(375, 354);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(167, 18);
            this.label19.TabIndex = 309;
            this.label19.Text = "Date of Application For TC";
            // 
            // dtDol
            // 
            this.dtDol.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDol.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDol.Location = new System.Drawing.Point(253, 350);
            this.dtDol.Name = "dtDol";
            this.dtDol.Size = new System.Drawing.Size(117, 26);
            this.dtDol.TabIndex = 17;
            // 
            // dtDaTc
            // 
            this.dtDaTc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDaTc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDaTc.Location = new System.Drawing.Point(548, 350);
            this.dtDaTc.Name = "dtDaTc";
            this.dtDaTc.Size = new System.Drawing.Size(118, 26);
            this.dtDaTc.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(686, 354);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 18);
            this.label20.TabIndex = 312;
            this.label20.Text = "Date of TC";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(201, 244);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 18);
            this.label21.TabIndex = 313;
            this.label21.Text = "Group";
            // 
            // cmbgroup
            // 
            this.cmbgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbgroup.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbgroup.FormattingEnabled = true;
            this.cmbgroup.Items.AddRange(new object[] {
            "102-COMPUTER SCIENCE",
            "103-BIOLOGY",
            "302-COMMERCE",
            "308-COMMERCE"});
            this.cmbgroup.Location = new System.Drawing.Point(253, 240);
            this.cmbgroup.Name = "cmbgroup";
            this.cmbgroup.Size = new System.Drawing.Size(155, 26);
            this.cmbgroup.TabIndex = 13;
            // 
            // dtDtc
            // 
            this.dtDtc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDtc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDtc.Location = new System.Drawing.Point(763, 350);
            this.dtDtc.Name = "dtDtc";
            this.dtDtc.Size = new System.Drawing.Size(117, 26);
            this.dtDtc.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(144, 392);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 18);
            this.label22.TabIndex = 316;
            this.label22.Text = "Academic Years";
            // 
            // cmbAY1
            // 
            this.cmbAY1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAY1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAY1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAY1.FormattingEnabled = true;
            this.cmbAY1.Items.AddRange(new object[] {
            "2017 - 2018",
            "2016 - 2017",
            "2015 - 2016",
            "2014 - 2015",
            "2013 - 2014",
            "2012 - 2013",
            "2011 - 2012"});
            this.cmbAY1.Location = new System.Drawing.Point(253, 388);
            this.cmbAY1.Name = "cmbAY1";
            this.cmbAY1.Size = new System.Drawing.Size(119, 26);
            this.cmbAY1.TabIndex = 20;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(127, 433);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(120, 18);
            this.label23.TabIndex = 318;
            this.label23.Text = "Standards Studied";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(151, 473);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 18);
            this.label24.TabIndex = 319;
            this.label24.Text = "First Language";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(102, 510);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(145, 18);
            this.label25.TabIndex = 320;
            this.label25.Text = "Medium of Instruction";
            // 
            // cmbMI1
            // 
            this.cmbMI1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMI1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMI1.FormattingEnabled = true;
            this.cmbMI1.Items.AddRange(new object[] {
            "TAMIL",
            "ENGLISH"});
            this.cmbMI1.Location = new System.Drawing.Point(253, 506);
            this.cmbMI1.Name = "cmbMI1";
            this.cmbMI1.Size = new System.Drawing.Size(117, 26);
            this.cmbMI1.TabIndex = 26;
            // 
            // cmbMI2
            // 
            this.cmbMI2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMI2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMI2.FormattingEnabled = true;
            this.cmbMI2.Items.AddRange(new object[] {
            "TAMIL",
            "ENGLISH"});
            this.cmbMI2.Location = new System.Drawing.Point(387, 506);
            this.cmbMI2.Name = "cmbMI2";
            this.cmbMI2.Size = new System.Drawing.Size(121, 26);
            this.cmbMI2.TabIndex = 27;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(53, 545);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(194, 18);
            this.label26.TabIndex = 323;
            this.label26.Text = "Total Number of Working Days";
            // 
            // txt_totalNo_WD
            // 
            this.txt_totalNo_WD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totalNo_WD.Location = new System.Drawing.Point(846, 504);
            this.txt_totalNo_WD.MaxLength = 250;
            this.txt_totalNo_WD.Name = "txt_totalNo_WD";
            this.txt_totalNo_WD.Size = new System.Drawing.Size(108, 26);
            this.txt_totalNo_WD.TabIndex = 324;
            this.txt_totalNo_WD.Visible = false;
            // 
            // txtTNWD
            // 
            this.txtTNWD.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTNWD.Location = new System.Drawing.Point(253, 541);
            this.txtTNWD.MaxLength = 250;
            this.txtTNWD.Name = "txtTNWD";
            this.txtTNWD.Size = new System.Drawing.Size(117, 26);
            this.txtTNWD.TabIndex = 29;
            this.txtTNWD.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(384, 545);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(129, 18);
            this.label27.TabIndex = 326;
            this.label27.Text = "No. of Days Precent";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(633, 545);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 18);
            this.label29.TabIndex = 327;
            this.label29.Text = "Percentage";
            // 
            // txtEMIS
            // 
            this.txtEMIS.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEMIS.Location = new System.Drawing.Point(584, 207);
            this.txtEMIS.MaxLength = 250;
            this.txtEMIS.Name = "txtEMIS";
            this.txtEMIS.Size = new System.Drawing.Size(221, 26);
            this.txtEMIS.TabIndex = 12;
            // 
            // txtNDP
            // 
            this.txtNDP.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNDP.Location = new System.Drawing.Point(521, 541);
            this.txtNDP.MaxLength = 250;
            this.txtNDP.Name = "txtNDP";
            this.txtNDP.Size = new System.Drawing.Size(93, 26);
            this.txtNDP.TabIndex = 30;
            this.txtNDP.Text = "0";
            // 
            // txtpercentage
            // 
            this.txtpercentage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpercentage.Location = new System.Drawing.Point(717, 541);
            this.txtpercentage.MaxLength = 250;
            this.txtpercentage.Name = "txtpercentage";
            this.txtpercentage.Size = new System.Drawing.Size(88, 26);
            this.txtpercentage.TabIndex = 31;
            this.txtpercentage.Text = "0";
            // 
            // cmbAY2
            // 
            this.cmbAY2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAY2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAY2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAY2.FormattingEnabled = true;
            this.cmbAY2.Items.AddRange(new object[] {
            "2017 - 2018",
            "2016 - 2017",
            "2015 - 2016",
            "2014 - 2015",
            "2013 - 2014",
            "2012 - 2013",
            "2011 - 2012"});
            this.cmbAY2.Location = new System.Drawing.Point(389, 388);
            this.cmbAY2.Name = "cmbAY2";
            this.cmbAY2.Size = new System.Drawing.Size(119, 26);
            this.cmbAY2.TabIndex = 21;
            // 
            // cmbAY3
            // 
            this.cmbAY3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAY3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAY3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAY3.FormattingEnabled = true;
            this.cmbAY3.Items.AddRange(new object[] {
            "2017 - 2018",
            "2016 - 2017",
            "2015 - 2016",
            "2014 - 2015",
            "2013 - 2014",
            "2012 - 2013",
            "2011 - 2012"});
            this.cmbAY3.Location = new System.Drawing.Point(523, 388);
            this.cmbAY3.Name = "cmbAY3";
            this.cmbAY3.Size = new System.Drawing.Size(119, 26);
            this.cmbAY3.TabIndex = 332;
            // 
            // cmbAY4
            // 
            this.cmbAY4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAY4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAY4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAY4.FormattingEnabled = true;
            this.cmbAY4.Items.AddRange(new object[] {
            "2017 - 2018",
            "2016 - 2017",
            "2015 - 2016",
            "2014 - 2015",
            "2013 - 2014",
            "2012 - 2013",
            "2011 - 2012"});
            this.cmbAY4.Location = new System.Drawing.Point(658, 388);
            this.cmbAY4.Name = "cmbAY4";
            this.cmbAY4.Size = new System.Drawing.Size(119, 26);
            this.cmbAY4.TabIndex = 333;
            // 
            // cmbAY5
            // 
            this.cmbAY5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAY5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAY5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAY5.FormattingEnabled = true;
            this.cmbAY5.Items.AddRange(new object[] {
            "2017 - 2018",
            "2016 - 2017",
            "2015 - 2016",
            "2014 - 2015",
            "2013 - 2014",
            "2012 - 2013",
            "2011 - 2012"});
            this.cmbAY5.Location = new System.Drawing.Point(792, 388);
            this.cmbAY5.Name = "cmbAY5";
            this.cmbAY5.Size = new System.Drawing.Size(119, 26);
            this.cmbAY5.TabIndex = 334;
            // 
            // cmbSstud1
            // 
            this.cmbSstud1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSstud1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSstud1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSstud1.FormattingEnabled = true;
            this.cmbSstud1.Items.AddRange(new object[] {
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD",
            "IX STANDARD",
            "X STANDARD",
            "XI STANDARD",
            "XII STANDARD"});
            this.cmbSstud1.Location = new System.Drawing.Point(253, 429);
            this.cmbSstud1.Name = "cmbSstud1";
            this.cmbSstud1.Size = new System.Drawing.Size(119, 26);
            this.cmbSstud1.TabIndex = 22;
            // 
            // cmbSstud2
            // 
            this.cmbSstud2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSstud2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSstud2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSstud2.FormattingEnabled = true;
            this.cmbSstud2.Items.AddRange(new object[] {
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD",
            "IX STANDARD",
            "X STANDARD",
            "XI STANDARD",
            "XII STANDARD"});
            this.cmbSstud2.Location = new System.Drawing.Point(387, 429);
            this.cmbSstud2.Name = "cmbSstud2";
            this.cmbSstud2.Size = new System.Drawing.Size(119, 26);
            this.cmbSstud2.TabIndex = 23;
            // 
            // cmbSstud3
            // 
            this.cmbSstud3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSstud3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSstud3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSstud3.FormattingEnabled = true;
            this.cmbSstud3.Items.AddRange(new object[] {
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD",
            "IX STANDARD",
            "X STANDARD",
            "XI STANDARD",
            "XII STANDARD"});
            this.cmbSstud3.Location = new System.Drawing.Point(521, 429);
            this.cmbSstud3.Name = "cmbSstud3";
            this.cmbSstud3.Size = new System.Drawing.Size(119, 26);
            this.cmbSstud3.TabIndex = 337;
            // 
            // cmbSstud4
            // 
            this.cmbSstud4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSstud4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSstud4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSstud4.FormattingEnabled = true;
            this.cmbSstud4.Items.AddRange(new object[] {
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD",
            "IX STANDARD",
            "X STANDARD",
            "XI STANDARD",
            "XII STANDARD"});
            this.cmbSstud4.Location = new System.Drawing.Point(656, 429);
            this.cmbSstud4.Name = "cmbSstud4";
            this.cmbSstud4.Size = new System.Drawing.Size(119, 26);
            this.cmbSstud4.TabIndex = 338;
            // 
            // cmbSstud5
            // 
            this.cmbSstud5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSstud5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSstud5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSstud5.FormattingEnabled = true;
            this.cmbSstud5.Items.AddRange(new object[] {
            "VI STANDARD",
            "VII STANDARD",
            "VIII STANDARD",
            "IX STANDARD",
            "X STANDARD",
            "XI STANDARD",
            "XII STANDARD"});
            this.cmbSstud5.Location = new System.Drawing.Point(790, 429);
            this.cmbSstud5.Name = "cmbSstud5";
            this.cmbSstud5.Size = new System.Drawing.Size(119, 26);
            this.cmbSstud5.TabIndex = 339;
            // 
            // cmbFL1
            // 
            this.cmbFL1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFL1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFL1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFL1.FormattingEnabled = true;
            this.cmbFL1.Items.AddRange(new object[] {
            "TAMIL",
            "HINDI",
            "FRENCH"});
            this.cmbFL1.Location = new System.Drawing.Point(253, 469);
            this.cmbFL1.Name = "cmbFL1";
            this.cmbFL1.Size = new System.Drawing.Size(119, 26);
            this.cmbFL1.TabIndex = 24;
            // 
            // cmbFL2
            // 
            this.cmbFL2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFL2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFL2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFL2.FormattingEnabled = true;
            this.cmbFL2.Items.AddRange(new object[] {
            "FRENCH",
            "HINDI",
            "TAMIL"});
            this.cmbFL2.Location = new System.Drawing.Point(387, 469);
            this.cmbFL2.Name = "cmbFL2";
            this.cmbFL2.Size = new System.Drawing.Size(119, 26);
            this.cmbFL2.TabIndex = 25;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(815, 537);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(76, 30);
            this.btnsave.TabIndex = 77;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.txtStudNameSearch);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridStudent);
            this.grSearch.Controls.Add(this.txtsclname);
            this.grSearch.Controls.Add(this.txteddis);
            this.grSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grSearch.Location = new System.Drawing.Point(336, 40);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(403, 280);
            this.grSearch.TabIndex = 286;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // txtStudNameSearch
            // 
            this.txtStudNameSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStudNameSearch.Location = new System.Drawing.Point(10, 18);
            this.txtStudNameSearch.Name = "txtStudNameSearch";
            this.txtStudNameSearch.Size = new System.Drawing.Size(373, 26);
            this.txtStudNameSearch.TabIndex = 4;
            this.txtStudNameSearch.TextChanged += new System.EventHandler(this.txtStudNameSearch_TextChanged);
            this.txtStudNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStudNameSearch_KeyDown);
            // 
            // btnHide
            // 
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Location = new System.Drawing.Point(326, 251);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(61, 26);
            this.btnHide.TabIndex = 4;
            this.btnHide.Text = "Hide";
            this.btnHide.UseVisualStyleBackColor = true;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridStudent
            // 
            this.DataGridStudent.AllowUserToAddRows = false;
            this.DataGridStudent.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStudent.Location = new System.Drawing.Point(10, 47);
            this.DataGridStudent.Name = "DataGridStudent";
            this.DataGridStudent.RowHeadersVisible = false;
            this.DataGridStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStudent.Size = new System.Drawing.Size(377, 204);
            this.DataGridStudent.TabIndex = 0;
            this.DataGridStudent.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStudent_CellMouseDoubleClick);
            this.DataGridStudent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridStudent_KeyDown);
            // 
            // txtsclname
            // 
            this.txtsclname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsclname.Location = new System.Drawing.Point(75, 165);
            this.txtsclname.MaxLength = 250;
            this.txtsclname.Name = "txtsclname";
            this.txtsclname.Size = new System.Drawing.Size(154, 26);
            this.txtsclname.TabIndex = 288;
            // 
            // txteddis
            // 
            this.txteddis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txteddis.Location = new System.Drawing.Point(123, 167);
            this.txteddis.MaxLength = 250;
            this.txteddis.Name = "txteddis";
            this.txteddis.Size = new System.Drawing.Size(154, 26);
            this.txteddis.TabIndex = 289;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(890, 538);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 30);
            this.btnaddrcan.TabIndex = 76;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // txtstudent
            // 
            this.txtstudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstudent.Location = new System.Drawing.Point(338, 16);
            this.txtstudent.MaxLength = 250;
            this.txtstudent.Name = "txtstudent";
            this.txtstudent.Size = new System.Drawing.Size(276, 24);
            this.txtstudent.TabIndex = 1;
            // 
            // cmbTcType
            // 
            this.cmbTcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTcType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTcType.FormattingEnabled = true;
            this.cmbTcType.Items.AddRange(new object[] {
            "Secondary",
            "Higher Secondary",
            "Primary"});
            this.cmbTcType.Location = new System.Drawing.Point(107, 16);
            this.cmbTcType.Name = "cmbTcType";
            this.cmbTcType.Size = new System.Drawing.Size(155, 26);
            this.cmbTcType.TabIndex = 0;
            this.cmbTcType.SelectedIndexChanged += new System.EventHandler(this.cmbTcType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(274, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 346;
            this.label3.Text = "Student";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 18);
            this.label5.TabIndex = 348;
            this.label5.Text = "Select Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(523, 510);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(195, 18);
            this.label7.TabIndex = 349;
            this.label7.Text = "Language offered under Part-1";
            // 
            // LP_1
            // 
            this.LP_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LP_1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LP_1.FormattingEnabled = true;
            this.LP_1.Items.AddRange(new object[] {
            "TAMIL",
            "ENGLISH"});
            this.LP_1.Location = new System.Drawing.Point(724, 506);
            this.LP_1.Name = "LP_1";
            this.LP_1.Size = new System.Drawing.Size(117, 26);
            this.LP_1.TabIndex = 28;
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.txtVocEdu);
            this.grBack.Controls.Add(this.cmbGenedu);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtPWS);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.LP_1);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.cmbTcType);
            this.grBack.Controls.Add(this.txtstudent);
            this.grBack.Controls.Add(this.btnaddrcan);
            this.grBack.Controls.Add(this.btnsave);
            this.grBack.Controls.Add(this.cmbFL2);
            this.grBack.Controls.Add(this.cmbFL1);
            this.grBack.Controls.Add(this.cmbSstud5);
            this.grBack.Controls.Add(this.cmbSstud4);
            this.grBack.Controls.Add(this.cmbSstud3);
            this.grBack.Controls.Add(this.cmbSstud2);
            this.grBack.Controls.Add(this.cmbSstud1);
            this.grBack.Controls.Add(this.cmbAY5);
            this.grBack.Controls.Add(this.cmbAY4);
            this.grBack.Controls.Add(this.cmbAY3);
            this.grBack.Controls.Add(this.cmbAY2);
            this.grBack.Controls.Add(this.txtpercentage);
            this.grBack.Controls.Add(this.txtNDP);
            this.grBack.Controls.Add(this.txtEMIS);
            this.grBack.Controls.Add(this.label29);
            this.grBack.Controls.Add(this.label27);
            this.grBack.Controls.Add(this.txtTNWD);
            this.grBack.Controls.Add(this.txt_totalNo_WD);
            this.grBack.Controls.Add(this.label26);
            this.grBack.Controls.Add(this.cmbMI2);
            this.grBack.Controls.Add(this.cmbMI1);
            this.grBack.Controls.Add(this.label25);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.label23);
            this.grBack.Controls.Add(this.cmbAY1);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.dtDtc);
            this.grBack.Controls.Add(this.cmbgroup);
            this.grBack.Controls.Add(this.label21);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.dtDaTc);
            this.grBack.Controls.Add(this.dtDol);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.label18);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.cmbfeesdue);
            this.grBack.Controls.Add(this.cmbqph);
            this.grBack.Controls.Add(this.txttamilname);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtTMR);
            this.grBack.Controls.Add(this.txtregNo);
            this.grBack.Controls.Add(this.txtsno);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.cmbFL3);
            this.grBack.Controls.Add(this.cmbFL4);
            this.grBack.Controls.Add(this.cmbFL5);
            this.grBack.Controls.Add(this.cmbMedical);
            this.grBack.Controls.Add(this.cmbScholarship);
            this.grBack.Controls.Add(this.cmbconduct);
            this.grBack.Controls.Add(this.txtAADHAR);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Location = new System.Drawing.Point(7, 5);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(960, 583);
            this.grBack.TabIndex = 293;
            this.grBack.TabStop = false;
            this.grBack.Enter += new System.EventHandler(this.grBack_Enter);
            // 
            // txtVocEdu
            // 
            this.txtVocEdu.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVocEdu.Location = new System.Drawing.Point(584, 173);
            this.txtVocEdu.MaxLength = 250;
            this.txtVocEdu.Name = "txtVocEdu";
            this.txtVocEdu.Size = new System.Drawing.Size(221, 26);
            this.txtVocEdu.TabIndex = 10;
            // 
            // cmbGenedu
            // 
            this.cmbGenedu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGenedu.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGenedu.FormattingEnabled = true;
            this.cmbGenedu.Items.AddRange(new object[] {
            "GENERAL EDUCATION",
            "VOCATIONAL EDUCATION"});
            this.cmbGenedu.Location = new System.Drawing.Point(253, 173);
            this.cmbGenedu.Name = "cmbGenedu";
            this.cmbGenedu.Size = new System.Drawing.Size(222, 26);
            this.cmbGenedu.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(509, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 18);
            this.label10.TabIndex = 360;
            this.label10.Text = "Education";
            // 
            // txtPWS
            // 
            this.txtPWS.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPWS.Location = new System.Drawing.Point(253, 138);
            this.txtPWS.MaxLength = 250;
            this.txtPWS.Name = "txtPWS";
            this.txtPWS.Size = new System.Drawing.Size(221, 26);
            this.txtPWS.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(122, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 18);
            this.label8.TabIndex = 351;
            this.label8.Text = "Pupil Was Studying";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(120, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 18);
            this.label9.TabIndex = 352;
            this.label9.Text = "Vecation Education ";
            // 
            // cmbMedical
            // 
            this.cmbMedical.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMedical.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMedical.FormattingEnabled = true;
            this.cmbMedical.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbMedical.Location = new System.Drawing.Point(584, 274);
            this.cmbMedical.Name = "cmbMedical";
            this.cmbMedical.Size = new System.Drawing.Size(155, 26);
            this.cmbMedical.TabIndex = 15;
            // 
            // cmbScholarship
            // 
            this.cmbScholarship.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScholarship.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbScholarship.FormattingEnabled = true;
            this.cmbScholarship.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.cmbScholarship.Location = new System.Drawing.Point(253, 319);
            this.cmbScholarship.Name = "cmbScholarship";
            this.cmbScholarship.Size = new System.Drawing.Size(155, 26);
            this.cmbScholarship.TabIndex = 16;
            this.cmbScholarship.SelectedIndexChanged += new System.EventHandler(this.cmbScholarship_SelectedIndexChanged);
            // 
            // FrmTcDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(974, 631);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmTcDetails";
            this.Text = "Tc Details";
            this.Load += new System.EventHandler(this.FrmStud2_Load);
            this.panadd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.grSearch.ResumeLayout(false);
            this.grSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStudent)).EndInit();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnFirstPage;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.GroupBox grFront;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.TextBox txtscr3;
        internal System.Windows.Forms.TextBox txtscr2;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.Button btnSecondPage;
        private System.Windows.Forms.ComboBox cmbFL5;
        private System.Windows.Forms.ComboBox cmbFL4;
        private System.Windows.Forms.ComboBox cmbFL3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox txtsno;
        internal System.Windows.Forms.TextBox txtAADHAR;
        internal System.Windows.Forms.TextBox txtregNo;
        internal System.Windows.Forms.TextBox txtTMR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        internal System.Windows.Forms.TextBox txttamilname;
        private System.Windows.Forms.ComboBox cmbqph;
        private System.Windows.Forms.ComboBox cmbfeesdue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbconduct;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtDol;
        private System.Windows.Forms.DateTimePicker dtDaTc;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbgroup;
        private System.Windows.Forms.DateTimePicker dtDtc;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cmbAY1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbMI1;
        private System.Windows.Forms.ComboBox cmbMI2;
        private System.Windows.Forms.Label label26;
        internal System.Windows.Forms.TextBox txt_totalNo_WD;
        internal System.Windows.Forms.TextBox txtTNWD;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        internal System.Windows.Forms.TextBox txtEMIS;
        internal System.Windows.Forms.TextBox txtNDP;
        internal System.Windows.Forms.TextBox txtpercentage;
        private System.Windows.Forms.ComboBox cmbAY2;
        private System.Windows.Forms.ComboBox cmbAY3;
        private System.Windows.Forms.ComboBox cmbAY4;
        private System.Windows.Forms.ComboBox cmbAY5;
        private System.Windows.Forms.ComboBox cmbSstud1;
        private System.Windows.Forms.ComboBox cmbSstud2;
        private System.Windows.Forms.ComboBox cmbSstud3;
        private System.Windows.Forms.ComboBox cmbSstud4;
        private System.Windows.Forms.ComboBox cmbSstud5;
        private System.Windows.Forms.ComboBox cmbFL1;
        private System.Windows.Forms.ComboBox cmbFL2;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.TextBox txtStudNameSearch;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridStudent;
        internal System.Windows.Forms.TextBox txtsclname;
        internal System.Windows.Forms.TextBox txteddis;
        private System.Windows.Forms.Button btnaddrcan;
        internal System.Windows.Forms.TextBox txtstudent;
        private System.Windows.Forms.ComboBox cmbTcType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox LP_1;
        private System.Windows.Forms.GroupBox grBack;
        internal System.Windows.Forms.TextBox txtPWS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbMedical;
        private System.Windows.Forms.ComboBox cmbScholarship;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmbGenedu;
        internal System.Windows.Forms.TextBox txtVocEdu;
    }
}