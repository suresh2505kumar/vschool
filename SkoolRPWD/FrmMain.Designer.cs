﻿namespace SkoolRPWD
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeMastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeStructureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vanMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherFeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeConcessionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.feeReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeReconciliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tCIssueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boardingPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentBusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overDueReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeRemainderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentToolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ClasstoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeGrouptoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeMasttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeStructtoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.BustoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.VantoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentsImporttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ExittoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassTransfertoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeConsessiontoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeReceipttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.termFeeReconciliationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeRecontoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.TcIssuetoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.feeUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tCDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.StudBustoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.OverDuetoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ReceipttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeReporttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentRpttoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentLedgertoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.FeeRemindertoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileMenu
            // 
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(60, 20);
            this.fileMenu.Text = "Masters";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.newToolStripMenuItem.Text = "Student\'s Mster";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.openToolStripMenuItem.Text = "Class Master";
            // 
            // feeGroupToolStripMenuItem
            // 
            this.feeGroupToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeGroupToolStripMenuItem.Image")));
            this.feeGroupToolStripMenuItem.Name = "feeGroupToolStripMenuItem";
            this.feeGroupToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.feeGroupToolStripMenuItem.Text = "Fee Group";
            // 
            // feeMastToolStripMenuItem
            // 
            this.feeMastToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeMastToolStripMenuItem.Image")));
            this.feeMastToolStripMenuItem.Name = "feeMastToolStripMenuItem";
            this.feeMastToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.feeMastToolStripMenuItem.Text = "Fee Mast";
            // 
            // feeStructureToolStripMenuItem
            // 
            this.feeStructureToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeStructureToolStripMenuItem.Image")));
            this.feeStructureToolStripMenuItem.Name = "feeStructureToolStripMenuItem";
            this.feeStructureToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.feeStructureToolStripMenuItem.Text = "Fee Structure";
            // 
            // busToolStripMenuItem
            // 
            this.busToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("busToolStripMenuItem.Image")));
            this.busToolStripMenuItem.Name = "busToolStripMenuItem";
            this.busToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.busToolStripMenuItem.Text = "Bus Master";
            // 
            // vanMToolStripMenuItem
            // 
            this.vanMToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vanMToolStripMenuItem.Image")));
            this.vanMToolStripMenuItem.Name = "vanMToolStripMenuItem";
            this.vanMToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.vanMToolStripMenuItem.Text = "Van Master";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.otherFeeToolStripMenuItem,
            this.feeConcessionToolStripMenuItem1,
            this.feeReceiptToolStripMenuItem,
            this.feeReconciliToolStripMenuItem,
            this.classToolStripMenuItem,
            this.tCIssueToolStripMenuItem});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(86, 20);
            this.editMenu.Text = "Transactions";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("undoToolStripMenuItem.Image")));
            this.undoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.undoToolStripMenuItem.Text = "Class Transfer";
            // 
            // otherFeeToolStripMenuItem
            // 
            this.otherFeeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("otherFeeToolStripMenuItem.Image")));
            this.otherFeeToolStripMenuItem.Name = "otherFeeToolStripMenuItem";
            this.otherFeeToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.otherFeeToolStripMenuItem.Text = "Other Fee Posting";
            // 
            // feeConcessionToolStripMenuItem1
            // 
            this.feeConcessionToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("feeConcessionToolStripMenuItem1.Image")));
            this.feeConcessionToolStripMenuItem1.Name = "feeConcessionToolStripMenuItem1";
            this.feeConcessionToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.feeConcessionToolStripMenuItem1.Text = "Fee Concession";
            // 
            // feeReceiptToolStripMenuItem
            // 
            this.feeReceiptToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeReceiptToolStripMenuItem.Image")));
            this.feeReceiptToolStripMenuItem.Name = "feeReceiptToolStripMenuItem";
            this.feeReceiptToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.feeReceiptToolStripMenuItem.Text = "Fee Receipt";
            // 
            // feeReconciliToolStripMenuItem
            // 
            this.feeReconciliToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeReconciliToolStripMenuItem.Image")));
            this.feeReconciliToolStripMenuItem.Name = "feeReconciliToolStripMenuItem";
            this.feeReconciliToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.feeReconciliToolStripMenuItem.Text = "Fee Reconciliation";
            // 
            // classToolStripMenuItem
            // 
            this.classToolStripMenuItem.Name = "classToolStripMenuItem";
            this.classToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.classToolStripMenuItem.Text = "Class Reconciliation";
            this.classToolStripMenuItem.Visible = false;
            // 
            // tCIssueToolStripMenuItem
            // 
            this.tCIssueToolStripMenuItem.Name = "tCIssueToolStripMenuItem";
            this.tCIssueToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.tCIssueToolStripMenuItem.Text = "TC Issue";
            // 
            // boardingPointToolStripMenuItem
            // 
            this.boardingPointToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentBusToolStripMenuItem});
            this.boardingPointToolStripMenuItem.Name = "boardingPointToolStripMenuItem";
            this.boardingPointToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.boardingPointToolStripMenuItem.Text = "Boarding Point";
            // 
            // studentBusToolStripMenuItem
            // 
            this.studentBusToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("studentBusToolStripMenuItem.Image")));
            this.studentBusToolStripMenuItem.Name = "studentBusToolStripMenuItem";
            this.studentBusToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.studentBusToolStripMenuItem.Text = "Student Bus";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.overDueReportToolStripMenuItem,
            this.feeReportToolStripMenuItem,
            this.toolStripMenuItem1,
            this.studentReportToolStripMenuItem,
            this.studentLedgerToolStripMenuItem,
            this.feeRemainderToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // overDueReportToolStripMenuItem
            // 
            this.overDueReportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("overDueReportToolStripMenuItem.Image")));
            this.overDueReportToolStripMenuItem.Name = "overDueReportToolStripMenuItem";
            this.overDueReportToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.overDueReportToolStripMenuItem.Text = "Over Due Report";
            // 
            // feeReportToolStripMenuItem
            // 
            this.feeReportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeReportToolStripMenuItem.Image")));
            this.feeReportToolStripMenuItem.Name = "feeReportToolStripMenuItem";
            this.feeReportToolStripMenuItem.ShowShortcutKeys = false;
            this.feeReportToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.feeReportToolStripMenuItem.Text = "Fee Report";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShowShortcutKeys = false;
            this.toolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.toolStripMenuItem1.Text = "Receipt";
            // 
            // studentReportToolStripMenuItem
            // 
            this.studentReportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("studentReportToolStripMenuItem.Image")));
            this.studentReportToolStripMenuItem.Name = "studentReportToolStripMenuItem";
            this.studentReportToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.studentReportToolStripMenuItem.Text = "Student Report";
            // 
            // studentLedgerToolStripMenuItem
            // 
            this.studentLedgerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("studentLedgerToolStripMenuItem.Image")));
            this.studentLedgerToolStripMenuItem.Name = "studentLedgerToolStripMenuItem";
            this.studentLedgerToolStripMenuItem.ShowShortcutKeys = false;
            this.studentLedgerToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.studentLedgerToolStripMenuItem.Text = "Student Ledger";
            // 
            // feeRemainderToolStripMenuItem
            // 
            this.feeRemainderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("feeRemainderToolStripMenuItem.Image")));
            this.feeRemainderToolStripMenuItem.Name = "feeRemainderToolStripMenuItem";
            this.feeRemainderToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.feeRemainderToolStripMenuItem.Text = "Fee Remainder";
            // 
            // sMSToolStripMenuItem
            // 
            this.sMSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smsToolStripMenuItem1});
            this.sMSToolStripMenuItem.Name = "sMSToolStripMenuItem";
            this.sMSToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.sMSToolStripMenuItem.Text = "SMS";
            // 
            // smsToolStripMenuItem1
            // 
            this.smsToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("smsToolStripMenuItem1.Image")));
            this.smsToolStripMenuItem1.Name = "smsToolStripMenuItem1";
            this.smsToolStripMenuItem1.ShowShortcutKeys = false;
            this.smsToolStripMenuItem1.Size = new System.Drawing.Size(89, 22);
            this.smsToolStripMenuItem1.Text = "Sms";
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            this.viewMenu.Visible = false;
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Checked = true;
            this.toolBarToolStripMenuItem.CheckOnClick = true;
            this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.toolBarToolStripMenuItem.Text = "&Toolbar";
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.statusBarToolStripMenuItem.Text = "&Status Bar";
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator2,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(632, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            this.toolStrip.Visible = false;
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "New";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Open";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Save";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Print";
            // 
            // printPreviewToolStripButton
            // 
            this.printPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printPreviewToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripButton.Image")));
            this.printPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
            this.printPreviewToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printPreviewToolStripButton.Text = "Print Preview";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "Help";
            // 
            // statusStrip
            // 
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(927, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem11,
            this.toolStripMenuItem18,
            this.toolStripMenuItem20,
            this.toolStripMenuItem26,
            this.toolStripMenuItem28});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(927, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.Visible = false;
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StudentToolStripMenu,
            this.ClasstoolStripMenu,
            this.FeeGrouptoolStripMenu,
            this.FeeMasttoolStripMenu,
            this.FeeStructtoolStripMenu,
            this.BustoolStripMenu,
            this.VantoolStripMenuItem,
            this.StudentsImporttoolStripMenu,
            this.ExittoolStripMenu});
            this.toolStripMenuItem2.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(60, 20);
            this.toolStripMenuItem2.Text = "Masters";
            // 
            // StudentToolStripMenu
            // 
            this.StudentToolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("StudentToolStripMenu.Image")));
            this.StudentToolStripMenu.ImageTransparentColor = System.Drawing.Color.Black;
            this.StudentToolStripMenu.Name = "StudentToolStripMenu";
            this.StudentToolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.StudentToolStripMenu.Text = "Students Master";
            this.StudentToolStripMenu.Click += new System.EventHandler(this.StudentToolStripMenu_Click);
            // 
            // ClasstoolStripMenu
            // 
            this.ClasstoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("ClasstoolStripMenu.Image")));
            this.ClasstoolStripMenu.ImageTransparentColor = System.Drawing.Color.Black;
            this.ClasstoolStripMenu.Name = "ClasstoolStripMenu";
            this.ClasstoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.ClasstoolStripMenu.Text = "Class Master";
            this.ClasstoolStripMenu.Click += new System.EventHandler(this.ClasstoolStripMenu_Click);
            // 
            // FeeGrouptoolStripMenu
            // 
            this.FeeGrouptoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeGrouptoolStripMenu.Image")));
            this.FeeGrouptoolStripMenu.Name = "FeeGrouptoolStripMenu";
            this.FeeGrouptoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.FeeGrouptoolStripMenu.Text = "Fee Group";
            this.FeeGrouptoolStripMenu.Click += new System.EventHandler(this.FeeGrouptoolStripMenu_Click);
            // 
            // FeeMasttoolStripMenu
            // 
            this.FeeMasttoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeMasttoolStripMenu.Image")));
            this.FeeMasttoolStripMenu.Name = "FeeMasttoolStripMenu";
            this.FeeMasttoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.FeeMasttoolStripMenu.Text = "Fee Master";
            this.FeeMasttoolStripMenu.Click += new System.EventHandler(this.FeeMasttoolStripMenu_Click);
            // 
            // FeeStructtoolStripMenu
            // 
            this.FeeStructtoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeStructtoolStripMenu.Image")));
            this.FeeStructtoolStripMenu.Name = "FeeStructtoolStripMenu";
            this.FeeStructtoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.FeeStructtoolStripMenu.Text = "Fee Structure";
            this.FeeStructtoolStripMenu.Click += new System.EventHandler(this.FeeStructtoolStripMenu_Click);
            // 
            // BustoolStripMenu
            // 
            this.BustoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("BustoolStripMenu.Image")));
            this.BustoolStripMenu.Name = "BustoolStripMenu";
            this.BustoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.BustoolStripMenu.Text = "Bus Master";
            this.BustoolStripMenu.Click += new System.EventHandler(this.BustoolStripMenu_Click);
            // 
            // VantoolStripMenuItem
            // 
            this.VantoolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("VantoolStripMenuItem.Image")));
            this.VantoolStripMenuItem.Name = "VantoolStripMenuItem";
            this.VantoolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.VantoolStripMenuItem.Text = "Van Master";
            this.VantoolStripMenuItem.Click += new System.EventHandler(this.VantoolStripMenuItem_Click_1);
            // 
            // StudentsImporttoolStripMenu
            // 
            this.StudentsImporttoolStripMenu.Name = "StudentsImporttoolStripMenu";
            this.StudentsImporttoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.StudentsImporttoolStripMenu.Text = "Import Students";
            this.StudentsImporttoolStripMenu.Visible = false;
            this.StudentsImporttoolStripMenu.Click += new System.EventHandler(this.StudentsImporttoolStripMenu_Click);
            // 
            // ExittoolStripMenu
            // 
            this.ExittoolStripMenu.Name = "ExittoolStripMenu";
            this.ExittoolStripMenu.Size = new System.Drawing.Size(159, 22);
            this.ExittoolStripMenu.Text = "E&xit";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClassTransfertoolStripMenuItem,
            this.toolStripMenuItem13,
            this.FeeConsessiontoolStripMenu,
            this.FeeReceipttoolStripMenu,
            this.termFeeReconciliationToolStripMenuItem,
            this.FeeRecontoolStripMenu,
            this.TcIssuetoolStripMenu,
            this.feeUpdateToolStripMenuItem,
            this.tCDetailsToolStripMenuItem});
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(85, 20);
            this.toolStripMenuItem11.Text = "Transactions";
            // 
            // ClassTransfertoolStripMenuItem
            // 
            this.ClassTransfertoolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ClassTransfertoolStripMenuItem.Image")));
            this.ClassTransfertoolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.ClassTransfertoolStripMenuItem.Name = "ClassTransfertoolStripMenuItem";
            this.ClassTransfertoolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ClassTransfertoolStripMenuItem.Text = "Class Transfer";
            this.ClassTransfertoolStripMenuItem.Click += new System.EventHandler(this.ClassTransfertoolStripMenuItem_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem13.Image")));
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem13.Text = "Other Fee Posting";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // FeeConsessiontoolStripMenu
            // 
            this.FeeConsessiontoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeConsessiontoolStripMenu.Image")));
            this.FeeConsessiontoolStripMenu.Name = "FeeConsessiontoolStripMenu";
            this.FeeConsessiontoolStripMenu.Size = new System.Drawing.Size(170, 22);
            this.FeeConsessiontoolStripMenu.Text = "Fee Concession";
            this.FeeConsessiontoolStripMenu.Click += new System.EventHandler(this.FeeConsessiontoolStripMenu_Click);
            // 
            // FeeReceipttoolStripMenu
            // 
            this.FeeReceipttoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeReceipttoolStripMenu.Image")));
            this.FeeReceipttoolStripMenu.Name = "FeeReceipttoolStripMenu";
            this.FeeReceipttoolStripMenu.Size = new System.Drawing.Size(170, 22);
            this.FeeReceipttoolStripMenu.Text = "Fee Receipt";
            this.FeeReceipttoolStripMenu.Click += new System.EventHandler(this.FeeReceipttoolStripMenu_Click);
            // 
            // termFeeReconciliationToolStripMenuItem
            // 
            this.termFeeReconciliationToolStripMenuItem.Name = "termFeeReconciliationToolStripMenuItem";
            this.termFeeReconciliationToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.termFeeReconciliationToolStripMenuItem.Text = "View Term Fees";
            this.termFeeReconciliationToolStripMenuItem.Click += new System.EventHandler(this.termFeeReconciliationToolStripMenuItem_Click);
            // 
            // FeeRecontoolStripMenu
            // 
            this.FeeRecontoolStripMenu.Name = "FeeRecontoolStripMenu";
            this.FeeRecontoolStripMenu.Size = new System.Drawing.Size(170, 22);
            this.FeeRecontoolStripMenu.Text = "Fee Reconciliation";
            this.FeeRecontoolStripMenu.Click += new System.EventHandler(this.FeeRecontoolStripMenu_Click);
            // 
            // TcIssuetoolStripMenu
            // 
            this.TcIssuetoolStripMenu.Name = "TcIssuetoolStripMenu";
            this.TcIssuetoolStripMenu.Size = new System.Drawing.Size(170, 22);
            this.TcIssuetoolStripMenu.Text = "Tc Issue";
            this.TcIssuetoolStripMenu.Click += new System.EventHandler(this.TcIssuetoolStripMenu_Click);
            // 
            // feeUpdateToolStripMenuItem
            // 
            this.feeUpdateToolStripMenuItem.Name = "feeUpdateToolStripMenuItem";
            this.feeUpdateToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.feeUpdateToolStripMenuItem.Text = "Fees Update";
            this.feeUpdateToolStripMenuItem.Click += new System.EventHandler(this.feeUpdateToolStripMenuItem_Click);
            // 
            // tCDetailsToolStripMenuItem
            // 
            this.tCDetailsToolStripMenuItem.Name = "tCDetailsToolStripMenuItem";
            this.tCDetailsToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.tCDetailsToolStripMenuItem.Text = "TC Details";
            this.tCDetailsToolStripMenuItem.Click += new System.EventHandler(this.tCDetailsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StudBustoolStripMenu});
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(98, 20);
            this.toolStripMenuItem18.Text = "Boarding Point";
            // 
            // StudBustoolStripMenu
            // 
            this.StudBustoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("StudBustoolStripMenu.Image")));
            this.StudBustoolStripMenu.Name = "StudBustoolStripMenu";
            this.StudBustoolStripMenu.Size = new System.Drawing.Size(137, 22);
            this.StudBustoolStripMenu.Text = "Student Bus";
            this.StudBustoolStripMenu.Click += new System.EventHandler(this.StudBustoolStripMenu_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OverDuetoolStripMenu,
            this.ReceipttoolStripMenu,
            this.FeeReporttoolStripMenu,
            this.StudentRpttoolStripMenu,
            this.StudentLedgertoolStripMenu,
            this.FeeRemindertoolStripMenu});
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(59, 20);
            this.toolStripMenuItem20.Text = "Reports";
            // 
            // OverDuetoolStripMenu
            // 
            this.OverDuetoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("OverDuetoolStripMenu.Image")));
            this.OverDuetoolStripMenu.Name = "OverDuetoolStripMenu";
            this.OverDuetoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.OverDuetoolStripMenu.Text = "Over Due Report";
            this.OverDuetoolStripMenu.Click += new System.EventHandler(this.OverDuetoolStripMenu_Click);
            // 
            // ReceipttoolStripMenu
            // 
            this.ReceipttoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("ReceipttoolStripMenu.Image")));
            this.ReceipttoolStripMenu.Name = "ReceipttoolStripMenu";
            this.ReceipttoolStripMenu.ShowShortcutKeys = false;
            this.ReceipttoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.ReceipttoolStripMenu.Text = "Receipt Report";
            this.ReceipttoolStripMenu.Click += new System.EventHandler(this.ReceipttoolStripMenu_Click);
            // 
            // FeeReporttoolStripMenu
            // 
            this.FeeReporttoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("FeeReporttoolStripMenu.Image")));
            this.FeeReporttoolStripMenu.Name = "FeeReporttoolStripMenu";
            this.FeeReporttoolStripMenu.ShowShortcutKeys = false;
            this.FeeReporttoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.FeeReporttoolStripMenu.Text = "Fee Report";
            this.FeeReporttoolStripMenu.Click += new System.EventHandler(this.FeeReporttoolStripMenu_Click);
            // 
            // StudentRpttoolStripMenu
            // 
            this.StudentRpttoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("StudentRpttoolStripMenu.Image")));
            this.StudentRpttoolStripMenu.Name = "StudentRpttoolStripMenu";
            this.StudentRpttoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.StudentRpttoolStripMenu.Text = "Student Report";
            this.StudentRpttoolStripMenu.Click += new System.EventHandler(this.StudentRpttoolStripMenu_Click);
            // 
            // StudentLedgertoolStripMenu
            // 
            this.StudentLedgertoolStripMenu.Image = ((System.Drawing.Image)(resources.GetObject("StudentLedgertoolStripMenu.Image")));
            this.StudentLedgertoolStripMenu.Name = "StudentLedgertoolStripMenu";
            this.StudentLedgertoolStripMenu.ShowShortcutKeys = false;
            this.StudentLedgertoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.StudentLedgertoolStripMenu.Text = "Student Ledger";
            this.StudentLedgertoolStripMenu.Click += new System.EventHandler(this.StudentLedgertoolStripMenu_Click);
            // 
            // FeeRemindertoolStripMenu
            // 
            this.FeeRemindertoolStripMenu.Name = "FeeRemindertoolStripMenu";
            this.FeeRemindertoolStripMenu.Size = new System.Drawing.Size(161, 22);
            this.FeeRemindertoolStripMenu.Text = "Fee Remainder";
            this.FeeRemindertoolStripMenu.Click += new System.EventHandler(this.FeeRemindertoolStripMenu_Click);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem27});
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(42, 20);
            this.toolStripMenuItem26.Text = "SMS";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem27.Image")));
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.ShowShortcutKeys = false;
            this.toolStripMenuItem27.Size = new System.Drawing.Size(89, 22);
            this.toolStripMenuItem27.Text = "Sms";
            this.toolStripMenuItem27.Click += new System.EventHandler(this.toolStripMenuItem27_Click);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem29,
            this.toolStripMenuItem30});
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(44, 20);
            this.toolStripMenuItem28.Text = "&View";
            this.toolStripMenuItem28.Visible = false;
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Checked = true;
            this.toolStripMenuItem29.CheckOnClick = true;
            this.toolStripMenuItem29.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem29.Text = "&Toolbar";
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Checked = true;
            this.toolStripMenuItem30.CheckOnClick = true;
            this.toolStripMenuItem30.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem30.Text = "&Status Bar";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(927, 453);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.Text = "Myla School";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem feeGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeMastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeStructureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeConcessionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem otherFeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overDueReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem studentLedgerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boardingPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentBusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vanMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeReconciliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeRemainderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tCIssueToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem StudentToolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem ClasstoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeGrouptoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeMasttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeStructtoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem BustoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem VantoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StudentsImporttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem ExittoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem ClassTransfertoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem FeeConsessiontoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeReceipttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeRecontoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem TcIssuetoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem StudBustoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem OverDuetoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeReporttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem StudentRpttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem StudentLedgertoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem FeeRemindertoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem ReceipttoolStripMenu;
        private System.Windows.Forms.ToolStripMenuItem termFeeReconciliationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tCDetailsToolStripMenuItem;
    }
}



