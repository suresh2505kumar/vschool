﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmFeeConcessionBulk : Form
    {
        public FrmFeeConcessionBulk()
        {
            InitializeComponent();
        }
        CommonClass cmc = new CommonClass();
        SqlConnection conn = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        BindingSource bsStudentName = new BindingSource();
        private void label4_Click(object sender, EventArgs e)
        {

        }
        protected void getClass()
        {
            CommonClass cmc = new CommonClass();
            DataTable dt = cmc.GetAllClass("SPGetClass", conn);
            cmbClass.DataSource = null;
            cmbClass.DisplayMember = "Class_Desc";
            cmbClass.ValueMember = "Cid";
            cmbClass.DataSource = dt;
        }

        private void FrmFeeConcessionBulk_Load(object sender, EventArgs e)
        {
            getClass();
        }

        private void cmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dt = cmc.GetStudentById(Convert.ToInt32(cmbClass.SelectedValue), "SP_GetStudentByclass", conn);
            bsStudentName.DataSource = dt;
            LoadDataGrid(bsStudentName);
            grStudent.Visible = true;
            txtStudentsearch.Focus();
        }
        protected void LoadDataGrid(BindingSource dt)
        {
            DataGridStudent.DataSource = null;
            DataGridStudent.AutoGenerateColumns = false;
            DataGridStudent.ColumnCount = 2;
            DataGridStudent.Columns[0].Name = "StudentName";
            DataGridStudent.Columns[0].HeaderText = "Student Name";
            DataGridStudent.Columns[0].DataPropertyName = "StudentName";
            DataGridStudent.Columns[0].Width = 260;
            DataGridStudent.Columns[1].Name = "Uid";
            DataGridStudent.Columns[1].HeaderText = "Uid";
            DataGridStudent.Columns[1].DataPropertyName = "Uid";
            DataGridStudent.Columns[1].Visible = false;
            DataGridStudent.DataSource = dt;
        }

        private void DataGridStudent_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = DataGridStudent.SelectedCells[0].RowIndex;
            txtStudentName.Text = DataGridStudent.Rows[index].Cells[0].Value.ToString();
            txtStudentName.Tag = DataGridStudent.Rows[index].Cells[1].Value;
            grStudent.Visible = false;
        }

        private void txtStudentsearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grStudent.Visible = false;
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            SqlParameter[] parameter = {
                    new SqlParameter("@ClassId",cmbClass.SelectedValue),
                    new SqlParameter("@StudId",txtStudentName.Tag)
                };
            DataSet ds = cmc.GetTcById(CommandType.StoredProcedure, "SP_GetAllTermFeeByStudId", parameter, conn);
            DataTable dtI = ds.Tables[0];
            DataTable dtII = ds.Tables[1];
            DataTable dtIII = ds.Tables[2];
            LoadTermFee(dtI);
            LoadTermIIFee(dtII);
            LoadTermIIIFee(dtIII);
        }

        protected void LoadTermFee(DataTable dt)
        {
            DataGridTermI.DataSource = null;
            DataGridTermI.AutoGenerateColumns = false;
            DataGridTermI.ColumnCount = 8;
            DataGridTermI.Columns[0].Name = "Puid";
            DataGridTermI.Columns[0].HeaderText = "Puid";
            DataGridTermI.Columns[0].DataPropertyName = "Puid";
            DataGridTermI.Columns[0].Visible = false;

            DataGridTermI.Columns[1].Name = "Studid";
            DataGridTermI.Columns[1].HeaderText = "Studid";
            DataGridTermI.Columns[1].DataPropertyName = "Studid";
            DataGridTermI.Columns[1].Visible = false;

            DataGridTermI.Columns[2].Name = "feeid";
            DataGridTermI.Columns[2].HeaderText = "feeid";
            DataGridTermI.Columns[2].DataPropertyName = "feeid";
            DataGridTermI.Columns[2].Visible = false;

            DataGridTermI.Columns[3].Name = "Fees";
            DataGridTermI.Columns[3].HeaderText = "Fees";
            DataGridTermI.Columns[3].DataPropertyName = "Descp";

            DataGridTermI.Columns[4].Name = "Fee Amount";
            DataGridTermI.Columns[4].HeaderText = "Fee Amount";
            DataGridTermI.Columns[4].DataPropertyName = "feeamt";
            DataGridTermI.Columns[4].Width = 70;
            DataGridTermI.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermI.Columns[5].Name = "Concession";
            DataGridTermI.Columns[5].HeaderText = "Concession";
            DataGridTermI.Columns[5].DataPropertyName = "concession";
            DataGridTermI.Columns[5].Width = 70;
            DataGridTermI.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermI.Columns[6].Name = "Finalamt";
            DataGridTermI.Columns[6].HeaderText = "Finalamt";
            DataGridTermI.Columns[6].DataPropertyName = "finalamt";
            DataGridTermI.Columns[6].Width = 70;
            DataGridTermI.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermI.Columns[7].Name = "Paid Amount";
            DataGridTermI.Columns[7].HeaderText = "Paid Amount";
            DataGridTermI.Columns[7].DataPropertyName = "paidamt";
            DataGridTermI.Columns[7].Width = 70;
            DataGridTermI.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTermI.DataSource = dt;
        }
        protected void LoadTermIIFee(DataTable dt)
        {
            DataGridTermII.DataSource = null;
            DataGridTermII.AutoGenerateColumns = false;
            DataGridTermII.ColumnCount = 8;
            DataGridTermII.Columns[0].Name = "Puid";
            DataGridTermII.Columns[0].HeaderText = "Puid";
            DataGridTermII.Columns[0].DataPropertyName = "Puid";
            DataGridTermII.Columns[0].Visible = false;

            DataGridTermII.Columns[1].Name = "Studid";
            DataGridTermII.Columns[1].HeaderText = "Studid";
            DataGridTermII.Columns[1].DataPropertyName = "Studid";
            DataGridTermII.Columns[1].Visible = false;

            DataGridTermII.Columns[2].Name = "feeid";
            DataGridTermII.Columns[2].HeaderText = "feeid";
            DataGridTermII.Columns[2].DataPropertyName = "feeid";
            DataGridTermII.Columns[2].Visible = false;

            DataGridTermII.Columns[3].Name = "Fees";
            DataGridTermII.Columns[3].HeaderText = "Fees";
            DataGridTermII.Columns[3].DataPropertyName = "Descp";

            DataGridTermII.Columns[4].Name = "Fee Amount";
            DataGridTermII.Columns[4].HeaderText = "Fee Amount";
            DataGridTermII.Columns[4].DataPropertyName = "feeamt";
            DataGridTermII.Columns[4].Width = 70;
            DataGridTermII.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermII.Columns[5].Name = "Concession";
            DataGridTermII.Columns[5].HeaderText = "Concession";
            DataGridTermII.Columns[5].DataPropertyName = "concession";
            DataGridTermII.Columns[5].Width = 70;
            DataGridTermII.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermII.Columns[6].Name = "Finalamt";
            DataGridTermII.Columns[6].HeaderText = "Finalamt";
            DataGridTermII.Columns[6].DataPropertyName = "finalamt";
            DataGridTermII.Columns[6].Width = 70;
            DataGridTermII.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermII.Columns[7].Name = "Paid Amount";
            DataGridTermII.Columns[7].HeaderText = "Paid Amount";
            DataGridTermII.Columns[7].DataPropertyName = "paidamt";
            DataGridTermII.Columns[7].Width = 70;
            DataGridTermII.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTermII.DataSource = dt;
        }
        protected void LoadTermIIIFee(DataTable dt)
        {
            DataGridTermIII.DataSource = null;
            DataGridTermIII.AutoGenerateColumns = false;
            DataGridTermIII.ColumnCount = 8;
            DataGridTermIII.Columns[0].Name = "Puid";
            DataGridTermIII.Columns[0].HeaderText = "Puid";
            DataGridTermIII.Columns[0].DataPropertyName = "Puid";
            DataGridTermIII.Columns[0].Visible = false;

            DataGridTermIII.Columns[1].Name = "Studid";
            DataGridTermIII.Columns[1].HeaderText = "Studid";
            DataGridTermIII.Columns[1].DataPropertyName = "Studid";
            DataGridTermIII.Columns[1].Visible = false;

            DataGridTermIII.Columns[2].Name = "feeid";
            DataGridTermIII.Columns[2].HeaderText = "feeid";
            DataGridTermIII.Columns[2].DataPropertyName = "feeid";
            DataGridTermIII.Columns[2].Visible = false;

            DataGridTermIII.Columns[3].Name = "Fees";
            DataGridTermIII.Columns[3].HeaderText = "Fees";
            DataGridTermIII.Columns[3].DataPropertyName = "Descp";

            DataGridTermIII.Columns[4].Name = "Fee Amount";
            DataGridTermIII.Columns[4].HeaderText = "Fee Amount";
            DataGridTermIII.Columns[4].DataPropertyName = "feeamt";
            DataGridTermIII.Columns[4].Width = 70;
            DataGridTermIII.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermIII.Columns[5].Name = "Concession";
            DataGridTermIII.Columns[5].HeaderText = "Concession";
            DataGridTermIII.Columns[5].DataPropertyName = "concession";
            DataGridTermIII.Columns[5].Width = 70;
            DataGridTermIII.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermIII.Columns[6].Name = "Finalamt";
            DataGridTermIII.Columns[6].HeaderText = "Finalamt";
            DataGridTermIII.Columns[6].DataPropertyName = "finalamt";
            DataGridTermIII.Columns[6].Width = 70;
            DataGridTermIII.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridTermIII.Columns[7].Name = "Paid Amount";
            DataGridTermIII.Columns[7].HeaderText = "Paid Amount";
            DataGridTermIII.Columns[7].DataPropertyName = "paidamt";
            DataGridTermIII.Columns[7].Width = 70;
            DataGridTermIII.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTermIII.DataSource = dt;
        }

        private void grStudent_Enter(object sender, EventArgs e)
        {

        }

        private void txtTermI_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtTermI_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtTermI.Text != string.Empty)
                    {
                        decimal cnAmount = Convert.ToDecimal(txtTermI.Text);
                        for (int i = 0; i < DataGridTermI.Rows.Count; i++)
                        {
                            decimal FinalAmount = Convert.ToDecimal(DataGridTermI.Rows[i].Cells[6].Value);
                            decimal PaidAmount = Convert.ToDecimal(DataGridTermI.Rows[i].Cells[7].Value);
                            decimal b = FinalAmount - PaidAmount;
                            if (FinalAmount == PaidAmount)
                            {
                                DataGridTermI.Rows[i].Cells[5].Value = b;
                            }
                            else
                            {
                                if (cnAmount > 0)
                                {
                                    if (cnAmount >= FinalAmount)
                                    {
                                        DataGridTermI.Rows[i].Cells[5].Value = b;
                                        cnAmount -= b;
                                    }
                                    else
                                    {
                                        DataGridTermI.Rows[i].Cells[5].Value = cnAmount;
                                        cnAmount -= FinalAmount;
                                    }
                                }
                                else
                                {
                                    DataGridTermI.Rows[i].Cells[5].Value = 0;
                                }
                            }
                        }

                        if (cnAmount > 0)
                        {
                            for (int i = 0; i < DataGridTermII.Rows.Count; i++)
                            {
                                decimal FinalAmount = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[6].Value);
                                decimal PaidAmount = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[7].Value);
                                decimal b = FinalAmount - PaidAmount;
                                if (FinalAmount == PaidAmount)
                                {
                                    DataGridTermII.Rows[i].Cells[5].Value = b;
                                }
                                else
                                {
                                    if (cnAmount > 0)
                                    {
                                        if (cnAmount >= FinalAmount)
                                        {
                                            DataGridTermII.Rows[i].Cells[5].Value = b;
                                            cnAmount -= b;
                                        }
                                        else
                                        {
                                            DataGridTermII.Rows[i].Cells[5].Value = cnAmount;
                                            cnAmount -= FinalAmount;
                                        }
                                    }
                                    else
                                    {
                                        DataGridTermII.Rows[i].Cells[5].Value = 0;
                                    }
                                }
                            }
                        }
                        if (cnAmount > 0)
                        {
                            for (int i = 0; i < DataGridTermIII.Rows.Count; i++)
                            {
                                decimal FinalAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[6].Value);
                                decimal PaidAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[7].Value);
                                decimal b = FinalAmount - PaidAmount;
                                if (FinalAmount == PaidAmount)
                                {
                                    DataGridTermIII.Rows[i].Cells[5].Value = b;
                                }
                                else
                                {
                                    if (cnAmount > 0)
                                    {
                                        if (cnAmount >= FinalAmount)
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = b;
                                            cnAmount -= b;
                                        }
                                        else
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = cnAmount;
                                            cnAmount -= FinalAmount;
                                        }
                                    }
                                    else
                                    {
                                        DataGridTermIII.Rows[i].Cells[5].Value = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("TermI amount can't empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTermI.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTermII_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtTermI.Text != string.Empty)
                    {
                        decimal cnAmount = Convert.ToDecimal(txtTermI.Text);
                        if (cnAmount > 0)
                        {
                            for (int i = 0; i < DataGridTermII.Rows.Count; i++)
                            {
                                decimal FinalAmount = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[6].Value);
                                decimal PaidAmount = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[7].Value);
                                decimal b = FinalAmount - PaidAmount;
                                if (FinalAmount == PaidAmount)
                                {
                                    DataGridTermII.Rows[i].Cells[5].Value = b;
                                }
                                else
                                {
                                    if (cnAmount > 0)
                                    {
                                        if (cnAmount >= FinalAmount)
                                        {
                                            DataGridTermII.Rows[i].Cells[5].Value = b;
                                            cnAmount -= b;
                                        }
                                        else
                                        {
                                            DataGridTermII.Rows[i].Cells[5].Value = cnAmount;
                                            cnAmount -= FinalAmount;
                                        }
                                    }
                                    else
                                    {
                                        DataGridTermII.Rows[i].Cells[5].Value = 0;
                                    }
                                }
                            }
                        }
                        if (cnAmount > 0)
                        {
                            for (int i = 0; i < DataGridTermIII.Rows.Count; i++)
                            {
                                decimal FinalAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[6].Value);
                                decimal PaidAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[7].Value);
                                decimal b = FinalAmount - PaidAmount;
                                if (FinalAmount == PaidAmount)
                                {
                                    DataGridTermIII.Rows[i].Cells[5].Value = b;
                                }
                                else
                                {
                                    if (cnAmount > 0)
                                    {
                                        if (cnAmount >= FinalAmount)
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = b;
                                            cnAmount -= b;
                                        }
                                        else
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = cnAmount;
                                            cnAmount -= FinalAmount;
                                        }
                                    }
                                    else
                                    {
                                        DataGridTermIII.Rows[i].Cells[5].Value = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("TermI amount can't empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTermII.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTermIII_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtTermI.Text != string.Empty)
                    {
                        decimal cnAmount = Convert.ToDecimal(txtTermIII.Text);
                        if (cnAmount > 0)
                        {
                            for (int i = 0; i < DataGridTermIII.Rows.Count; i++)
                            {
                                decimal FinalAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[6].Value);
                                decimal PaidAmount = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[7].Value);
                                decimal b = FinalAmount - PaidAmount;
                                if (FinalAmount == PaidAmount)
                                {
                                    DataGridTermIII.Rows[i].Cells[5].Value = b;
                                }
                                else
                                {
                                    if (cnAmount > 0)
                                    {
                                        if (cnAmount >= FinalAmount)
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = b;
                                            cnAmount -= b;
                                        }
                                        else
                                        {
                                            DataGridTermIII.Rows[i].Cells[5].Value = cnAmount;
                                            cnAmount -= FinalAmount;
                                        }
                                    }
                                    else
                                    {
                                        DataGridTermIII.Rows[i].Cells[5].Value = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("TermI amount can't empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtTermIII.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (txtRemarks.Text != string.Empty)
                {
                    // TermI
                    decimal TermAmt = 0;
                    decimal ConTerm1 = 0;
                    for (int i = 0; i < DataGridTermI.Rows.Count; i++)
                    {
                        TermAmt += Convert.ToDecimal(DataGridTermI.Rows[i].Cells[6].Value.ToString());
                        ConTerm1 += Convert.ToDecimal(DataGridTermI.Rows[i].Cells[5].Value.ToString());
                    }

                    if (ConTerm1 != 0)
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@ClassId",cmbClass.SelectedValue),
                            new SqlParameter("@StudId",txtStudentName.Tag),
                            new SqlParameter("@TermUid","1"),
                            new SqlParameter("@Amt",TermAmt),
                            new SqlParameter("@ConAmt",ConTerm1),
                            new SqlParameter("@Descp",txtRemarks.Text),
                            new SqlParameter("@dt",dtpDate.Text),
                            new SqlParameter("@FeeConnA",(TermAmt - ConTerm1)),
                            new SqlParameter("@ReturnId",SqlDbType.Int)
                        };
                        para[8].Direction = ParameterDirection.Output;
                        int uid = cmc.ExecuteNonQuery("SP_FeeConM", para, 8, conn);
                        string Query = "Delete from FeeCon_det Where fconid = " + uid + "";
                        cmc.ExecuteNonQuery(CommandType.Text, Query, conn);
                        for (int i = 0; i < DataGridTermI.Rows.Count; i++)
                        {
                            decimal FinalAmt = Convert.ToDecimal(DataGridTermI.Rows[i].Cells[6].Value.ToString());
                            decimal Concess = Convert.ToDecimal(DataGridTermI.Rows[i].Cells[5].Value.ToString());
                            SqlParameter[] paradet = {
                                    new SqlParameter("@FeeconUid",uid),
                                    new SqlParameter("@Amt",FinalAmt),
                                    new SqlParameter("@ConAmt",Concess),
                                    new SqlParameter("@FeeCon",(FinalAmt - Concess)),
                                    new SqlParameter("@Feeid",DataGridTermI.Rows[i].Cells[2].Value.ToString())
                            };
                            cmc.ExecuteNonQuery("SP_FeeConDet", paradet, conn);
                        }
                        SqlParameter[] paraUidI = { new SqlParameter("@Uid", uid) };
                        DataTable dt = cmc.GetDatabyParameter("SP_GetDataForUpdatePlf", paraUidI, conn);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            double s = Convert.ToDouble(dt.Rows[i]["Amt"].ToString());
                            double s1 = Convert.ToDouble(dt.Rows[i]["ConAmt"].ToString());
                            double j = s - s1;
                            conn.Open();
                            string qu = "update PLFType set concession =" + dt.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + dt.Rows[i]["studid"].ToString() + "  and feeid=" + dt.Rows[i]["feeid"].ToString() + "  and termuid=1 ";
                            SqlCommand scmd1 = new SqlCommand(qu, conn);
                            scmd1.ExecuteNonQuery();
                            conn.Close();
                        }
                    }

                    // TermII
                    decimal TermAmtII = 0;
                    decimal ConTerm1I = 0;
                    for (int i = 0; i < DataGridTermII.Rows.Count; i++)
                    {
                        TermAmtII += Convert.ToDecimal(DataGridTermII.Rows[i].Cells[6].Value.ToString());
                        ConTerm1I += Convert.ToDecimal(DataGridTermII.Rows[i].Cells[5].Value.ToString());
                    }
                    if (ConTerm1I != 0)
                    {
                        SqlParameter[] paraII = {
                            new SqlParameter("@ClassId",cmbClass.SelectedValue),
                            new SqlParameter("@StudId",txtStudentName.Tag),
                            new SqlParameter("@TermUid","2"),
                            new SqlParameter("@Amt",TermAmtII),
                            new SqlParameter("@ConAmt",ConTerm1I),
                            new SqlParameter("@Descp",txtRemarks.Text),
                            new SqlParameter("@dt",dtpDate.Text),
                            new SqlParameter("@FeeConnA",(TermAmtII - ConTerm1I)),
                            new SqlParameter("@ReturnId",SqlDbType.Int)
                        };
                        paraII[8].Direction = ParameterDirection.Output;
                        int uidII = cmc.ExecuteNonQuery("SP_FeeConM", paraII, 8, conn);
                        string QueryII = "Delete from FeeCon_det Where fconid = " + uidII + "";
                        cmc.ExecuteNonQuery(CommandType.Text, QueryII, conn);
                        for (int i = 0; i < DataGridTermII.Rows.Count; i++)
                        {
                            decimal FinalAmt = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[6].Value.ToString());
                            decimal Concess = Convert.ToDecimal(DataGridTermII.Rows[i].Cells[5].Value.ToString());
                            SqlParameter[] paradetII = {
                                new SqlParameter("@FeeconUid",uidII),
                                new SqlParameter("@Amt",FinalAmt),
                                new SqlParameter("@ConAmt",Concess),
                                new SqlParameter("@FeeCon",(FinalAmt - Concess)),
                                new SqlParameter("@Feeid",DataGridTermII.Rows[i].Cells[2].Value.ToString())
                            };
                            cmc.ExecuteNonQuery("SP_FeeConDet", paradetII, conn);
                        }
                        SqlParameter[] paraUidII = { new SqlParameter("@Uid", uidII) };
                        DataTable dt = cmc.GetDatabyParameter("SP_GetDataForUpdatePlf", paraUidII, conn);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            double s = Convert.ToDouble(dt.Rows[i]["Amt"].ToString());
                            double s1 = Convert.ToDouble(dt.Rows[i]["ConAmt"].ToString());
                            double j = s - s1;
                            conn.Open();
                            string qu = "update PLFType set concession =" + dt.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + dt.Rows[i]["studid"].ToString() + "  and feeid=" + dt.Rows[i]["feeid"].ToString() + "  and termuid=2 ";
                            SqlCommand scmd1 = new SqlCommand(qu, conn);
                            scmd1.ExecuteNonQuery();
                            conn.Close();
                        }
                    }

                    // TermIII
                    decimal TermAmtIII = 0;
                    decimal ConTerm1II = 0;
                    for (int i = 0; i < DataGridTermIII.Rows.Count; i++)
                    {
                        TermAmtIII += Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[6].Value.ToString());
                        ConTerm1II += Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[5].Value.ToString());
                    }

                    if (ConTerm1II != 0)
                    {
                        SqlParameter[] paraIII = {
                            new SqlParameter("@ClassId",cmbClass.SelectedValue),
                            new SqlParameter("@StudId",txtStudentName.Tag),
                            new SqlParameter("@TermUid","3"),
                            new SqlParameter("@Amt",TermAmtIII),
                            new SqlParameter("@ConAmt",ConTerm1II),
                            new SqlParameter("@Descp",txtRemarks.Text),
                            new SqlParameter("@dt",dtpDate.Text),
                            new SqlParameter("@FeeConnA",(TermAmtIII - ConTerm1II)),
                            new SqlParameter("@ReturnId",SqlDbType.Int)
                        };
                        paraIII[8].Direction = ParameterDirection.Output;
                        int uidIII = cmc.ExecuteNonQuery("SP_FeeConM", paraIII, 8, conn);
                        string QueryIII = "Delete from FeeCon_det Where fconid = " + uidIII + "";
                        cmc.ExecuteNonQuery(CommandType.Text, QueryIII, conn);
                        for (int i = 0; i < DataGridTermIII.Rows.Count; i++)
                        {
                            decimal FinalAmt = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[6].Value.ToString());
                            decimal Concess = Convert.ToDecimal(DataGridTermIII.Rows[i].Cells[5].Value.ToString());
                            SqlParameter[] paradetIII = {
                                new SqlParameter("@FeeconUid",uidIII),
                                new SqlParameter("@Amt",FinalAmt),
                                new SqlParameter("@ConAmt",Concess),
                                new SqlParameter("@FeeCon",(FinalAmt - Concess)),
                                new SqlParameter("@Feeid",DataGridTermIII.Rows[i].Cells[2].Value.ToString())
                            };
                            cmc.ExecuteNonQuery("SP_FeeConDet", paradetIII, conn);
                        }

                        SqlParameter[] paraUidIII = { new SqlParameter("@Uid", uidIII) };
                        DataTable dt = cmc.GetDatabyParameter("SP_GetDataForUpdatePlf", paraUidIII, conn);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            double s = Convert.ToDouble(dt.Rows[i]["Amt"].ToString());
                            double s1 = Convert.ToDouble(dt.Rows[i]["ConAmt"].ToString());
                            double j = s - s1;
                            conn.Open();
                            string qu = "update PLFType set concession =" + dt.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + dt.Rows[i]["studid"].ToString() + "  and feeid=" + dt.Rows[i]["feeid"].ToString() + "  and termuid=3 ";
                            SqlCommand scmd1 = new SqlCommand(qu, conn);
                            scmd1.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    MessageBox.Show("Saved Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRemarks.Text = string.Empty;
                    DataGridTermI.DataSource = null;
                    DataGridTermII.DataSource = null;
                    DataGridTermIII.DataSource = null;
                    txtStudentName.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("Remarks Can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRemarks.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtStudentName_Click(object sender, EventArgs e)
        {
            grStudent.Visible = false;
        }

        private void TxtStudentsearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsStudentName.Filter = string.Format("StudentName LIKE '%{0}%'", txtStudentsearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
