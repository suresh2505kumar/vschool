﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmTcIssue : Form
    {
        public FrmTcIssue()
        {
            InitializeComponent();
        }
        int StudId = 0;
        DateTime DOB;
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        private void FrmTcIssue_Load(object sender, EventArgs e)
        {
            LoadTcDetails();           
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            grBack.Visible = false;
            grFront.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnsave.Visible = true;
            btnaddrcan.Visible = false;
            btnexit.Visible = true;
            btnsave.Text = "Save";

        }
        protected void LoadTcDetails()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_GetTCDetails";
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@Tag", 0);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataGridTC.DataSource = null;
                DataGridTC.AutoGenerateColumns = false;
                DataGridTC.ColumnCount = 10;

                DataGridTC.Columns[0].Name = "TcId";
                DataGridTC.Columns[0].HeaderText = "TcId";
                DataGridTC.Columns[0].DataPropertyName = "TcId";
                DataGridTC.Columns[0].Visible = false;

                DataGridTC.Columns[1].Name = "AdmissionId";
                DataGridTC.Columns[1].HeaderText = "AdmissionId";
                DataGridTC.Columns[1].DataPropertyName = "AdmissionId";
                DataGridTC.Columns[1].Visible = false;

                DataGridTC.Columns[2].Name = "Stud_id";
                DataGridTC.Columns[2].HeaderText = "Stud_id";
                DataGridTC.Columns[2].DataPropertyName = "Stud_id";
                DataGridTC.Columns[2].Visible = false;

                DataGridTC.Columns[3].Name = "AdmissionNo";
                DataGridTC.Columns[3].HeaderText = "AdmissionNo";
                DataGridTC.Columns[3].DataPropertyName = "AdmissionNo";

                DataGridTC.Columns[4].Name = "StudentName";
                DataGridTC.Columns[4].HeaderText = "StudentName";
                DataGridTC.Columns[4].DataPropertyName = "StudentName";
                DataGridTC.Columns[4].Width = 120;

                DataGridTC.Columns[5].Name = "Sex";
                DataGridTC.Columns[5].HeaderText = "Sex";
                DataGridTC.Columns[5].DataPropertyName = "Sex";

                DataGridTC.Columns[6].Name = "FatherName";
                DataGridTC.Columns[6].HeaderText = "FatherName";
                DataGridTC.Columns[6].DataPropertyName = "FatherName";

                DataGridTC.Columns[7].Name = "Classdesc";
                DataGridTC.Columns[7].HeaderText = "Classdesc";
                DataGridTC.Columns[7].DataPropertyName = "Classdesc";

                DataGridTC.Columns[8].Name = "AdmissionDate";
                DataGridTC.Columns[8].HeaderText = "AdmissionDate";
                DataGridTC.Columns[8].DataPropertyName = "AdmissionDate";

                DataGridTC.Columns[9].Name = "DoTC";
                DataGridTC.Columns[9].HeaderText = "DoTC";
                DataGridTC.Columns[9].DataPropertyName = "DoTC";
                DataGridTC.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            grBack.Visible = true;
            grFront.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnexit.Visible = false;
            txtStudName.Focus();
            Cleartextbox();

        }
        protected void Cleartextbox()
        {
            txtStudName.Text = string.Empty;
            lblAdmissionDate.Text = "-";
            lblClass.Text = "-";
            lblFatherName.Text = "-";
            lblSex.Text = "-";
            lblAdmissionNo.Text = "-";
            cmbReason.SelectedIndex = -1;

        }
        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            grBack.Visible = false;
            grFront.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnsave.Visible = true;
            btnaddrcan.Visible = false;
            btnexit.Visible = true;
            btnsave.Text = "Save";
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            grBack.Visible = true;
            grFront.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnsave.Text = "Update";
            btnaddrcan.Visible = true;
            btnexit.Visible = false;
        }

        private void txtStudName_Click(object sender, EventArgs e)
        {
            getStudetSearch(1);
            txtSeach.Focus();
        }

        private void txtStudName_Enter(object sender, EventArgs e)
        {
            getStudetSearch(1);
            txtSeach.Focus();
            grSearch.Visible = true;
        }

        protected void getStudetSearch(int Id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_TCStudent";
            if (Id == 1)
            {
                cmd.Parameters.AddWithValue("@Tag", "Select");
            }
            else if (Id == 2)
            {
                cmd.Parameters.AddWithValue("@Tag", "Search");
                cmd.Parameters.AddWithValue("@SearchText", txtSeach.Text);
            }
            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            DataGridStudent.DataSource = null;
            DataGridStudent.AutoGenerateColumns = false;
            DataGridStudent.ColumnCount = 2;
            DataGridStudent.Columns[0].Name = "Student Name";
            DataGridStudent.Columns[0].HeaderText = "Student Name";
            DataGridStudent.Columns[0].DataPropertyName = "StudentName";
            DataGridStudent.Columns[0].Width = 250;
            DataGridStudent.Columns[1].Name = "Uid";
            DataGridStudent.Columns[1].HeaderText = "Uid";
            DataGridStudent.Columns[1].DataPropertyName = "Uid";
            DataGridStudent.Columns[1].Visible = false;
            DataGridStudent.DataSource = dt;
        }

        private void txtSeach_TextChanged(object sender, EventArgs e)
        {
            getStudetSearch(2);
            grSearch.Visible = true;
        }

        private void DataGridStudent_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                int index = DataGridStudent.SelectedCells[0].RowIndex;
                int Uid =Convert.ToInt32(DataGridStudent.Rows[index].Cells[1].Value.ToString());
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_TCStudent";
                cmd.Parameters.AddWithValue("@Tag", "SelectStudent");                
                cmd.Parameters.AddWithValue("@SearchText", Uid.ToString());
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                txtStudName.Text = dt.Rows[0]["SName"].ToString();
                lblAdmissionNo.Text = dt.Rows[0]["Admisno"].ToString();
                lblFatherName.Text = dt.Rows[0]["FName"].ToString();
                lblSex.Text = dt.Rows[0]["Sex"].ToString();
                lblClass.Text = dt.Rows[0]["Class_Desc"].ToString();
                StudId = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                DOB = Convert.ToDateTime(dt.Rows[0]["DOB"].ToString());
                if(dt.Rows[0]["DOA"].ToString() == "")
                {
                    lblAdmissionDate.Text = "-";
                }
                else
                {
                    lblAdmissionDate.Text = dt.Rows[0]["DOA"].ToString();
                }
                grSearch.Visible = false;             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtStudName_MouseClick(object sender, MouseEventArgs e)
        {
            getStudetSearch(1);
            grSearch.Visible = true;
        }

        private void txtSeach_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_TCIssue";
                cmd.Parameters.AddWithValue("@StudentName", txtStudName.Text);
                cmd.Parameters.AddWithValue("@AdmissionNo", lblAdmissionNo.Text);
                cmd.Parameters.AddWithValue("@AdmissionID", StudId);
                cmd.Parameters.AddWithValue("@FatherName", lblFatherName.Text);
                cmd.Parameters.AddWithValue("@ClassDesc", lblClass.Text);
                if (lblSex.Text == "Male")
                {
                    cmd.Parameters.AddWithValue("@Sex", "M");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Sex", "F");
                }                
                cmd.Parameters.AddWithValue("@Standard", cmbReason.Text);
                cmd.Parameters.AddWithValue("@DOTCA",Convert.ToDateTime(dtpadmissionDate.Text));               
                cmd.Parameters.AddWithValue("@DOB", DOB);
                cmd.Parameters.AddWithValue("@Tag", "Insert");
                cmd.Connection = con;
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("TC Generated Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadTcDetails();
                grBack.Visible = false;
                grFront.Visible = true;
                btnadd.Visible = true;
                btnedit.Visible = true;
                btnsave.Visible = true;
                btnaddrcan.Visible = false;
                btnexit.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                con.Close();
                return;
            }
        }
    }
}
