﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SkoolRPWD.Reports;

namespace SkoolRPWD
{
    public partial class FrmReportview : Form
    {
        public FrmReportview()
        {
            InitializeComponent();
        }

        private void FrmReportview_Load(object sender, EventArgs e)
        {
            CrystalReport1  rprt = new CrystalReport1();
            crysReportViewer.Refresh();            
            //rprt.DataSourceConnections.Clear();            
            //rprt.Refresh();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
            rprt.Load(@"D:\SkoolRPWD\SkoolRPWD\Reports\StudentReport.rpt");
            SqlCommand cmd = new SqlCommand("select * from StudentM where Classid = 27", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            conn.Close();
            sda.Fill(ds, "Newtbl_data");
            //rprt.SetDatabaseLogon("MepUser01", "CounterPay01", "anemone.arvixe.com", "MylaSchool");
            //rprt.SetDatabaseLogon("MepUser01", "CounterPay01", "anemone.arvixe.com", "MylaSchool");
            rprt.SetDataSource(ds);
            crysReportViewer.ReportSource = rprt;
        }
    }
}
