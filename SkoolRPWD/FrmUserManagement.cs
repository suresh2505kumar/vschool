﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SkoolRPWD
{
    public partial class FrmUserManagement : Form
    {
        public FrmUserManagement()
        {
            InitializeComponent();
            txtPassword.KeyUp += textBox_Compare;
            txtConfirmPassword.KeyUp += textBox_Compare;
        }
        SqlConnection con = new SqlConnection("Data Source=" + Module.ServerName + "; Initial Catalog=" + Module.DbName + ";User id=" + Module.UserName + ";Password=" + Module.Password + "");
        CommonClass db = new CommonClass();
        TreeNode parentNode = null;
        int Id;
        private void FrmUserManagement_Load(object sender, EventArgs e)
        {
            LoadTreViewMenuNewUser();
            FunC.buttonstyleform(this);
            FunC.buttonstylepanel(panel1);
            GetUser();
            LoadButton(0);
        }
        private void textBox_Compare(object sender, KeyEventArgs e)
        {
            Color cBackColor = Color.Red;
            if (txtPassword.Text == txtConfirmPassword.Text)
            {
                cBackColor = Color.Green;
            }
            txtPassword.ForeColor = cBackColor;
            txtConfirmPassword.ForeColor = cBackColor;
        }
        protected void LoadTreViewMenuNewUser()
        {
            try
            {
                string Query = "SELECT MAINMNU,MENUPARVAL,STATUS FROM MNU_PARENT WHERE USERID = 1 order by MENUPARVAL";
                DataTable dt = db.GetData(CommandType.Text, Query,con);
                foreach (DataRow dr in dt.Rows)
                {
                    parentNode = treeviewMenu.Nodes.Add(dr["MAINMNU"].ToString());
                    PopulateTreeViewNew(Convert.ToInt32(dr["MENUPARVAL"].ToString()), parentNode);
                }
                treeviewMenu.ExpandAll();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            finally
            {

            }
        }
        private void PopulateTreeViewNew(int parentId, TreeNode parentNode)
        {
            string Seqchildc = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS FROM MNU_SUBMENU WHERE MENUPARVAL=" + parentId + " and  USERID = 1";
            DataTable dtchildc = db.GetData(CommandType.Text, Seqchildc,con);
            TreeNode childNode;
            foreach (DataRow dr in dtchildc.Rows)
            {
                if (parentNode == null)
                {
                    childNode = treeviewMenu.Nodes.Add(dr["FRM_NAME"].ToString());
                }
                else
                {
                    childNode = parentNode.Nodes.Add(dr["FRM_NAME"].ToString());
                }
                PopulateTreeViewNew(Convert.ToInt32(dr["MNUSUBMENU"].ToString()), childNode);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = { new SqlParameter("@UserName", txtUserName.Text), new SqlParameter("@Password", txtPassword.Text), new SqlParameter("@ReturnId", SqlDbType.Int) };
                    para[2].Direction = ParameterDirection.Output;
                    Id = db.ExecuteNonQuery("SP_UserRegistration", para, con);                    
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@UserName", txtUserName.Text), new SqlParameter("@Password", txtPassword.Text), new SqlParameter("@Uid", Id) };
                    Id = db.ExecuteNonQuery("UdateUserRegistration", para,con);
                }                
                foreach (TreeNode node in treeviewMenu.Nodes)
                {
                    string parent = node.Text;
                    //string chckpar = node.Checked.ToString();
                    if (node.Checked == true)
                    {
                        SqlParameter[] Parame = { new SqlParameter("@UserId", Id), new SqlParameter("@MainMenu", parent) };
                        db.ExecuteNonQuery("SP_UpdateParenMenu", Parame,con);
                    }

                    foreach (TreeNode child in node.Nodes)
                    {
                        string chid = child.Text;
                        if (child.Checked == true)
                        {
                            SqlParameter[] ChildPra = { new SqlParameter("@UserId", Id), new SqlParameter("@SubMenu", chid) };
                            db.ExecuteNonQuery("SP_UpdateSubMenu", ChildPra,con);
                        }
                    }
                }
                MessageBox.Show("Role Created Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadButton(int Id)
        {
            if (Id == 0)
            {
                btnadd.Visible = true;
                btnedit.Visible = true;
                btnExit.Visible = true;
                btnSave.Visible = false;
                btnaddrcan.Visible = false;
                grFront.Visible = true;
                grBack.Visible = false;
            }
            else if (Id == 1)
            {
                btnadd.Visible = false;
                btnedit.Visible = false;
                btnExit.Visible = false;
                btnSave.Visible = true;
                btnaddrcan.Visible = true;
                grFront.Visible = false;
                grBack.Visible = true;
                btnSave.Text = "Save";
            }
            else if (Id == 2)
            {
                btnadd.Visible = false;
                btnedit.Visible = false;
                btnExit.Visible = false;
                btnSave.Visible = true;
                btnaddrcan.Visible = true;
                grFront.Visible = false;
                grBack.Visible = true;
                btnSave.Text = "Update";
            }
            else
            {
                btnadd.Visible = true;
                btnedit.Visible = true;
                btnExit.Visible = true;
                btnSave.Visible = false;
                btnaddrcan.Visible = false;
                grFront.Visible = true;
                grBack.Visible = false;
            }
        }

        protected void GetUser()
        {
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetUser",con);
            DataGridUser.DataSource = null;
            DataGridUser.AutoGenerateColumns = false;
            DataGridUser.ColumnCount = 4;
            DataGridUser.Columns[0].Name = "Uid";
            DataGridUser.Columns[0].HeaderText = "Uid";
            DataGridUser.Columns[0].DataPropertyName = "Uid";
            DataGridUser.Columns[0].Visible = false;
            DataGridUser.Columns[1].Name = "SlNo";
            DataGridUser.Columns[1].HeaderText = "SlNo";
            DataGridUser.Columns[1].Visible = false;
            DataGridUser.Columns[2].Name = "UserName";
            DataGridUser.Columns[2].HeaderText = "UserName";
            DataGridUser.Columns[2].DataPropertyName = "UserName";
            DataGridUser.Columns[2].Width = 560;
            DataGridUser.Columns[3].Name = "Password";
            DataGridUser.Columns[3].HeaderText = "Password";
            DataGridUser.Columns[3].DataPropertyName = "Password";
            DataGridUser.Columns[3].Visible = false;
            DataGridUser.DataSource = dt;

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GetUser();
            LoadButton(3);
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridUser.SelectedCells[0].RowIndex;
                Id = (int)DataGridUser.Rows[Index].Cells[0].Value;
                txtUserName.Text = DataGridUser.Rows[Index].Cells[2].Value.ToString();
                txtPassword.Text = DataGridUser.Rows[Index].Cells[3].Value.ToString();
                txtConfirmPassword.Text = DataGridUser.Rows[Index].Cells[3].Value.ToString();
                treeviewMenu.Nodes.Clear();
                string Query = "SELECT MAINMNU,MENUPARVAL,STATUS FROM MNU_PARENT Where UserId=" + Id + "ORDER BY MENUPARVAL ";
                DataTable dt = db.GetData(CommandType.Text, Query,con);
                foreach (DataRow dr in dt.Rows)
                {
                    parentNode = treeviewMenu.Nodes.Add(dr["MAINMNU"].ToString());
                    if (dr["STATUS"].ToString() == "Y")
                    {
                        parentNode.Checked = true;
                    }
                    PopulateTreeView(Convert.ToInt32(dr["MENUPARVAL"].ToString()), parentNode, Id);
                }

                treeviewMenu.ExpandAll();

                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void PopulateTreeView(int MenuId, TreeNode parentNode, object userid)
        {
            string Query = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS FROM MNU_SUBMENU WHERE MENUPARVAL=" + MenuId + "and UserId=" + userid + "";

            DataTable dt = db.GetData(CommandType.Text, Query, con);
            TreeNode childNode;
            foreach (DataRow dr in dt.Rows)
            {
                if (parentNode == null)
                {
                    childNode = treeviewMenu.Nodes.Add(dr["FRM_NAME"].ToString());
                }
                else
                {
                    childNode = parentNode.Nodes.Add(dr["FRM_NAME"].ToString());
                    if (dr["STATUS"].ToString() == "Y")
                    {
                        childNode.Checked = true;
                    }
                }
                PopulateTreeView(Convert.ToInt32(dr["MNUSUBMENU"].ToString()), childNode, userid);
            }
        }
    }
}
